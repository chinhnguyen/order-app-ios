//
//  MOStore.m
//  iskipline
//
//  Created by Chinh Nguyen on 12/16/14.
//  Copyright (c) 2014 Chinh Nguyen. All rights reserved.
//

#import "MOStore.h"
#import "MOManager.h"
#import "ZTMandrill.h"
#import "ZTTrumpia.h"
#import "NSString+Extra.h"
#import <RNDecryptor.h>
#import <GTLBase64.h>

@implementation MOStore

// Persistent fields
@dynamic account;
@dynamic phone;
@dynamic email;
@dynamic store;
@dynamic street;
@dynamic zipcode;
@dynamic city;
@dynamic state;
@dynamic close;
@dynamic open;
@dynamic merchant_id;
@dynamic merchant_key;
@dynamic lat;
@dynamic lng;
@dynamic image;
@dynamic max_paywhenpickup;
@dynamic max_paywithcard;
@dynamic mo_paywhenpickup;

@synthesize storeDatabase;

#pragma mark - Local Database

- (CBLDatabase *)openStoreDatabase:(NSError**)outError {
    if (!self.account || self.account.length == 0) {
        *outError = [NSError errorWithDomain:@"Configuration" code:500 userInfo:@{NSLocalizedDescriptionKey: @"Account database was not set for this store"}];
        return nil;
    }
    storeDatabase = [[CBLManager sharedInstance] databaseNamed:self.account error:outError];
    return storeDatabase;
}

- (NSDictionary*)findItemById:(NSString*)itemid {
    // Bad input
    if (itemid == nil || itemid.length == 0)
        return nil;
    // Database not yet opened
    if (storeDatabase == nil)
        return nil;
    // Get items document
    CBLDocument *doc = [storeDatabase existingDocumentWithID:@"items"];
    if (doc == nil)
        return nil;
    // Get items property
    NSArray *items = doc[@"items"];
    if (items == nil) {
        return nil;
    }
    // Find the matched item
    for (int i = 0; i < items.count; i++) {
        NSDictionary *item = [items objectAtIndex:i];
        if ([itemid isEqualToString:item[@"id"]])
            return item;
    }
    // Not found
    return nil;
}

#pragma mark - Calculated Properties

- (int)discount {
    int discount = [[self getValueOfProperty:@"mo-discount"] intValue];
    if (discount < 0 || discount > 100)
        discount = 0;
    return discount;
    
}

- (BOOL)allowPayWhenPickup {
    return !self.mo_paywhenpickup || [self.mo_paywhenpickup isEqual:[NSNull null]] || [@"yes" isEqualToString:self.mo_paywhenpickup];
}

- (NSString *)decrypt:(NSString*)data {
    if (!data || data.length == 0)
        return nil;
    NSError *error;
    NSData *decryptedData = [RNDecryptor decryptData:GTLDecodeBase64(data) withPassword:@"W4QESBBTjus&,;r" error:&error];
    if (error)
        return nil;
    return [[NSString alloc] initWithData:decryptedData encoding:NSUTF8StringEncoding];
}

- (NSString *)decryptedMerchantId {
    return [self decrypt:self.merchant_id];
}

- (NSString *)decryptedMerchantKey {
    return [self decrypt:self.merchant_key];
}


#pragma mark - Static formatters

static NSDateFormatter *_orderIdFormatter;

+ (NSDateFormatter*)orderIdFormatter {
    if (_orderIdFormatter == nil) {
        _orderIdFormatter = [NSDateFormatter new];
        _orderIdFormatter.dateFormat = @"yyyyddMMHHmmss";
    }
    return _orderIdFormatter;
}

static NSDateFormatter *_createdDateFormatter;

+ (NSDateFormatter*)createdDateFormatter {
    if (_createdDateFormatter == nil) {
        _createdDateFormatter = [NSDateFormatter new];
        _createdDateFormatter.dateFormat = @"MM/dd/yyyy hh:mm a";
    }
    return _createdDateFormatter;
}

static NSDateFormatter *_pickupDateFormatter;

+ (NSDateFormatter*)pickupDateFormatter {
    if (_pickupDateFormatter == nil) {
        _pickupDateFormatter = [NSDateFormatter new];
        _pickupDateFormatter.dateFormat = @"MM/dd/yyyy";
    }
    return _pickupDateFormatter;
}

static NSDateFormatter *_pickupTimeFormatter;

+ (NSDateFormatter*)pickupTimeFormatter {
    if (_pickupTimeFormatter == nil) {
        _pickupTimeFormatter = [NSDateFormatter new];
        _pickupTimeFormatter.dateFormat = @"hh:mm a";
    }
    return _pickupTimeFormatter;
}

#pragma mark - Order Related

- (void)makeOrder:(NSArray *)orderItems
          forUser:(MOAppUser *)user
  withTransaction:(MOTransaction *)transaction
    andPickupTime:(NSDate *)pickupTime
         subsribe:(BOOL)promotionSubscribe
         ereceipt:(NSString *)ereceiptType
          success:(void (^)())success
          failure:(void (^)(NSError *error))failure
   receiptSuccess:(void (^)())receiptSuccess
   receiptFailure:(void (^)(NSError *error))receiptFailure {
    NSDate *now = [NSDate date];
    NSString *docid = [[MOStore orderIdFormatter] stringFromDate:now];
    
    NSMutableDictionary *order = [NSMutableDictionary new];
    [order setValue:docid forKey:@"_id"];
    [order setValue:@"MO" forKey:@"note"];
    [order setValue:[NSNumber numberWithInt:0] forKey:@"station"];
    [order setValue:[NSNumber numberWithInt:1] forKey:@"mode"];
    [order setValue:@"iPhone" forKey:@"cashier"];
    [order setValue:@YES forKey:@"isPrint"];
    
    if (user) {
        [order setValue:[NSString stringWithFormat:@"%@ %@", user.firstname, user.lastname] forKey:@"customerName"];
        [order setValue:user.address forKey:@"address"];
        [order setValue:user.phone forKey:@"phone"];
        [order setValue:user.email forKey:@"email"];
    } else {
        [order setValue:@"" forKey:@"customerName"];
        [order setValue:@"" forKey:@"address"];
        [order setValue:@"" forKey:@"phone"];
        [order setValue:@"" forKey:@"email"];
    }
    
    if (transaction) {
        [order setValue:transaction.transactionID forKey:@"idTransactionNumber"];
        [order setValue:[NSNumber numberWithDouble:transaction.totalAmount] forKey:@"totalDue"];
        [order setValue:[NSNumber numberWithDouble:transaction.subTotalAmount] forKey:@"totalPrice"];
        [order setValue:[NSNumber numberWithDouble:transaction.taxAmount] forKey:@"salesTax"];
        [order setValue:[NSNumber numberWithDouble:transaction.discountAmount] forKey:@"discount"];
    } else {
        [order setValue:@"" forKey:@"idTransactionNumber"];
        [order setValue:[NSNumber numberWithInt:0] forKey:@"totalDue"];
        [order setValue:[NSNumber numberWithInt:0] forKey:@"totalPrice"];
        [order setValue:[NSNumber numberWithInt:0] forKey:@"salesTax"];
        [order setValue:[NSNumber numberWithInt:0] forKey:@"discount"];
    }
    
    [order setValue:@YES forKey:@"withAlert"];
    [order setValue:@"" forKey:@"kindRemind"];
    
    if (pickupTime) {
        pickupTime = now;
        [order setValue:[[MOStore pickupDateFormatter] stringFromDate:pickupTime] forKey:@"datePickup"];
        [order setValue:[[MOStore pickupTimeFormatter] stringFromDate:pickupTime] forKey:@"timePickup"];
        [order setValue:@"" forKey:@"dateRemind"];
        [order setValue:@"" forKey:@"timeRemind"];
    } else {
        [order setValue:[[MOStore pickupDateFormatter] stringFromDate:pickupTime] forKey:@"datePickup"];
        [order setValue:[[MOStore pickupTimeFormatter] stringFromDate:pickupTime] forKey:@"timePickup"];
        
        NSDate *remindTime = [pickupTime dateByAddingTimeInterval:-(60 * 60)];
        [order setValue:[[MOStore pickupDateFormatter] stringFromDate:remindTime] forKey:@"dateRemind"];
        [order setValue:[[MOStore pickupTimeFormatter] stringFromDate:remindTime] forKey:@"timeRemind"];
    }
    
    [order setValue:[[MOStore createdDateFormatter] stringFromDate:now] forKey:@"dayCreate"];
    [order setValue:[NSNumber numberWithInt:0] forKey:@"depositAmount"];
    [order setValue:@"8" forKey:@"type"];
    [order setValue:@"" forKey:@"express_code"];
    
    // Create the order
    NSError *error;
    NSData *orderJsonData = [NSJSONSerialization dataWithJSONObject:@{@"order":orderItems} options:0 error:&error];
    NSString *orderJson = [[NSString alloc] initWithData:orderJsonData encoding:NSUTF8StringEncoding];
    [order setValue:orderJson forKey:@"order"];
    
    // Update the order with detail information
    [order setValue:user.phone forKey:@"phone"];
    
    // Add promo subscription
    [order setValue:(promotionSubscribe ? @"yes" : @"no") forKey:@"promo_subscription"];
    // Ereceipt
    [order setValue:ereceiptType forKey:@"ereceipt"];
    
    NSString *url = storeOrderApiURLString(self.account, docid);
    [[MOManager sharedInstance] put:url object:order success:^(NSInteger statusCode, NSString *responseString) {
        // Inform success
        success();
        // Sending receipt in the background
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            // Set back to Dictionary to avoid parsing JSON
            [order setValue:[NSDictionary dictionaryWithObject:orderItems forKey:@"order"] forKey:@"order"];
            [self sendOrderReceipt:order success:receiptSuccess failure:receiptFailure];
        });
    } failure:^(NSInteger statusCode, NSError *error) {
        if (statusCode == 404) {
            // Database not created, must have clearer error message
            failure([NSError errorWithDomain:@"MobileOrder" code:kMOErrorOrderDbNotExists                                    userInfo:@{ NSLocalizedDescriptionKey: NSLocalizedString(@"The store's order database is not yet created.", @"") }]);
        } else {
            failure(error);
        }
    }];
}

- (void)sendOrderReceipt:(NSDictionary *)order
                 success:(void (^)())success
                 failure:(void (^)(NSError *error))failure {
    NSString *receiptType = order[@"ereceipt"];
    if (receiptType && receiptType.length > 0) {
        if ([receiptType isEqualToString:@"email"]) {
            NSString *email = order[@"email"];
            if (email && email.length > 0) {
                [self sendEmailReceipt:order success:success failure:failure];
            }
        } else if ([receiptType isEqualToString:@"sms"]) {
            NSString *phone = order[@"phone"];
            if (phone && phone.length > 0) {
                [self sendSMSReceipt:order success:success failure:failure];
            }
        }
    }
}

- (void)sendEmailReceipt:(NSDictionary *)order
                 success:(void (^)())success
                 failure:(void (^)(NSError *error))failure {
    // Construct the message
    NSMutableDictionary *message = [NSMutableDictionary new];
    message[@"subject"] = [NSString stringWithFormat:@"Order Receipt: %@", order[@"_id"]];
    message[@"text"] = [self buildTextEmailReceipt:order];
    message[@"html"] = [self buildHtmlEmailReceipt:order];
    
    message[@"from_name"] = self.store;
    message[@"from_email"] = self.email;
    
    NSMutableArray *addresses = [NSMutableArray new];
    [addresses addObject:[NSDictionary dictionaryWithObjectsAndKeys:
                          @"to", @"type",
                          order[@"customerName"], @"name",
                          order[@"email"], @"email",
                          nil]];
    [addresses addObject:[NSDictionary dictionaryWithObjectsAndKeys:
                          @"cc", @"type",
                          self.store, @"name",
                          self.email, @"email",
                          nil]];
    message[@"to"] = addresses;
    
    // Start sending mail
    [ZTMandrill sendMail:message success:success failure:failure];
}

- (void)sendSMSReceipt:(NSDictionary *)order
               success:(void (^)())success
               failure:(void (^)(NSError *error))failure {
    void(^sendSMS)(NSString*) = ^(NSString* subscriptionId) {
        NSString *description = [NSString stringWithFormat:NSLocalizedString(@"Order Receipt %d", @""), order[@"_id"]];
        NSString *message = [self buildSMSReceipt:order];
        [ZTTrumpia sendSMSTo:subscriptionId withDescription:description organizationName:self.store text:message success:success failure:failure];
    };
    
    [ZTTrumpia searchSubscriptionWithType:@"2" andData:order[@"phone"] success:^(NSString *subscriptionId) {
        if (subscriptionId && subscriptionId.length > 0) {
            sendSMS(subscriptionId);
        } else {
            NSArray *names = [order[@"customerName"] componentsSeparatedByString:@" "];
            NSDictionary *user = @{
                @"phone": order[@"phone"],
                @"firstname": names[0],
                @"lastname": names[1],
                };
            [ZTTrumpia putSubscription:user success:^(NSString *subscriptionId) {
                sendSMS(subscriptionId);
            } failure:failure];
        }
    } failure:failure];
}

- (NSString*)buildSMSReceipt:(NSDictionary *)order {
    NSString *text = [NSMutableString new];
    
    text = [text stringByAppendingFormat:@"E-receipt #%@", [order[@"_id"] substringToIndex:8]];

    NSArray *items = order[@"order"][@"order"];
    if (items && items.count > 0) {
        for (NSDictionary *item in items) {
            text = [text stringByAppendingFormat:@", %@ %@",                     item[@"quatityItemOrder"], item[@"nameItem"]];
            
            NSArray *modifiers = item[@"item"];
            NSString *note;
            for (NSDictionary *modifierItem in modifiers) {
                NSString *modifierType = modifierItem[@"typeModifierOrder"];
                if ([@"Add" isEqualToString:modifierType]) {
                    text = [text stringByAppendingFormat:@" %@",                     item[@"nameModifier"]];
                } else if ([@"Note" isEqualToString:modifierType]) {
                    note = item[@"noteModifierOrder"];
                }
            }
            
            if (note && note.length > 0) {
                [text stringByAppendingFormat:@"note: %@",                    note];
            }
        }
        // Grand total
        text = [text stringByAppendingFormat:@", Total %.2f",[order[@"TransactionAmount"] doubleValue]];
    }
    
    NSString *pickup = @"";
    NSString *timePickup = order[@"timePickup"];
    if (timePickup && timePickup.length > 0) {
        pickup = [pickup stringByAppendingString:timePickup];
    }
    NSString *datePickup = order[@"datePickup"];
    if (datePickup && datePickup.length > 0) {
        pickup = [pickup stringByAppendingFormat:@" %@", datePickup];
    }
    pickup = [pickup stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (pickup.length > 0) {
        text = [text stringByAppendingFormat:@". Pickup@ %@", pickup];
    }
    
    NSString *customerName = order[@"customerName"];
    if (customerName && customerName.length > 0) {
        text = [text stringByAppendingFormat:@". Thank you, %@!", customerName];
    } else {
        text = [text stringByAppendingString:@". Thank you!"];
    }
    
    return text;
}

- (NSString*)buildTextEmailReceipt:(NSDictionary *)order {
    NSString *text = [NSMutableString new];
    
    text = [text stringByAppendingString:@"\n"];
    text = [text stringByAppendingString:[@"" padRight:@"-" forLength:51]];
    
    
    NSString *datePickup = order[@"datePickup"];
    if (datePickup && datePickup.length > 0) {
        text = [text stringByAppendingString:[NSString stringWithFormat:@"\nPickup date: %@", datePickup]];
    }
    
    NSString *timePickup = order[@"timePickup"];
    if (timePickup && timePickup.length > 0) {
        text = [text stringByAppendingString:[NSString stringWithFormat:@"\nPickup time: %@", timePickup]];
    }
    
    text = [text stringByAppendingString:@"\n"];
    
    NSString *phone = order[@"phone"];
    if (phone && phone.length > 0) {
        text = [text stringByAppendingString:[NSString stringWithFormat:@"\nPhone/Account: %@", phone]];
    }
    //
    NSString *transactionID = order[@"TransactionID"];
    if (transactionID && transactionID.length > 0) {
        text = [text stringByAppendingString:@"\nPayment Received: YES"];
    } else {
        text = [text stringByAppendingString:@"\nPayment Received: NO"];
    }
    
    text = [text stringByAppendingString:@"\n"];
    text = [text stringByAppendingString:[@"" padRight:@"-" forLength:51]];
    text = [text stringByAppendingString:@"\n"];
    
    NSString *customerName = order[@"customerName"];
    if (!customerName || customerName.length == 0) {
        NSString *email = order[@"email"];
        if (email && email.length > 0) {
            NSRange range = [email rangeOfString:@"@"];
            if (range.location == NSNotFound) {
                customerName = email;
            } else {
                customerName = [email substringToIndex:range.location - 1];
            }
        }
    }
    
    if (customerName && customerName.length > 0) {
        text = [text stringByAppendingString:[NSString stringWithFormat:@"\nDear %@,", customerName]];
    } else {
        text = [text stringByAppendingString:@"\nDear customer,"];
    }
    
    text = [text stringByAppendingString:@"\nHere is your e-receipt.\n"];
    
    
    NSArray *items = order[@"order"][@"order"];
    if (items && items.count > 0) {
        // Receipt order
        text = [text stringByAppendingString:@"\n"];
        text = [text stringByAppendingString:
                [NSString stringWithFormat:@"%@ | %@ | %@",
                 [@"Item" padRight:@" " forLength:30],
                 [@"Qty." padLeft:@" " forLength:5],
                 [@"Subtotal" padLeft:@" " forLength:10]]];
        
        // Separator
        text = [text stringByAppendingString:@"\n"];
        text = [text stringByAppendingString:[@"" padRight:@"-" forLength:51]];
        
        for (NSDictionary *item in items) {
            text = [text stringByAppendingString:@"\n"];
            
            double priceItem = [item[@"priceItem"] doubleValue];
            int quantity = [item[@"quatityItemOrder"] doubleValue];
            double subtotal = priceItem * quantity;
            
            text = [text stringByAppendingString:
                    [NSString stringWithFormat:@"%@ | %@ | %@",
                     [item[@"nameItem"] padRight:@" " forLength:30],
                     [[NSString stringWithFormat:@"%d", quantity] padLeft:@" " forLength:5],
                     [[NSString stringWithFormat:@"$%.2f", subtotal] padLeft:@" " forLength:10]]];
            
            NSArray *modifiers = item[@"item"];
            for (NSDictionary *modifierItem in modifiers) {
                text = [text stringByAppendingString:@"\n"];
                
                NSString *modifierType = modifierItem[@"typeModifierOrder"];
                if ([@"Add" isEqualToString:modifierType]) {
                    priceItem = [modifierItem[@"priceModifier"] doubleValue];
                    quantity = [modifierItem[@"quantityModifier"] intValue];
                    subtotal = priceItem * quantity;
                    text = [text stringByAppendingString:
                            [NSString stringWithFormat:@"  %@ | %@ | %@",
                             [modifierItem[@"nameModifier"] padRight:@" " forLength:28],
                             [[NSString stringWithFormat:@"%d", quantity] padLeft:@" " forLength:5],
                             [[NSString stringWithFormat:@"$%.2f", subtotal] padLeft:@" " forLength:10]]];
                } else if ([@"Note" isEqualToString:modifierType]) {
                    text = [text stringByAppendingString:[[NSString stringWithFormat:@"Note: %@", modifierItem[@"noteModifierOrder"]] padRight:@"-" forLength:51]];
                }
            }
        }
        text = [text stringByAppendingString:@"\n"];
        
        // Separator
        text = [text stringByAppendingString:@"\n"];
        text = [text stringByAppendingString:[@"" padRight:@"-" forLength:51]];
        
        // Subtotal
        text = [text stringByAppendingString:@"\n"];
        text = [text stringByAppendingString:
                [NSString stringWithFormat:@"%@ | %@",
                 [@"Subtotal" padRight:@" " forLength:30],
                 [[NSString stringWithFormat:@"$%.2f", [order[@"subtotalAmount"] doubleValue]] padLeft:@" " forLength:18]]];
        // Discount
        double discountAmount = [order[@"discountAmount"] doubleValue];
        if (discountAmount > 0) {
            text = [text stringByAppendingString:@"\n"];
            text = [text stringByAppendingString:
                    [NSString stringWithFormat:@"%@ | %@",
                     [@"Discount" padRight:@" " forLength:30],
                     [[NSString stringWithFormat:@"$%.2f", discountAmount] padLeft:@" " forLength:18]]];
        };
        // Tax
        text = [text stringByAppendingString:@"\n"];
        text = [text stringByAppendingString:
                [NSString stringWithFormat:@"%@ | %@",
                 [@"Tax" padRight:@" " forLength:30],
                 [[NSString stringWithFormat:@"$%.2f", [order[@"taxAmount"] doubleValue]] padLeft:@" " forLength:18]]];
        // Grand total
        text = [text stringByAppendingString:@"\n"];
        text = [text stringByAppendingString:
                [NSString stringWithFormat:@"%@ | %@",
                 [@"Total" padRight:@" " forLength:30],
                 [[NSString stringWithFormat:@"$%.2f", [order[@"TransactionAmount"] doubleValue]] padLeft:@" " forLength:18]]];
    }
    
    text = [text stringByAppendingString:[NSString stringWithFormat:@"\nCreated: %@", order[@"dayCreate"]]];
    text = [text stringByAppendingString:[NSString stringWithFormat:@"\nFor question, please contact us at %@.", self.phone]];
    text = [text stringByAppendingString:[NSString stringWithFormat:@"\nThank you for shopping at %@.", self.store]];
    
    return text;
}

- (NSString*)buildHtmlEmailReceipt:(NSDictionary *)order {
    NSString *html = [NSString new];
    html = [html stringByAppendingString:@"<p>"];
    html = [html stringByAppendingString:@"<br/>----------------------------------------"];
    
    NSString *datePickup = order[@"datePickup"];
    if (datePickup && datePickup.length > 0) {
        html = [html stringByAppendingString:[NSString stringWithFormat:@"<br/>Pickup date: %@", datePickup]];
    }
    
    NSString *timePickup = order[@"timePickup"];
    if (timePickup && timePickup.length > 0) {
        html = [html stringByAppendingString:[NSString stringWithFormat:@"<br/>Pickup time: %@", timePickup]];
    }
    
    html = [html stringByAppendingString:@"<br/>"];
    
    NSString *phone = order[@"phone"];
    if (phone && phone.length > 0) {
        html = [html stringByAppendingString:[NSString stringWithFormat:@"<br/>Phone/Account: %@", phone]];
    }
    
    NSString *transactionID = order[@"TransactionID"];
    if (transactionID && transactionID.length > 0) {
        html = [html stringByAppendingString:@"<br/>Payment Received: YES"];
    } else {
        html = [html stringByAppendingString:@"<br/>Payment Received: NO"];
    }
    
    html = [html stringByAppendingString:@"<br/>----------------------------------------"];
    html = [html stringByAppendingString:@"</p>"];
    
    
    NSString *customerName = order[@"customerName"];
    if (!customerName || customerName.length == 0) {
        NSString *email = order[@"email"];
        if (email && email.length > 0) {
            NSRange range = [email rangeOfString:@"@"];
            if (range.location == NSNotFound) {
                customerName = email;
            } else {
                customerName = [email substringToIndex:range.location - 1];
            }
        }
    }
    
    if (customerName && customerName.length > 0) {
        html = [html stringByAppendingString:[NSString stringWithFormat:@"<p>Dear %@,</p>", customerName]];
    } else {
        html = [html stringByAppendingString:@"<p>Dear customer,</p>"];
    }
    html = [html stringByAppendingString:@"<p>Here is your e-receipt:</p>"];
    
    // Receipt order
    html = [html stringByAppendingString:@"<table>"];
    html = [html stringByAppendingString:@"<thead><tr><th style='border-bottom: 1px dotted black;'>Item</th><th style='border-bottom: 1px dotted black;' align=\"right\">Qty.</th><th style='border-bottom: 1px dotted black;' align=\"right\">Subtotal</th></tr></thead>"];
    html = [html stringByAppendingString:@"<tbody>"];
    
    NSArray *items = order[@"order"][@"order"];
    if (items && items.count > 0) {
        for (NSDictionary *item in items) {
            html = [html stringByAppendingString:[NSString stringWithFormat:@"<tr><td>%@</td>", item[@"nameItem"]]];
            
            double priceItem = [item[@"priceItem"] doubleValue];
            int quantity = [item[@"quatityItemOrder"] doubleValue];
            double subtotal = priceItem * quantity;
            
            html = [html stringByAppendingString:[NSString stringWithFormat:@"<td align=\"right\">%d</td>", quantity]];
            html = [html stringByAppendingString:[NSString stringWithFormat:@"<td align=\"right\">$%.2f</td></tr>", subtotal]];
            
            NSArray *modifiers = item[@"item"];
            for (NSDictionary *modifierItem in modifiers) {
                html = [html stringByAppendingString:@"<tr>"];
                NSString *modifierType = modifierItem[@"typeModifierOrder"];
                if ([@"Add" isEqualToString:modifierType]) {
                    priceItem = [modifierItem[@"priceModifier"] doubleValue];
                    quantity = [modifierItem[@"quantityModifier"] intValue];
                    subtotal = priceItem * quantity;
                    
                    html = [html stringByAppendingString:[NSString stringWithFormat:@"<td>&nbsp;&nbsp;%@</td><td align=\"right\">%d</td><td align=\"right\">$%.2f</td>", modifierItem[@"nameModifier"], quantity, subtotal]];
                } else if ([@"Note" isEqualToString:modifierType]) {
                    html = [html stringByAppendingString:[NSString stringWithFormat:@"<td colspan='3'>&nbsp;&nbsp;Note:&nbsp;%@</td>", modifierItem[@"noteModifierOrder"]]];
                }
                html = [html stringByAppendingString:@"</tr>"];
            }
        }
    }
    html = [html stringByAppendingString:@"</tbody>"];
    
    // Footer
    html = [html stringByAppendingString:@"<tfoot>"];
    
    // Subtotal
    html = [html stringByAppendingString:@"<tr>"];
    html = [html stringByAppendingString:@"<td style='border-top: 1px dotted black;'>Subtotal</td>"];
    html = [html stringByAppendingString:@"<td style='border-top: 1px dotted black;'></td>"];
    
    html = [html stringByAppendingString:[NSString stringWithFormat:@"<td style='border-top: 1px dotted black;' align=\"right\">$%.2f</td>", [order[@"subtotalAmount"] doubleValue]]];
    html = [html stringByAppendingString:@"</tr>"];
    if ([order[@"discountAmount"] doubleValue] > 0.0) {
        // Discount
        html = [html stringByAppendingString:@"<tr>"];
        html = [html stringByAppendingString:@"<td>Discount</td>"];
        html = [html stringByAppendingString:@"<td></td>"];
        html = [html stringByAppendingString:[NSString stringWithFormat:@"<td align=\"right\">$%.2f</td>", [order[@"discountAmount"] doubleValue]]];
        html = [html stringByAppendingString:@"</tr>"];
    }
    // Tax
    html = [html stringByAppendingString:@"<tr>"];
    html = [html stringByAppendingString:@"<td>Tax</td>"];
    html = [html stringByAppendingString:@"<td></td>"];
    html = [html stringByAppendingString:[NSString stringWithFormat:@"<td align=\"right\">$%.2f</td>", [order[@"taxAmount"] doubleValue]]];
    html = [html stringByAppendingString:@"</tr>"];
    // Grand total
    html = [html stringByAppendingString:@"<tr>"];
    html = [html stringByAppendingString:@"<td>Total</td>"];
    html = [html stringByAppendingString:@"<td></td>"];
    html = [html stringByAppendingString:[NSString stringWithFormat:@"<td align=\"right\">$%.2f</td>", [order[@"TransactionAmount"] doubleValue]]];
    html = [html stringByAppendingString:@"</tr>"];
    
    html = [html stringByAppendingString:@"</tfoot>"];
    
    html = [html stringByAppendingString:@"</table>"];
    
    html = [html stringByAppendingString:[NSString stringWithFormat:@"<p>%@</p>", order[@"dayCreate"]]];
    html = [html stringByAppendingString:[NSString stringWithFormat:@"<p>For question, please contact us at %@</p>", self.phone]];
    html = [html stringByAppendingString:[NSString stringWithFormat:@"<p>Thank you for shopping at  %@</p>", self.store]];
    
    return html;
}

@end

@implementation MOStore (PaymentXP)

- (void)addCustomer:(MOAppUser*)customer
            success:(void (^)(NSDictionary *results))success
            failure:(void (^)(NSError *error))failure {
    
    NSString *cardnumber = [customer.cardNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    NSString *cardexpiredmonth = [NSString stringWithFormat:@"%02d", [customer.monthExpired intValue]];
    NSString *cardexpiredyear = [[NSString stringWithFormat:@"%02d", [customer.yearExpired intValue]] substringFromIndex:2];
    NSString *cardexpireddate = [NSString stringWithFormat:@"%@%@", cardexpiredmonth, cardexpiredyear];
    NSString *customername = [NSString stringWithFormat:@"%@ %@", customer.firstname, customer.lastname];
    NSString *customerphone = [customer.phone stringByReplacingOccurrencesOfString:@"-" withString:@""];
    NSString *customerid = [customername stringByReplacingOccurrencesOfString:@" " withString:@""];
    customerid = [customerid stringByAppendingString:customerphone];
    
    NSDictionary *parameters =
    @{
      kXPMerchantID:                [self decryptedMerchantId],
      kXPMerchantKey:               [self decryptedMerchantKey],
      kXPCardNumber:                trim(cardnumber),
      kXPCardExpirationDate:        trim(cardexpireddate),
      kXPFirstName:                 trim(customer.firstname),
      kXPLastName:                  trim(customer.lastname),
      kXPAddress:                   trim(customer.address),
      kXPEmail:                     trim(customer.email),
      kXPZip:                       trim(customer.zip),
      kXPCustomerName:              trim(customername),
      kXPPhone:                     trim(customerphone),
      kXPCustomerID:                trim(customerid),
      kXPTransactionType:           @"AddCustomer",
      };
    
    // Create the request
    NSError *error;
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"POST" URLString:kXPUrl parameters:parameters error:&error];
    if (request == nil) {
        failure(error);
        return;
    }
    // Create operation
    AFHTTPRequestOperation *op = [[AFHTTPRequestOperationManager manager] HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *results = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSMutableDictionary *resultsDictionary = [NSMutableDictionary new];
        NSArray *values = [results componentsSeparatedByString:@"&"];
        for (NSString *kvp in values) {
            NSUInteger equalsSignLocation = [kvp rangeOfString:@"="].location;
            if (equalsSignLocation == NSNotFound) {
                [resultsDictionary setObject:@"" forKey:kvp];
            } else {
                NSString *key = [kvp substringToIndex:equalsSignLocation];
                NSString *value = [kvp substringFromIndex:equalsSignLocation+1];
                [resultsDictionary setObject:value forKey:key];
            }
        }
        success([NSDictionary dictionaryWithDictionary:resultsDictionary]);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
    op.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    // Schedule for execution
    [[NSOperationQueue mainQueue] addOperation:op];
    
}

- (void)updateCustomer:(MOAppUser*)customer
               success:(void (^)(NSDictionary *results))success
               failure:(void (^)(NSError *error))failure {
    
    NSString *customername = [NSString stringWithFormat:@"%@ %@", customer.firstname, customer.lastname];
    NSString *customerphone = [customer.phone stringByReplacingOccurrencesOfString:@"-" withString:@""];
    NSString *customerid = [customername stringByReplacingOccurrencesOfString:@" " withString:@""];
    customerid = [customerid stringByAppendingString:customerphone];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithDictionary:
    @{
      kXPMerchantID:                [self decryptedMerchantId],
      kXPMerchantKey:               [self decryptedMerchantKey],
      kXPFirstName:                 trim(customer.firstname),
      kXPLastName:                  trim(customer.lastname),
      kXPAddress:                   trim(customer.address),
      kXPEmail:                     trim(customer.email),
      kXPZip:                       trim(customer.zip),
      kXPCustomerName:              trim(customername),
      kXPPhone:                     trim(customerphone),
      kXPCustomerID:                trim(customerid),
      kXPTransactionType:           @"UpdateCustomer",
      }];
    if (customer.cardNumber.length > 0 || customer.monthExpired.length > 0 || customer.yearExpired.length > 0) {
        NSString *cardnumber = [customer.cardNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
        NSString *cardexpiredmonth = [NSString stringWithFormat:@"%02d", [customer.monthExpired intValue]];
        NSString *cardexpiredyear = [[NSString stringWithFormat:@"%02d", [customer.yearExpired intValue]] substringFromIndex:2];
        NSString *cardexpireddate = [NSString stringWithFormat:@"%@%@", cardexpiredmonth, cardexpiredyear];
        [parameters addEntriesFromDictionary:
         @{
           kXPCardNumber:                trim(cardnumber),
           kXPCardExpirationDate:        trim(cardexpireddate),
           }];
    }
    // Create the request
    NSError *error;
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"POST" URLString:kXPUrl parameters:parameters error:&error];
    if (request == nil) {
        failure(error);
        return;
    }
    // Create operation
    AFHTTPRequestOperation *op = [[AFHTTPRequestOperationManager manager] HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *results = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSMutableDictionary *resultsDictionary = [NSMutableDictionary new];
        NSArray *values = [results componentsSeparatedByString:@"&"];
        for (NSString *kvp in values) {
            NSUInteger equalsSignLocation = [kvp rangeOfString:@"="].location;
            if (equalsSignLocation == NSNotFound) {
                [resultsDictionary setObject:@"" forKey:kvp];
            } else {
                NSString *key = [kvp substringToIndex:equalsSignLocation];
                NSString *value = [kvp substringFromIndex:equalsSignLocation+1];
                [resultsDictionary setObject:value forKey:key];
            }
        }
        success([NSDictionary dictionaryWithDictionary:resultsDictionary]);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
    op.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    // Schedule for execution
    [[NSOperationQueue mainQueue] addOperation:op];
}

- (void)chargeCustomer:(MOTransaction*)transaction
               success:(void (^)(MOTransaction* transaction))success
               failure:(void (^)(NSError *error))failure {
    NSDictionary *parameters =
    @{
      kXPMerchantID:                [self decryptedMerchantId],
      kXPMerchantKey:               [self decryptedMerchantKey],
      kXPClerkID:                   transaction.clerkId,
      kXPTransactionAmount:         [NSNumber numberWithDouble:transaction.totalAmount],
      kXPSubtotalAmount:            [NSNumber numberWithDouble:transaction.subTotalAmount],
      kXPTaxAmount:                 [NSNumber numberWithDouble:transaction.taxAmount],
      kXPDiscountAmount:            [NSNumber numberWithDouble:transaction.discountAmount],
      kXPCustomerID:                transaction.customerId,
      kXPTransactionType:           @"AddCustomerCCCharge",
      };
    
    // Create the request
    NSError *error;
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"POST" URLString:kXPUrl parameters:parameters error:&error];
    if (request == nil) {
        failure(error);
        return;
    }
    // Create operation
    AFHTTPRequestOperation *op = [[AFHTTPRequestOperationManager manager] HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *results = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSArray *values = [results componentsSeparatedByString:@"&"];
        for (NSString *kvp in values) {
            NSUInteger equalsSignLocation = [kvp rangeOfString:@"="].location;
            if (equalsSignLocation == NSNotFound)
                continue;
            
            NSString *key = [kvp substringToIndex:equalsSignLocation];
            NSString *value = [kvp substringFromIndex:equalsSignLocation+1];
            if ([key isEqualToString:kXPTransactionID]) {
                transaction.transactionID = value;
            } else if ([key isEqualToString:kXPStatusID]) {
                transaction.statusCode = value;
            } else if ([key isEqualToString:kXPResponseCode]) {
                transaction.responseCode = value;
            } else if ([key isEqualToString:kXPResponseMessage]) {
                transaction.responseMessage = value;
            } else if ([key isEqualToString:kXPAuthorizationCode]) {
                transaction.authorizationCode = value;
            } else if ([key isEqualToString:kXPPostedDate]) {
                transaction.postedDate = value;
            }
        }
        
        success(transaction);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
    op.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    // Schedule for execution
    [[NSOperationQueue mainQueue] addOperation:op];
}

@end
