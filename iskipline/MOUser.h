//
//  SOUser.h
//  SelfOrder
//
//  Created by PC02 on 12/31/13.
//  Copyright (c) 2013 nexmac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MOUser : NSObject

@property (nonatomic, retain) NSString* username;
@property (nonatomic, retain) NSString* password;
@property (nonatomic, retain) NSString* firstname;
@property (nonatomic, retain) NSString* lastname;
@property (nonatomic, retain) NSString* roles;
@property (nonatomic, retain) NSNumber* status;
@property (nonatomic, retain) NSNumber* hidden;

@end
