//
//  MOCheckoutViewController.m
//  iskipline
//
//  Created by Chinh Nguyen on 12/19/14.
//  Copyright (c) 2014 Chinh Nguyen. All rights reserved.
//

#import "MOCheckoutViewController.h"
#import "MOOrderItemTableViewCell.h"
#import "MOOrderItemHeaderTableViewCell.h"
#import "MOPaymentViewController.h"

@interface MOCheckoutViewController () {
    NSMutableDictionary *_groupedOrderItems;
//    NSMutableDictionary *printerListCates;
    BOOL isOrderPadMode;
}

@end

@implementation MOCheckoutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    isOrderPadMode = [userDefaults boolForKey:kOMIsOrderPadMode];
    NSString *buttonTitle = isOrderPadMode ? @"Print" : @"Continue";
    [self.mBtnContinue setTitle:buttonTitle forState:UIControlStateNormal];
    if (self.order && self.order.orderItems) {
        // Group orders by category
        _groupedOrderItems = [NSMutableDictionary new];
        
        NSArray *orderItems = [self.order.orderItems allValues];
        for (int i = 0; i < orderItems.count; i++) {
            NSDictionary *orderItem = [orderItems objectAtIndex:i];
            NSString *cat = orderItem[@"groupModelNameItem"];
            
            NSMutableArray *items = _groupedOrderItems[cat];
            if (items) {
                [items addObject:orderItem];
            } else {
                [_groupedOrderItems setValue:[NSMutableArray arrayWithObject:orderItem] forKey:cat];
            }
        }
        
        double subtotal = 0.0;
        double tax = 0.0;
        [self.order calculateTotalDetail:&subtotal andTax:&tax];
        
        self.subtotalLabel.text = [NSString stringWithFormat:@"$%.2f", subtotal];
        self.taxLabel.text = [NSString stringWithFormat:@"$%.2f", tax];
        
        int discountRate = [self.store discount];
        double discount = 0.0;
        if (discountRate > 0) {
            discount = subtotal * discountRate / 100.0;
            self.disountInfoLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Discount %d%%:", @""), discountRate];
            self.discountLabel.text =[NSString stringWithFormat:@"$%.2f", discount];
        } else {
            self.disountInfoLabel.text = NSLocalizedString(@"No Discount:", @"");
            self.discountLabel.text =[NSString stringWithFormat:@"$%.2f", 0.0];
        }
        
        self.totalLabel.text = [NSString stringWithFormat:@"$%.2f", (subtotal + tax - discount)];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showPayment"]) {
        MOPaymentViewController *destinationVC = segue.destinationViewController;
        destinationVC.store = self.store;
        destinationVC.order = self.order;
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSString *category = [_groupedOrderItems.allKeys objectAtIndex:section];
    NSArray *orderItems = _groupedOrderItems[category];
    return orderItems.count;
}

#define kMOOrderItemHeight 36
#define kMOOrderItemModifierHeight 36
#define kMOOrderItemModifierMarginTop 0

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MOOrderItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"OrderItemCell"];
    
    NSString *category = [_groupedOrderItems.allKeys objectAtIndex:indexPath.section];
    NSArray *orderItems = _groupedOrderItems[category];
    NSDictionary *orderItem = [orderItems objectAtIndex:indexPath.row];
    
    int quantity = [orderItem[@"quatityItemOrder"] intValue];
    double subtotal = quantity * [orderItem[@"priceItem"] doubleValue];
    
    cell.itemCountLabel.text = [NSString stringWithFormat:@"%d", quantity];
    cell.itemNameLabel.text = orderItem[@"nameItem"];
    cell.itemPriceLabel.text = [NSString stringWithFormat:@"$%.2f", subtotal];
    //    if (indexPath.row % 2 == 0) {
    //    cell.backgroundColor = [UIColor colorWithWhite:0.8 alpha:0.8];
    //    } else {
    //    cell.backgroundColor = [UIColor colorWithWhite:0.1 alpha:0.8];
    //    }
    
    NSArray *orderModifiers = orderItem[@"item"];
    if (orderModifiers) {
        for (int i = 0; i < orderModifiers.count; i++) {
            MOOrderItemTableViewCell *modifierCell = [tableView dequeueReusableCellWithIdentifier:@"OrderModifierCell"];
            NSDictionary *orderModifier = [orderModifiers objectAtIndex:i];
            if ([@"Note" isEqualToString:orderModifier[@"typeModifierOrder"]]) {
                modifierCell.modifierNoteLabel.hidden = NO;
                modifierCell.modifierNoteLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Note: %@", @""), orderModifier[@"noteModifierOrder"]];
                modifierCell.itemCountLabel.hidden = YES;
                modifierCell.xLabel.hidden = YES;
                modifierCell.itemNameLabel.hidden = YES;
                modifierCell.itemPriceLabel.hidden = YES;
            } else if ([@"Add" isEqualToString:orderModifier[@"typeModifierOrder"]]) {
                modifierCell.modifierNoteLabel.hidden = YES;
                modifierCell.modifierNoteLabel.text = @"";
                modifierCell.itemCountLabel.hidden = NO;
                modifierCell.xLabel.hidden = NO;
                modifierCell.itemNameLabel.hidden = NO;
                modifierCell.itemPriceLabel.hidden = NO;
                
                modifierCell.itemCountLabel.text = [NSString stringWithFormat:@"%d", quantity];
                modifierCell.itemNameLabel.text = orderModifier[@"nameModifier"];
                double modifierSubtotal = quantity * [orderModifier[@"priceModifier"] doubleValue];
                modifierCell.itemPriceLabel.text = [NSString stringWithFormat:@"$%.2f", modifierSubtotal];
            }
            //            modifierCell.backgroundColor = [UIColor redColor];
            // Do not transation autoresizing mask to constraints
            // We will do it manually
            modifierCell.translatesAutoresizingMaskIntoConstraints = NO;
            // Add to content view
            [cell.contentView addSubview:modifierCell];
            // Align left with content view
            [cell.contentView addConstraint:[NSLayoutConstraint constraintWithItem:modifierCell attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:cell.contentView attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
            // Align right with content view
            [cell.contentView addConstraint:[NSLayoutConstraint constraintWithItem:modifierCell attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:cell.contentView attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
            // Fixed height
            [cell.contentView addConstraint:[NSLayoutConstraint constraintWithItem:modifierCell attribute:NSLayoutAttributeHeight  relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:0.0 constant:kMOOrderItemModifierHeight]];
            // Align right with content view
            [cell.contentView addConstraint:[NSLayoutConstraint constraintWithItem:modifierCell attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:cell.contentView attribute:NSLayoutAttributeTop multiplier:1.0 constant:(kMOOrderItemHeight + i * (kMOOrderItemModifierMarginTop + kMOOrderItemModifierHeight))]];
        }
    }
    
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _groupedOrderItems.count;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 32;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *category = [_groupedOrderItems.allKeys objectAtIndex:indexPath.section];
    NSArray *orderItems = _groupedOrderItems[category];
    NSDictionary *orderItem = [orderItems objectAtIndex:indexPath.row];
    NSArray *orderModifiers = orderItem[@"item"];
    float height = kMOOrderItemHeight;
    if (orderModifiers) {
        height += orderModifiers.count * (kMOOrderItemModifierMarginTop + kMOOrderItemModifierHeight);
    }
    return height;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    MOOrderItemHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"OrderItemHeaderCell"];
    cell.nameLabel.text = [_groupedOrderItems.allKeys objectAtIndex:section];
    return cell;
}

#pragma mark - IBAction

- (IBAction)mBtnContinue:(id)sender {
    if (!isOrderPadMode) {
        [self performSegueWithIdentifier:@"showPayment" sender:self];
    } else {
        [self printReceiptOrders];
    }
}

#pragma mark - Receipt Processing

- (void)printReceiptOrders {
    // Get categories
    CBLDocument *doc = [self.store.storeDatabase existingDocumentWithID:kMODocCategories];
    if (!doc)  return; // Should never go to here
    // Show loading
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Printing in the background
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        // List of all categories
        NSArray *categories = doc[kMODocCategories];
        // Make printer categories
        NSMutableDictionary* printerGroupedOrderItems = [NSMutableDictionary new];
        NSArray *printerNames = [kMOPrinters allKeys];
        for (NSString *printer in printerNames) {
            NSMutableArray *byPrinterOrderItems = [NSMutableArray new];
            for (NSInteger i = 0; i < _groupedOrderItems.count; i++) {
                // Get the category id
                NSString *category = [_groupedOrderItems.allKeys objectAtIndex:i];
                // Get the category's printer id
                NSString *categoryPrinter = [self printerByCategory:category from:categories];
                // Put to list if name is equal
                if ([printer isEqualToString:categoryPrinter]) {
                    [byPrinterOrderItems addObject:@{category: _groupedOrderItems[category]}];
                }
            }
            // Put to printer list if there is something to print
            if (byPrinterOrderItems.count > 0)
                [printerGroupedOrderItems setObject:byPrinterOrderItems forKey:printer];
        }
        // Print order processing
        if (printerGroupedOrderItems.count == 0) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                [UIAlertView showWithTitle:NSLocalizedString(@"Empty order", @"")
                                   message:NSLocalizedString(@"Please add some item to order before printing.", @"")
                         cancelButtonTitle:NSLocalizedString(@"OK", "")
                         otherButtonTitles:nil
                                  tapBlock:nil];
            });
        } else {
            __block int finishedCount = 0;
            void (^finishOneReceipt)() = ^() {
                finishedCount++;
                if (finishedCount == printerGroupedOrderItems.count) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                        [self performSegueWithIdentifier:@"orderSubmitted" sender:nil];
                    });
                }
            };
            
            [printerGroupedOrderItems enumerateKeysAndObjectsUsingBlock:^(id printerName, id orderItems, BOOL *stop) {
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    [MBProgressHUD hideHUDForView:self.view animated:YES];
//                    [self retryPrint:orderItems finished:finishOneReceipt];
//                });
                [self printToPrinter:printerName items:orderItems finished:finishOneReceipt];
            }];
        }
    });
}

- (void)retryPrint:(NSArray*)orderItems finished:(void (^)())finished {
    MOSelectPrinterView *selectPrinterView = [[MOSelectPrinterView alloc] initWithFrame:self.view.bounds title:NSLocalizedString(@"Select Printer", @"")];
    [self.view addSubview:selectPrinterView];
    selectPrinterView.printerSelected = ^(NSString *printerName) {
        [self printToPrinter:printerName items:orderItems finished:finished];
    };
    selectPrinterView.skip = ^() {
        if (finished) finished();
    };
    [selectPrinterView showFromPoint:self.view.bounds.origin];
}

- (void)printToPrinter:(NSString*)printer items:(NSArray*)orderItems finished:(void (^)())finished {
    [MOReceiptPrinter printReceipt:printer items:orderItems table:self.tableNumber cart:self.order store:self.store success:^(NSString *printer) {
        finished();
    } failure:^(NSString *printer, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSString *message = [NSString stringWithFormat:NSLocalizedString(@"Could not print the receipt to printer %@", @""), printer];
            [UIAlertView showWithTitle:NSLocalizedString(@"Print failed", @"")
                               message:message
                     cancelButtonTitle:NSLocalizedString(@"Skip", "")
                     otherButtonTitles:@[NSLocalizedString(@"Reroute", "")]
                              tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                  if (buttonIndex == 0) {
                                      // Skip ~ finish
                                      finished();
                                  } else {
                                      [self retryPrint:orderItems finished:finished];
                                  }
                              }];
        });
    }];
}

#pragma mark - Utils

- (NSString *)printerByCategory:(NSString *)categoryName from:(NSArray*)categories {
    for (NSDictionary *dictionary in categories) {
        if ([dictionary[@"name1"] isEqualToString:categoryName]) {
            return dictionary[@"printer_id"];
        }
    }
    return nil;
}

@end

@implementation MOSelectPrinterView {
    NSDictionary *_printers;
    UITableView *_tableView;
    UIView *_actionView;
    UIButton *_skipButton;
    UIButton *_printButton;
    UIView *_sep1;
    UIView *_sep2;
    NSInteger _selectedRow;
}

- (instancetype)initWithFrame:(CGRect)frame title:(NSString *)title {
    if (self = [super initWithFrame:frame]) {
        _printers = kMOPrinters;
        self.headerLabel.text = title;
        
        self.margin = UIEdgeInsetsMake(80.0f, 40.0f, 80.0f, 40.0f);
        self.padding = UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f);
        self.contentColor = [UIColor whiteColor];
        self.shouldBounce = NO;
        self.cornerRadius = 5.0f;
        self.contentView.clipsToBounds = YES;
        
//        [self.actionButton setTitle:NSLocalizedString(@"Print", @"") forState:UIControlStateNormal];
        
        self.headerLabel.text = title;
        self.headerLabel.font = [UIFont boldSystemFontOfSize:16];
        self.headerLabel.backgroundColor = [UIColor clearColor];
        self.headerLabel.textColor = [UIColor blackColor];
        self.headerLabel.shadowColor = [UIColor clearColor];
        self.headerLabel.shadowOffset = CGSizeMake(0, 0);
        self.headerLabel.textAlignment = NSTextAlignmentCenter;

        self.titleBar.gradientStyle = UAGradientBackgroundStyleLinear;
        CGFloat colors[8] = {1, 1, 1, 1, 1, 1, 1, 1 };
        [self.titleBar setColorComponents:colors];
        
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [self.contentView addSubview:_tableView];
        
        _actionView = [[UIView alloc] initWithFrame:CGRectZero];
        
        _skipButton = [[UIButton alloc] initWithFrame:CGRectZero];
        _skipButton.titleLabel.font = [UIFont systemFontOfSize:16];
        [_skipButton setTitle:NSLocalizedString(@"Skip", @"") forState:UIControlStateNormal];
        [_skipButton setTitleColor:[UIColor colorWithRed:0.0 green:122.0/255.0 blue:1.0 alpha:1.0] forState:UIControlStateNormal];
        [_skipButton addTarget:self action:@selector(skipDidTouch) forControlEvents:UIControlEventTouchUpInside];
        [_actionView addSubview:_skipButton];
        
        _printButton = [[UIButton alloc] initWithFrame:CGRectZero];
        _printButton.titleLabel.font = [UIFont boldSystemFontOfSize:16];
        [_printButton setTitle:NSLocalizedString(@"Print", @"") forState:UIControlStateNormal];
        [_printButton setTitleColor:[UIColor colorWithRed:0.0 green:122.0/255.0 blue:1.0 alpha:1.0] forState:UIControlStateNormal];
        [_printButton addTarget:self action:@selector(printDidTouch) forControlEvents:UIControlEventTouchUpInside];
        [_actionView addSubview:_printButton];
        
        _sep1 = [[UIView alloc] initWithFrame:CGRectZero];
        _sep1.backgroundColor = [UIColor colorWithWhite:0.8f alpha:1.0f];
        [_actionView addSubview:_sep1];
        
        _sep2 = [[UIView alloc] initWithFrame:CGRectZero];
        _sep2.backgroundColor = [UIColor colorWithWhite:0.8f alpha:1.0f];
        [_actionView addSubview:_sep2];
        
        [self.contentView addSubview:_actionView];
        
        // Button's highlighted image
        CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
        UIGraphicsBeginImageContext(rect.size);
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGContextSetFillColorWithColor(context, [[UIColor colorWithWhite:0.9f alpha:0.9f] CGColor]);
        CGContextFillRect(context, rect);
        UIImage *buttonHighlightedBackgroundImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        [_skipButton setBackgroundImage:buttonHighlightedBackgroundImage forState:UIControlStateHighlighted];
        [_printButton setBackgroundImage:buttonHighlightedBackgroundImage forState:UIControlStateHighlighted];

        [self.closeButton removeFromSuperview];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGSize size = self.contentView.frame.size;
    _tableView.frame = CGRectMake(8.0f, 0.0f, size.width-8.0f, size.height-44.0f);
    _actionView.frame = CGRectMake(0.0f, size.height-44, size.width, 44.0f);
    _skipButton.frame = CGRectMake(0.0f, 0.0f, (size.width)/2, 44.0f);
    _printButton.frame = CGRectMake((size.width)/2, 0.0f, (size.width)/2, 44.0f);
    _sep1.frame = CGRectMake((size.width)/2, 0.0f, 1, 44.0f);
    _sep2.frame = CGRectMake(0.0f, 0.0f, size.width, 1.0f);
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _printers.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [UITableViewCell new];
    cell.textLabel.text = [_printers.allKeys objectAtIndex:indexPath.row];
    if (indexPath.row == _selectedRow) {
        cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"check.png"]];
    } else {
        cell.accessoryView = nil;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    _selectedRow = indexPath.row;
    [tableView reloadData];
}

- (void)skipDidTouch {
    if (self.skip) self.skip();
    [self hide];
}

- (void)printDidTouch {
    if (self.printerSelected) self.printerSelected([_printers.allKeys objectAtIndex:_selectedRow]);
    [self hide];
}

@end
