//
//  MOModifierViewController.h
//  iskipline
//
//  Created by Chinh Nguyen on 12/18/14.
//  Copyright (c) 2014 Chinh Nguyen. All rights reserved.
//

#import "MOOrderViewController.h"

@interface MOModifierViewController : MOOrderViewController<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>

#pragma mark - IBOutlet

@property (weak, nonatomic) IBOutlet UILabel *labelCheckOut;
@property (weak, nonatomic) IBOutlet UILabel *itemNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *itemDescriptionLabel;

#pragma mark - Propertises

@property (weak, nonatomic) NSDictionary *category;
@property (weak, nonatomic) NSDictionary *item;

@property (weak, nonatomic) IBOutlet UITextField *noteTextField;

@end
