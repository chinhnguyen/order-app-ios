//
//  MOPrinters.h
//  iskipline
//
//  Created by TIENPHAM on 9/2/15.
//  Copyright (c) 2015 Chinh Nguyen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ePOS-Print.h"
#import "MOCart.h"
#import "MOStore.h"
#import "SSIDFinder.h"

#define SEND_TIMEOUT        10 * 1000
#define MAX_TEXT_IN_LINE    48

#define kMOPrinters @{@"RP1":@"151", @"RP2":@"152", @"RP3":@"153", @"KP1":@"161", @"KP2":@"162", @"KP3":@"163", }

@interface MOReceiptPrinter : NSObject

#pragma mark - Static

+ (NSString*)printerAddressFor:(NSString*)name;

+ (void)printReceipt:(NSString*)printerName items:(NSArray *)orderItems table:(NSString *)table cart:(MOCart *)cart store:(MOStore *)store success:(void (^)(NSString* printer))success failure:(void (^)(NSString* printer, NSError *error))failure;

@end
