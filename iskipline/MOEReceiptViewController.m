//
//  MOEReceiptViewController.m
//  iskipline
//
//  Created by Chinh Nguyen on 12/23/14.
//  Copyright (c) 2014 Chinh Nguyen. All rights reserved.
//

#import "MOEReceiptViewController.h"
#import "MOAppUser.h"

#pragma mark - MOEReceiptViewController

@interface MOEReceiptViewController ()

@end

@implementation MOEReceiptViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"tableContainer"]) {
        MOEReceiptTableViewController *destinationVC = segue.destinationViewController;
        destinationVC.name = self.name;
        destinationVC.phone = self.phone;
        destinationVC.store = self.store;
        destinationVC.order = self.order;
    }
}

@end

#pragma mark - MOEReceiptTableViewController

@interface MOEReceiptTableViewController ()

@end

@implementation MOEReceiptTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.emailText.layer.borderColor = [UIColor whiteColor].CGColor;
    self.emailText.layer.borderWidth = 1;
    self.emailText.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Your email", @"") attributes:@{ NSForegroundColorAttributeName:[UIColor colorWithWhite:0.5f alpha:0.8f]}];
    
    self.submitButton.layer.cornerRadius = 5;
    self.submitButton.layer.borderWidth = 1;
    self.submitButton.layer.borderColor = [UIColor whiteColor].CGColor;
    
    UILabel *titleLabel = [UILabel new];
    titleLabel.font = [UIFont fontWithName:@"Myriad Pro" size:16.0];
    titleLabel.text = NSLocalizedString(@"Please send me the promotions/coupons/discounts", @"");
    titleLabel.numberOfLines = 2;
    titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.frame = CGRectMake(0, 0, self.promotionButton.frame.size.width, self.promotionButton.frame.size.height);
    titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.promotionButton addSubview:titleLabel];
    // Top constraint
    [self.promotionButton addConstraint:[NSLayoutConstraint constraintWithItem:titleLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.promotionButton attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    // Bottom constraint
    [self.promotionButton addConstraint:[NSLayoutConstraint constraintWithItem:titleLabel attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.promotionButton attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    // Trailing constraint
    [self.promotionButton addConstraint:[NSLayoutConstraint constraintWithItem:titleLabel attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.promotionButton attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    // Leading constraint
    [self.promotionButton addConstraint:[NSLayoutConstraint constraintWithItem:titleLabel attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.promotionButton attribute:NSLayoutAttributeLeading multiplier:1.0 constant:30.0]];
    
    // Select by default
    self.emailButton.selected = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBAction

- (IBAction)textButtonDidTouch:(id)sender {
    // Touch effect
    self.textButton.backgroundColor = [UIColor colorWithWhite:0.5 alpha:0.5];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.textButton.backgroundColor = [UIColor clearColor];
    });
    
    self.textButton.selected = YES;
    self.emailButton.selected = NO;
    [self.emailText resignFirstResponder];
}

- (IBAction)emailButtonDidTouch:(id)sender {
    // Touch effect
    self.emailButton.backgroundColor = [UIColor colorWithWhite:0.5 alpha:0.5];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.emailButton.backgroundColor = [UIColor clearColor];
    });
    
    self.textButton.selected = NO;
    self.emailButton.selected = YES;
    [self.emailText becomeFirstResponder];
}

- (IBAction)emailTextEditingDidBegin:(id)sender {
    self.textButton.selected = NO;
    self.emailButton.selected = YES;
}

- (IBAction)promotionButtonDidTouch:(id)sender {
    // Touch effect
    self.promotionButton.backgroundColor = [UIColor colorWithWhite:0.5 alpha:0.5];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.promotionButton.backgroundColor = [UIColor clearColor];
    });
    
    self.promotionButton.selected = !self.promotionButton.selected;
}

- (IBAction)submitDidTouch:(id)sender {
    
    NSString *email = @"";
    if (self.emailButton.selected) {
        email = trim(self.emailText.text);
        if (email.length == 0) {
            [UIAlertView showWithTitle:NSLocalizedString(@"Missing input", @"") message:NSLocalizedString(@"Please enter a valid email for receiving receipt.", @"") cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
            return;
        }
    }
    
    // Hide keyboard
    [self.emailText resignFirstResponder];
    
    if (!self.store || !self.order) {
        [self.view makeToast:NSLocalizedString(@"Missing store or order", @"")];
    }
    
    // Create the user
    MOAppUser *user = [MOAppUser new];
    user.phone = self.phone;
    
    // Set name
    if (self.name && self.name.length > 0) {
        NSArray *words = [self.name componentsSeparatedByString:@" "];
        NSArray *lastnameWords = [words subarrayWithRange:NSMakeRange(1, words.count-1)];
        user.firstname = [words objectAtIndex:0];
        user.lastname = [lastnameWords componentsJoinedByString:@" "];
    }
    
    // Set email
    user.email = email;
    
    // Create the transaction information
    MOTransaction *transaction = [MOTransaction new];
    double subtotal = 0, tax = 0, discount = 0;
    [self.order calculateTotalDetail:&subtotal andTax:&tax];
    
    // Calculate discount
    int discountRate = [self.store discount];
    if (discountRate > 0) {
        discount = subtotal * discount / 100.0;
    }
    
    // Set value to transaction
    transaction.taxAmount = tax;
    transaction.subTotalAmount = subtotal;
    transaction.discountAmount = discount;
    transaction.totalAmount = subtotal - discount + tax;
    
    // Type of ereceipt
    NSString *ereceipt = self.emailButton.selected ? @"email" : @"sms";
    
    // Now make the order
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self.store makeOrder:self.order.orderItems.allValues forUser:user withTransaction:transaction andPickupTime:nil subsribe:self.promotionButton.selected ereceipt:ereceipt success:^(MOOrder *order) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        // Inform user
        [UIAlertView showWithTitle:NSLocalizedString(@"Order submitted", @"")
                           message:NSLocalizedString(@"Thanks for ordering, please wait while your order is being served.", @"")
                 cancelButtonTitle:NSLocalizedString(@"OK", @"")
                 otherButtonTitles:nil
                          tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                              // Now restart ordering
                              [self performSegueWithIdentifier:@"restartOrdering" sender:self];
                 }];
    } failure:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        [UIAlertView showWithTitle:NSLocalizedString(@"Order failed", @"")
                           message:NSLocalizedString(@"Sorry! The order was not submitted successfully.", @"")
                 cancelButtonTitle:NSLocalizedString(@"Close", @"")
                 otherButtonTitles:nil
                          tapBlock:nil];
    } receiptSuccess:^{
        // Do nothing
    } receiptFailure:^(NSError *error) {
        [self.view makeToast:NSLocalizedString(@"Failed sending receipt", @"")];
    }];
}

@end

