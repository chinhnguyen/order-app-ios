//
//  NSString+Extra.m
//  iskipline
//
//  Created by Chinh Nguyen on 1/2/15.
//  Copyright (c) 2015 Chinh Nguyen. All rights reserved.
//

#import "NSString+Extra.h"
#import <CommonCrypto/CommonDigest.h>

@implementation NSString(Extra)

- (NSString*)padLeft:(NSString*)stringToPad forLength:(NSUInteger)length {
    NSUInteger paddingLength = length - self.length;
    if (paddingLength > 0) {
        NSString *paddedString = [@"" stringByPaddingToLength:paddingLength withString:stringToPad startingAtIndex:0];
        return [NSString stringWithFormat:@"%@%@", paddedString, self];
    }
    
    return self;
}

- (NSString*)padRight:(NSString*)stringToPad forLength:(NSUInteger)length {
    NSUInteger paddingLength = length - self.length;
    if (paddingLength > 0) {
        NSString *paddedString = [@"" stringByPaddingToLength:paddingLength withString:stringToPad startingAtIndex:0];
        return [NSString stringWithFormat:@"%@%@", self, paddedString];
    }
    
    return self;
}

- (NSString*)md5 {
    // Create pointer to the string as UTF8
    const char *ptr = [self UTF8String];
    
    // Create byte array of unsigned chars
    unsigned char digest[CC_MD5_DIGEST_LENGTH];
    
    // Create 16 byte MD5 hash value, store in buffer
    CC_MD5(ptr, (CC_LONG)strlen(ptr), digest);
    
    // Convert MD5 value in the buffer to NSString of hex values
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x",digest[i]];
    
    return output;
}

@end
