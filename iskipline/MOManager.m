//
//  MOStoreDb.m
//  iskipline
//
//  Created by Chinh Nguyen on 12/15/14.
//  Copyright (c) 2014 Chinh Nguyen. All rights reserved.
//

#import "MOManager.h"
#import <AFNetworking.h>
#import <CommonCrypto/CommonDigest.h>

@implementation MOManager {
    CBLReplication* _ssidPull;
    CBLReplication* _storesPull;
}

#pragma mark - Static fields

static MOManager *_instance;

+ (MOManager *)sharedInstance {
    if (!_instance)
        _instance = [MOManager new];
    return _instance;
}


#pragma mark - Meta Databases

- (void)startPullMetaDatabases:(NSError**)outError {
    CBLManager *manager = [CBLManager sharedInstance];
    
    self.ssidListDb = [manager databaseNamed:kMODbSsid error:outError];
    if (!self.ssidListDb)
        return;
    
    self.storesDb = [manager databaseNamed:kMODbStores error:outError];
    if (!self.storesDb)
        return;
    
    _ssidPull = [self.ssidListDb createPullReplication:kMOSsidSyncURL];
    _ssidPull.continuous = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ssidReplicationChanged:) name: kCBLReplicationChangeNotification object:_ssidPull];
    [_ssidPull start];
    
    _storesPull = [self.storesDb createPullReplication:kMOStoresSyncURL];
    _storesPull.continuous = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(storesReplicationChanged:) name:kCBLReplicationChangeNotification object:_storesPull];
    [_storesPull start];
}

- (void)ssidReplicationChanged:(NSNotification*)n {
    [[NSNotificationCenter defaultCenter] postNotificationName:kMOSsidReplicationChangedNofification object:n.object];
}

- (void)storesReplicationChanged:(NSNotification*)n {
    [[NSNotificationCenter defaultCenter] postNotificationName:kMOStoresReplicationChangedNofification object:n.object];
}

#pragma mark - Local Stores

- (NSArray *)loadAllStores {
    CBLQuery *query = [_storesDb createAllDocumentsQuery];
    NSError *err;
    CBLQueryEnumerator *enumerator = [query run:&err];
    NSMutableArray* stores = [NSMutableArray new];
    if (enumerator) {
        for (int i = 0; i < enumerator.count; i++) {
            CBLDocument *d = [enumerator rowAtIndex:i].document;
            [stores addObject:[MOStore modelForDocument:d]];
        }
    }
    return stores;
}

- (NSDictionary *)loadAvailableStatesCities {
    CBLQuery* query = [_storesDb createAllDocumentsQuery];
    NSError* err;
    CBLQueryEnumerator *enumerator = [query run:&err];
    NSMutableDictionary *stateCitiesMap = [NSMutableDictionary new];
    if (enumerator) {
        for (int i = 0; i < enumerator.count; i++) {
            CBLDocument* d = [enumerator rowAtIndex:i].document;
            MOStore* s = [MOStore modelForDocument:d];
            if (s.state && s.state.length > 0 && s.city && s.city.length > 0) {
                NSMutableArray* cities = [stateCitiesMap objectForKey:s.state];
                if (cities) {
                    if ([cities indexOfObject:s.city] == NSNotFound) {
                        [cities addObject:s.city];
                    }
                } else {
                    cities = [NSMutableArray new];
                    [cities addObject:s.city];
                    [stateCitiesMap setObject:cities forKey:s.state];
                }
            }
        }
    }
    return stateCitiesMap;
}

- (NSArray *)findStoreByState:(NSString*)state andCity:(NSString*)city {
    CBLQuery *query = [_storesDb createAllDocumentsQuery];
    NSError *err;
    CBLQueryEnumerator *enumerator = [query run:&err];
    NSMutableArray *stores = [NSMutableArray new];
    
    if (enumerator) {
        for (int i = 0; i < enumerator.count; i++) {
            CBLDocument *d = [enumerator rowAtIndex:i].document;
            MOStore *s = [MOStore modelForDocument:d];
            if (s.state && [s.state isEqualToString:state] &&
                s.city && [s.city isEqualToString:city]) {
                [stores addObject:s];
            }
        }
    }
    
    return stores;
}

- (MOStore *)findStoreByAccount:(NSString *)account {
    CBLQuery *query = [_storesDb createAllDocumentsQuery];
    NSError *err;
    CBLQueryEnumerator *enumerator = [query run:&err];
    if (enumerator) {
        for (int i = 0; i < enumerator.count; i++) {
            CBLDocument *d = [enumerator rowAtIndex:i].document;
            MOStore *s = [MOStore modelForDocument:d];
            if (s.account && [s.account isEqualToString:account]) {
                return s;
            }
        }
    }
    return nil;
}

- (MOStore *)findStoreBySSID:(NSString *)nameSSID {
    // Get current SSID database name by SSID
    NSString *databaseSSID = nil;
    CBLQuery *queryWifiSSID = [self.ssidListDb createAllDocumentsQuery];
    NSError *errWifiSSID;
    CBLQueryEnumerator *enumWifiSSID = [queryWifiSSID run:&errWifiSSID];
    if (enumWifiSSID && enumWifiSSID.count > 1) {
        CBLDocument *docs = [enumWifiSSID rowAtIndex:2].document;
        NSMutableDictionary *itemsProperties = [docs.properties mutableCopy];
        NSArray *listWifies = itemsProperties[kMODbSsid];
        for (NSUInteger i = 0; i < listWifies.count; i++) {
            NSMutableDictionary *itemWifi = [listWifies objectAtIndex:i];
            NSString *wifiSSID = itemWifi[@"ssid"];
            if ([wifiSSID isEqualToString:nameSSID]) {
                databaseSSID = itemWifi[@"s1_db"];
            }
        }
    }
#ifdef TESTING
    databaseSSID = @"acct_ca94568leedublin5496";
#endif
    if ((nil == databaseSSID) || [databaseSSID isEqualToString:@""]) {
        return nil;
    }
    CBLQuery *query = [_storesDb createAllDocumentsQuery];
    NSError *err;
    CBLQueryEnumerator *enumerator = [query run:&err];
    if (enumerator) {
        for (int i = 0; i < enumerator.count; i++) {
            CBLDocument *d = [enumerator rowAtIndex:i].document;
            MOStore *store = [MOStore modelForDocument:d];
            if (store.account && [store.account isEqualToString:databaseSSID]) {
                return store;
            }
        }
    }
    return nil;
}

#pragma mark - Remote stores

- (void)loginHost:(NSString *)host store:(NSString *)account user:(NSString *)user password:(NSString *)password success:(void (^)(id user, id store))success failure:(void (^)(NSError *error))failure {
    if (!host || host.length == 0 || !account || account.length == 0 || !user || user.length == 0 || !password || password.length == 0)
        return;
    NSString *loginViewURL = storeLoginViewURLString(host, account, user);
    [self get:loginViewURL params:nil success:^(NSInteger statusCode, id responseObject) {
        NSArray *users = [responseObject valueForKey:@"rows"];
        if (users && users.count > 0) {
            id obj = [users objectAtIndex:0];
            id user = [obj valueForKey:@"value"];
            if (user) {
                NSString *p = [user valueForKey:@"password"];
                if ([password isEqualToString:p]) {
                    NSString *role = [user valueForKey:@"roles"];
                    if ([@"admin" caseInsensitiveCompare:role] == NSOrderedSame ||
                        [@"owner" caseInsensitiveCompare:role] == NSOrderedSame ||
                        [@"manager" caseInsensitiveCompare:role] == NSOrderedSame ||
                        [@"supervisor" caseInsensitiveCompare:role] == NSOrderedSame) {
                        
                        [self loadStore:account host:host success:^(id store) {
                            if (store) {
                                success(user, store);
                            } else {
                                NSError *error = [NSError errorWithDomain:@"Configuration" code:500 userInfo:@{NSLocalizedDescriptionKey: @"Store is not properly configure (missing in store_location)."}];
                                failure(error);
                            }
                        } failure:failure];
                        return;
                    }
                }
            }
        }
        
        // All other cases are considered bad credential
        NSError *error = [NSError errorWithDomain:@"Authentication" code:401 userInfo:@{NSLocalizedDescriptionKey: @"Invalid credential."}];
        failure(error);
    } failure:^(NSInteger statusCode, NSError *error) {
        failure(error);
    }];
}

- (void)loadStore:(NSString *)account  host:(NSString *)host success:(void (^)(id store))success failure:(void (^)(NSError *error))failure {
    if (!host || host.length == 0 || !account || account.length == 0)
        return;
    NSString *storeByAccountViewURL = storeByAccountViewURLString(host, account);
    [self get:storeByAccountViewURL params:nil success:^(NSInteger statusCode, id responseObject) {
        NSArray *stores = [responseObject valueForKey:@"rows"];
        if (stores && stores.count > 0) {
            id obj = [stores objectAtIndex:0];
            success([obj valueForKey:@"value"]);
        } else {
            success(nil);
        }
    } failure:^(NSInteger statusCode, NSError *error) {
        failure(error);
    }];
}

#pragma mark - App Users

- (void)loginAppUser:(NSString *)phone password:(NSString *)password success:(void (^)(id user))success failure:(void (^)(NSError *error))failure {
    if (!phone || phone.length == 0 || !password || password.length == 0)
        return;
    
    NSString *appuserLoginURL = appuserURLString(phone);
    [self get:appuserLoginURL params:nil success:^(NSInteger statusCode, id responseObject) {
        if ([[password md5] localizedCaseInsensitiveCompare:responseObject[@"password"]] == NSOrderedSame) {
            success(responseObject);
        } else {
            NSError *error = [NSError errorWithDomain:@"Authentication" code:401 userInfo:@{NSLocalizedDescriptionKey:@"Invalid credential."}];
            failure(error);
        }
    } failure:^(NSInteger statusCode, NSError *error) {
        failure(error);
    }];
}

- (void)putAppUser:(MOAppUser *)user withRev:(NSString *)rev forStore:(MOStore *)store ccinfo:(NSDictionary *)ccinfo success:(void (^)(NSDictionary *user))success failure:(void (^)(NSError *error))failure {
    if (!user || !user.phone || user.phone.length == 0)
        return;
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    dateFormatter.dateFormat = @"MM/dd/yyyy hh:mm a";
    NSString *date = [dateFormatter stringFromDate:[NSDate date]];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithDictionary:
    @{
      @"_id":                     user.phone,
      @"firstname":               user.firstname,
      @"lastname":                user.lastname,
      @"address":                 user.address,
      @"phone":                   user.phone,
      @"zip":                     user.zip,
      @"email":                   user.email,
      @"password":                user.password,
      @"date_created":            date,
      @"date_changed":            date,
      @"account_links":           store ? @{store.store: store.account} : @"",
      kXPCustomerID:              ccinfo[kXPCustomerID],
      kXPAccountNumber:           ccinfo[kXPAccountNumber],
      kXPCardNumber:              ccinfo[kXPCardNumber],
      @"AvailablePaymentTypes":   @"AC",
      }];
    if (rev && rev.length > 0) {
        [parameters setObject:rev forKey:@"_rev"];
    }
    
    NSString *appuserURL = appuserURLString(user.phone);
    [self put:appuserURL object:parameters success:^(NSInteger statusCode, id responseObject) {
        success(parameters);
    } failure:^(NSInteger statusCode, NSError *error) {
        if (statusCode == 409) {
            failure([NSError errorWithDomain:@"AppUser" code:409 userInfo:@{NSLocalizedDescriptionKey: @"Phone number is already registered as a user."}]);
        } else {
            failure(error);
        }
    }];
}

#pragma mark - Rest Api

- (void)put:(NSString*)url
     object:(NSDictionary*)data
    success:(void (^)(NSInteger statusCode, id responseObject))success
    failure:(void (^)(NSInteger statusCode, NSError *error))failure {
    
    // Create the request
    NSError *error;
    NSMutableURLRequest *request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"PUT" URLString:url parameters:data error:&error];
    if (request == nil) {
        failure(-1, error);
        return;
    }
    [request setValue:@"text/plain" forHTTPHeaderField:@"Accept"];
    // Set the required HTTP headers
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", kMODatabaseUser, kMODatabasePass];
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength]];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    // Create operation
    AFHTTPRequestOperation *op = [[AFHTTPRequestOperationManager manager] HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(operation.response.statusCode, responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(operation.response.statusCode, error);
    }];
    // Schedule for execution
    [[NSOperationQueue mainQueue] addOperation:op];
}

- (void)post:(NSString*)url
      object:(NSDictionary*)data
     success:(void (^)(NSInteger statusCode, id responseObject))success
     failure:(void (^)(NSInteger statusCode, NSError *error))failure {
    
    // Create the request
    NSError *error;
    NSMutableURLRequest *request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:url parameters:data error:&error];
    if (request == nil) {
        failure(-1, error);
        return;
    }
    [request setValue:@"text/plain" forHTTPHeaderField:@"Accept"];
    // Set the required HTTP headers
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", kMODatabaseUser, kMODatabasePass];
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength]];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    // Create operation
    AFHTTPRequestOperation *op = [[AFHTTPRequestOperationManager manager] HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(operation.response.statusCode, responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(operation.response.statusCode, error);
    }];
    // Schedule for execution
    [[NSOperationQueue mainQueue] addOperation:op];
}

- (void)get:(NSString*)url
     params:(NSDictionary*)params
    success:(void (^)(NSInteger statusCode, id responseObject))success
    failure:(void (^)(NSInteger statusCode, NSError *error))failure {
    // Create the request
    NSError *error;
    NSMutableURLRequest *request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"GET" URLString:url parameters:params error:&error];
    if (request == nil) {
        failure(-1, error);
        return;
    }
    [request setValue:@"text/plain" forHTTPHeaderField:@"Accept"];
    // Set the required HTTP headers
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", kMODatabaseUser, kMODatabasePass];
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength]];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    // Create operation
    AFHTTPRequestOperation *op = [[AFHTTPRequestOperationManager manager] HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(operation.response.statusCode, responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(operation.response.statusCode, error);
    }];
    // Schedule for execution
    [[NSOperationQueue mainQueue] addOperation:op];
}




@end
