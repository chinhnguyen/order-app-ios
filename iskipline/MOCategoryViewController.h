//
//  MOCategoryViewController.h
//  iskipline
//
//  Created by Chinh Nguyen on 12/17/14.
//  Copyright (c) 2014 Chinh Nguyen. All rights reserved.
//

#import "MOOrderViewController.h"

@interface MOCategoryViewController : MOOrderViewController<UITableViewDataSource, UITableViewDelegate>

#pragma mark - IBOutlet

@property (weak, nonatomic) IBOutlet UILabel *labelCheckOut;

#pragma mark - IBAction

- (IBAction)orderCancelled:(UIStoryboardSegue *)unwindSegue;

@end
