//
//  SOItem.h
//  SelfOrder
//
//  Created by PC02 on 12/31/13.
//  Copyright (c) 2013 nexmac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MOItem : NSObject

@property (nonatomic, retain) NSString* ID;
@property (nonatomic, retain) NSString* category;
@property (nonatomic, retain) NSNumber* item_number;
@property (nonatomic, retain) NSString* name1;
@property (nonatomic, retain) NSString* desc;
@property (nonatomic, retain) NSString* keyword;
@property (nonatomic, retain) NSNumber* regular_price;
@property (nonatomic, retain) NSNumber* promo_price;
@property (nonatomic, retain) NSNumber* combo_price;
@property (nonatomic, retain) NSNumber* quantity;
@property (nonatomic, retain) NSNumber* to_kitchen;
@property (nonatomic, retain) NSString* barcode;
@property (nonatomic, retain) NSNumber* togo_tax;
@property (nonatomic, retain) NSNumber* forhere_tax;
@property (nonatomic, retain) NSString* mods;
@property (nonatomic, retain) NSNumber* isShow;


@end
