//
//  AppDelegate.m
//  iskipline
//
//  Created by Chinh Nguyen on 12/15/14.
//  Copyright (c) 2014 Chinh Nguyen. All rights reserved.
//

#import "AppDelegate.h"
#import "MOManager.h"
#import "SSIDFinder.h"
#import "MOReceiptPrinter.h"
#import "MOHomeViewController.h"
#import "MOOrderPadViewController.h"
#import <UIAlertView+Blocks.h>
#import <BWLongTextViewController.h>
#import <BWSelectViewController.h>

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    NSError *err;
    [[MOManager sharedInstance] startPullMetaDatabases:&err];
    if (err) {
        [UIAlertView showWithTitle:NSLocalizedString(@"Critical Error", @"") message:NSLocalizedString(@"Could not init database.", @"") cancelButtonTitle:NSLocalizedString(@"Close", @"") otherButtonTitles:nil tapBlock:nil];
        return NO;
    }
    // Check if app connect Wifi match OP mode
    BOOL isOrderPadMode = [SSIDFinder checkIfSSIDMatchOrderPad];
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *navController;
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if (isOrderPadMode) {
        [userDefaults setBool:YES forKey:kOMIsOrderPadMode];
        [userDefaults synchronize];
        MOOrderPadViewController *orderPadVC = (MOOrderPadViewController *)[mainStoryboard instantiateViewControllerWithIdentifier:@"MOOrderPadViewController"];
        navController = [[UINavigationController alloc] initWithRootViewController:orderPadVC];
        self.window.rootViewController = navController;
    }
    // Else start app normally
    else {
        [userDefaults setBool:NO forKey:kOMIsOrderPadMode];
        [userDefaults synchronize];
        MOHomeViewController *homeVC = (MOHomeViewController *)[mainStoryboard instantiateViewControllerWithIdentifier:@"MOHomeViewController"];
        navController = [[UINavigationController alloc] initWithRootViewController:homeVC];
        self.window.rootViewController = navController;
    }

    [[UITableView appearance] setTableFooterView:[UIView new]];
    
    // iOS 7:
    [[UITableView appearance] setSeparatorInset:UIEdgeInsetsZero];
    [[UITableViewCell appearance] setSeparatorInset:UIEdgeInsetsZero];
    
    // iOS 8:
    if ([UITableView instancesRespondToSelector:@selector(setLayoutMargins:)]) {
        [[UITableView appearance] setLayoutMargins:UIEdgeInsetsZero];
        [[UITableViewCell appearance] setLayoutMargins:UIEdgeInsetsZero];
        [[UITableViewCell appearance] setPreservesSuperviewLayoutMargins:NO];
    }
    [[UITableView appearanceWhenContainedIn:[BWSelectViewController class], nil] setSeparatorInset:UIEdgeInsetsMake(0, 16, 0, 0)];
    [[UITableViewCell appearanceWhenContainedIn:[BWSelectViewController class], nil] setSeparatorInset:UIEdgeInsetsMake(0, 16, 0, 0)];
    [[UITableViewCell appearanceWhenContainedIn:[BWSelectViewController class], nil] setBackgroundColor:[UIColor clearColor]];
    [[UITableViewCell appearanceWhenContainedIn:[BWSelectViewController class], nil] setTintColor:[UIColor whiteColor]];
    [[UILabel appearanceWhenContainedIn:[BWSelectViewController class], nil] setTextColor:[UIColor whiteColor]];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
