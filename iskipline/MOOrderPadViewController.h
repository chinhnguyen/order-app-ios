//
//  MOOrderPadViewController.h
//  iskipline
//
//  Created by TIENPHAM on 8/30/15.
//  Copyright (c) 2015 Chinh Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MOBaseViewController.h"

@interface MOOrderPadViewController : MOBaseViewController {
    MOStore *currentStore;
}

#pragma mark - IBOutlet

@property (weak, nonatomic) IBOutlet UIView *orderTableView;
@property (weak, nonatomic) IBOutlet UITextField *mTFTableNum;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;

#pragma mark - IBAction

- (IBAction)nextScreenButDidTouch:(id)sender;

- (IBAction)orderSubmitted:(UIStoryboardSegue *)unwindSegue;



@end
