//
//  MOOrder.m
//  iskipline
//
//  Created by Chinh Nguyen on 12/18/14.
//  Copyright (c) 2014 Chinh Nguyen. All rights reserved.
//

#import "MOCart.h"

@implementation MOCart

- (id)initWithStore:(MOStore*)store; {
    self = [super init];
    if (self) {
        _orderItems = [NSMutableDictionary new];
        _store = store;
    }
    
    return self;
}

#pragma mark - Events

- (void)notifyCartChanged {
    NSString *status = [self orderSummary];
    [[NSNotificationCenter defaultCenter] postNotificationName:kMOCartUpdatedNotification object:status];
}

#pragma mark - Cart functions

- (NSDictionary*)addItem:(NSDictionary*)item withCategory:(NSDictionary*)category {
    if (item == nil || category == nil)
        return nil;
    NSString *itemId = item[@"id"];
    NSDictionary *orderItem = _orderItems[itemId];
    if (orderItem) {
        int quantity = [orderItem[@"quatityItemOrder"] intValue] + 1;
        [orderItem setValue:[NSNumber numberWithInt:quantity] forKey:@"quatityItemOrder"];
        // Update modifier as well
        NSArray *modifiers = orderItem[@"item"];
        for (int i = 0; i < modifiers.count; i++) {
            NSDictionary *m = modifiers[i];
            [m setValue:[NSNumber numberWithInt:quantity] forKey:@"quantityModifier"];
        }
    } else {
        orderItem = [NSMutableDictionary dictionaryWithDictionary:
                     @{
                       @"priceItem":         [NSNumber numberWithDouble:[item[@"regular_price"] doubleValue]],
                       @"numberItem":        @"",
                       @"colorItem":         category[@"bg_color"],
                       @"textColorItem":     category[@"font_color"] ,
                       @"idItem":            item[@"id"],
                       @"hasModifierItem":   @NO,
                       @"blockItem":         [NSNumber numberWithInt:2],
                       @"hasModifierOrder":  @NO,
                       @"idCategory":        category[@"id"] ,
                       @"quatityItemOrder":  [NSNumber numberWithInt:1],
                       @"groupModelNameItem":displayName(category),
                       @"item":              [NSMutableArray new],
                       @"modifierNumberItem":[NSNumber numberWithInt:0],
                       @"nameItem":          displayName(item),
                       @"typePriceItem":     @"",
                       @"quatityItem":       [NSNumber numberWithInt:[item[@"quantity"] intValue]]
                       }];
        [_orderItems setValue:orderItem forKey:itemId];
    }
    [self notifyCartChanged];
    return orderItem;
}

- (NSDictionary*)removeItem:(NSDictionary*)item {
    if (item == nil)
        return nil;
    
    NSString *itemId = item[@"id"];
    NSDictionary *orderItem = _orderItems[itemId];
    
    if (orderItem) {
        NSDictionary *orderItem = _orderItems[itemId];
        int quantity = [orderItem[@"quatityItemOrder"] intValue] - 1;
        if (quantity == 0) {
            [_orderItems removeObjectForKey:itemId];
        } else {
            [orderItem setValue:[NSNumber numberWithInt:quantity] forKey:@"quatityItemOrder"];
        }
        [self notifyCartChanged];
    }
    return orderItem;
}

- (NSDictionary*)getOrderItem:(NSDictionary*)item {
    if (item == nil)
        return nil;
    NSString *itemId = item[@"id"];
    return _orderItems[itemId];
}

- (BOOL)isEmpty {
    return _orderItems.count == 0;
}

- (void)emptyCart {
    [_orderItems removeAllObjects];
    [self notifyCartChanged];
}

- (NSDictionary*)addModifier:(NSDictionary*)modifier toItem:(NSDictionary*)orderItem {
    // Create new order modifier
    //    JSONObject orderModifier = new JSONObject();
    NSDictionary *orderModifier =
    [NSMutableDictionary dictionaryWithDictionary:
     @{
       @"blockModifier":        [NSNumber numberWithInt:2],
       @"noteModifierOrder":    @"",
       @"priceExtraModifier":   [NSNumber numberWithDouble:[modifier[@"extra"] doubleValue]],
       @"priceModifier":        [NSNumber numberWithDouble:[modifier[@"price"] doubleValue]],
       @"quantityModifier":     [NSNumber numberWithInt:[orderItem[@"quatityItemOrder"] intValue]],
       @"idModifier":           modifier[@"id"],
       @"typePriceModifier":    @"ADD" ,
       @"priceNoModifier":      [NSNumber numberWithDouble:[modifier[@"no"] doubleValue]],
       @"selectedTypeModifier": @"Add" ,
       @"colorModifier":        @"#ff6f7f90",
       @"currentModifierOrder": [NSNumber numberWithDouble:[modifier[@"price"] doubleValue]],
       @"priceLessModifier":    [NSNumber numberWithDouble:[modifier[@"less"] doubleValue]],
       @"nameModifier":         modifier[@"name1"],
       @"typeModifierOrder":    @"Add",
       }];
    // Add to order item
    NSMutableArray *modifiers = orderItem[@"item"];
    [modifiers addObject:orderModifier];
    
    // Inform cart updated
    [self notifyCartChanged];
    
    return orderModifier;
}

- (NSDictionary*)removeModifier:(NSDictionary*)modifier fromItem:(NSDictionary*)orderItem {
    NSMutableArray *modifiers = orderItem[@"item"];
    NSString *idModifier = modifier[@"id"];
    for (int i = 0; i < modifiers.count; i++) {
        NSDictionary *modifier = [modifiers objectAtIndex:i];
        if ([idModifier isEqualToString:modifier[@"idModifier"]]) {
            [modifiers removeObject:modifier];
            [self notifyCartChanged];
            return modifier;
        }
    }
    
    return nil;
}

- (NSDictionary*)saveOrderNote:(NSString*)note forItem:(NSDictionary*)orderItem {
    if (orderItem == nil)
        return nil;
    
    NSMutableArray *modifiers = orderItem[@"item"];
    
    // Try to find the existing note
    for (int i = 0; i < modifiers.count; i++) {
        NSMutableDictionary *modifier = [modifiers objectAtIndex:i];
        if ([@"Note" isEqualToString:modifier[@"typeModifierOrder"]]) {
            if (note == nil || note.length == 0) {
                [modifiers removeObject:modifier];
            } else {
                [modifier setValue:note forKey:@"noteModifierOrder"];
            }
            [self notifyCartChanged];
            return modifier;
        }
    }
    
    // Not found, create new one
    if (note == nil || note.length == 0)
        return nil;
    
    NSDictionary *orderNote =
    [NSMutableDictionary dictionaryWithDictionary:
     @{
       @"blockModifier":        [NSNumber numberWithInt:2],
       @"noteModifierOrder":    note,
       @"priceExtraModifier":   [NSNumber numberWithDouble:0.0f],
       @"priceModifier":        [NSNumber numberWithDouble:0.0f],
       @"quantityModifier":     [NSNumber numberWithInt:0],
       @"idModifier":           @"",
       @"typePriceModifier":    @"" ,
       @"priceNoModifier":      [NSNumber numberWithDouble:0.0f],
       @"selectedTypeModifier": @"" ,
       @"colorModifier":        @"",
       @"currentModifierOrder": [NSNumber numberWithDouble:0.0f],
       @"priceLessModifier":    [NSNumber numberWithDouble:0.0f],
       @"nameModifier":         @"",
       @"typeModifierOrder":    @"Note",
       }];
    
    // Insert as first item
    [modifiers insertObject:orderNote atIndex:0];
    
    [self notifyCartChanged];
    
    return orderNote;
}

- (NSString*)getOrderNote:(NSDictionary*)orderItem {
    NSMutableArray *modifiers = orderItem[@"item"];
    
    // Try to find the existing note
    for (int i = 0; i < modifiers.count; i++) {
        NSDictionary *modifier = [modifiers objectAtIndex:i];
        if ([@"Note" isEqualToString:modifier[@"typeModifierOrder"]]) {
            return modifier[@"noteModifierOrder"];
        }
    }
    
    return @"";
}

- (NSString*)orderSummary {
    return [NSString stringWithFormat:@"%d items - $%.2f", [self totalItems], [self totalAmount]];
}

- (int)totalItems {
    NSArray *items = [_orderItems allValues];
    
    int totalItems = 0;
    for (int i = 0; i < items.count; i++) {
        NSDictionary *orderItem = [items objectAtIndex:i];
        totalItems += [orderItem[@"quatityItemOrder"] intValue] ;
    }
    return totalItems;
}

- (double)totalAmount {
    double subtotal, tax;
    [self calculateTotalDetail:&subtotal andTax:&tax];
    
    int discount = [self.store discount];
    if (discount > 0) {
        subtotal = subtotal * (100 - discount) / 100;
    }
    return subtotal + tax;
}

- (void)calculateTotalDetail:(double*)subtotal andTax:(double*)tax {
    *subtotal = 0.0f;
    *tax = 0.0f;
    
    if (self.store == nil)
        return;
    
    NSArray *itemids = [_orderItems allKeys];
    for (int i = 0; i < itemids.count; i++) {
        NSString *itemId = [itemids objectAtIndex:i];
        NSDictionary *item = [self.store findItemById:itemId];
        if (item) {
            NSDictionary *orderItem = _orderItems[itemId];
            
            int quantity = [orderItem[@"quatityItemOrder"] intValue];
            double itemPrice = [orderItem[@"priceItem"] doubleValue];
            double modifierAmount = [self modifiersTotalAmount:orderItem];
            double itemAmount = quantity * (itemPrice + modifierAmount);
            
            *subtotal += itemAmount;
            
            if ([@"yes" isEqualToString:item[@"togo_tax"]]) {
                *tax += itemAmount * kMOTaxRate;
            }
        }
    }
}

- (double)modifiersTotalAmount:(NSDictionary*)orderItem {
    double amount = 0.0;
    
    NSArray *modifiers = orderItem[@"item"];
    for (int i = 0; i < modifiers.count; i++) {
        NSDictionary *orderModifier = [modifiers objectAtIndex:i];
        if ([@"Add" isEqualToString:orderModifier[@"typeModifierOrder"]]) {
            amount += [orderModifier[@"priceModifier"] doubleValue];
        }
    }
    
    return amount;
}

@end
