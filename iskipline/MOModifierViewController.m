//
//  MOModifierViewController.m
//  iskipline
//
//  Created by Chinh Nguyen on 12/18/14.
//  Copyright (c) 2014 Chinh Nguyen. All rights reserved.
//

#import "MOModifierViewController.h"
#import "MOItemTableViewCell.h"

@interface MOModifierViewController () {
    NSArray *_modifiers;
    NSDictionary *_orderItem;
}

@end

@implementation MOModifierViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    BOOL isOrderPadMode = [userDefaults boolForKey:kOMIsOrderPadMode];
    self.labelCheckOut.text = isOrderPadMode ? [NSString stringWithFormat:@"#%@ >>", self.tableNumber] : @"CHECK OUT >>";
    _modifiers = [NSArray new];
    
    // Setup UI relating to category
    if (self.item && self.category) {
        self.itemNameLabel.text = displayName(self.item);
        self.itemDescriptionLabel.text = self.item[@"description"];
        
        if (self.store) {
            _orderItem = [self.order getOrderItem:self.item];
            if (_orderItem == nil) {
                _orderItem = [self.order addItem:self.item withCategory:self.category];
            }
        }
        
        // Set note text
        self.noteTextField.text = [self.order getOrderNote:_orderItem];
        
    } else {
        self.itemNameLabel.hidden = YES;
        self.itemDescriptionLabel.hidden = YES;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (_modifiers.count == 0) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.noteTextField becomeFirstResponder];
        });        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _modifiers.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MOItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ItemCell"];
    
    if (indexPath.row % 2 == 0) {
        cell.contentView.backgroundColor = [UIColor colorWithRed:0xd1/255.0f green:0x4b/255.0f blue:0x60/255.0f alpha:1.0f];
    } else {
        cell.contentView.backgroundColor = [UIColor colorWithRed:0xab/255.0f green:0x29/255.0f blue:0x29/255.0f alpha:1.0f];
    }
    
    if (cell.selectedBackgroundView.backgroundColor == nil) {
        UIView *bgColorView = [UIView new];
        bgColorView.layer.masksToBounds = YES;
        [bgColorView setBackgroundColor:[UIColor colorWithWhite:0.5 alpha:0.5]];
        [cell setSelectedBackgroundView:bgColorView];
    }
    
    NSDictionary *modifier = [_modifiers objectAtIndex:indexPath.row];
    cell.nameLabel.text = modifier[@"name1"];
    cell.priceLabel.text = [NSString stringWithFormat:@"$%.2f", [modifier[@"price"] doubleValue]];
    
    if (_orderItem) {
        // Got an existing order for this item
        NSArray *orderModifiers = _orderItem[@"item"];
        NSString *modifierId = modifier[@"id"];
        int quantityModifier = 0;
        // NOTE: Performance, UX issue???
        for (int i = 0; i < orderModifiers.count; i++) {
            NSDictionary *orderModifier = [orderModifiers objectAtIndex:i];
            if ([modifierId isEqualToString:orderModifier[@"idModifier"]]) {
                quantityModifier = [orderModifier[@"quantityModifier"] intValue];
                break;
            }
        }
        
        if (quantityModifier > 0) {
            cell.countLabel.hidden = NO;
            cell.countLabel.text = [NSString stringWithFormat:@"x %d", quantityModifier];
            cell.minusButton.hidden = NO;
            cell.addButton.hidden = YES;
        } else {
            cell.countLabel.hidden = YES;
            cell.countLabel.text = @"";
            cell.minusButton.hidden = YES;
            cell.addButton.hidden = NO;
        }
    }
    
    cell.addButton.tag = indexPath.row;
    cell.minusButton.tag = indexPath.row;
    
    return cell;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.noteTextField resignFirstResponder];
    return YES;
}

- (IBAction)noteEditingChanged:(id)sender {
    if (self.order) {
        [self.order saveOrderNote:self.noteTextField.text forItem:_orderItem];
    }
}

#pragma mark - Replication

- (void)loadDataAndUpdateLayout {
    if (!self.store || !self.store.storeDatabase || !self.category)
        return;
    
    // Get categories
    CBLDocument *doc = [self.store.storeDatabase existingDocumentWithID:kMODocModifiers];
    if (!doc)
        return;
    
    // List of all categories
    NSArray *allModifiers = doc[kMODocModifiers];
    
    // Filtered list of categories
    NSMutableArray *filteredModifiers = [NSMutableArray new];
    
    NSString *cat = self.category[@"id"];
    // Do not show item with self_order=false
    for (NSDictionary *item in allModifiers) {
        NSString *isShow = item[@"self_order"];
        NSString *itemCat = item[@"id_category"];
        if ((isShow == nil || [isShow boolValue] == true) && [cat isEqualToString:itemCat]) {
            [filteredModifiers addObject:item];
        }
    }
    
    // Sort by name
    _modifiers = [filteredModifiers sortedArrayUsingComparator:kMOItemNameComparator];
    
    // Reload table view
    [self.tableView reloadData];
}

#pragma mark - Add/Minus items

- (IBAction)addItemDidTouch:(id)sender {
    if (self.order) {
        UIButton *b = sender;
        NSDictionary *modifier = [_modifiers objectAtIndex:b.tag];
        [self.order addModifier:modifier toItem:_orderItem];
    }
}

- (IBAction)minusItemDidTouch:(id)sender {
    if (self.order) {
        UIButton *b = sender;
        NSDictionary *modifier = [_modifiers objectAtIndex:b.tag];
        [self.order removeModifier:modifier fromItem:_orderItem];
    }
}

@end
