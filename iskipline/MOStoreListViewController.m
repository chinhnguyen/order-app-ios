//
//  MOStoreListViewController.m
//  iskipline
//
//  Created by Chinh Nguyen on 12/16/14.
//  Copyright (c) 2014 Chinh Nguyen. All rights reserved.
//

#import "MOStoreListViewController.h"
#import "MOStoreTableViewCell.h"
#import "MOCategoryViewController.h"

@interface MOStoreListViewController () {
    NSArray *_stores;
}

@end

@implementation MOStoreListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.isNearby) {
        self.titleLabel.text = NSLocalizedString(@"Nearby Stores", @"Title of the store list screen");
    } else {
        self.titleLabel.text = [NSString stringWithFormat:@"%@, %@", self.city, self.state];
    }
    
    // Init for first data loading
    _stores = [NSArray new];
    
    // Loading data asynchronously
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        _stores = [[MOManager sharedInstance] findStoreByState:self.state andCity:self.city];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (_stores && _stores.count > 0) {
                [self.tableView reloadData];
            }
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        });
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"enterStore"]) {
        NSIndexPath *p = [self.tableView indexPathForSelectedRow];
        MOCategoryViewController *destinationVC = segue.destinationViewController;
        destinationVC.store = [_stores objectAtIndex:p.row];
    }
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _stores.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MOStoreTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"StoreCell"];
    
    MOStore *s = [_stores objectAtIndex:indexPath.row];
    cell.nameLabel.text = s.store;
    cell.addressLabel.text = [NSString stringWithFormat:@"%@, %@, %@", s.street, s.city, s.state];    
   
    if (cell.selectedBackgroundView.backgroundColor == nil) {
        UIView *bgColorView = [UIView new];
        bgColorView.layer.masksToBounds = YES;
        [bgColorView setBackgroundColor:[UIColor colorWithWhite:0.5 alpha:0.5]];
        [cell setSelectedBackgroundView:bgColorView];
    }    
    return cell;
}


@end
