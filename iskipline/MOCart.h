//
//  MOOrder.h
//  iskipline
//
//  Created by Chinh Nguyen on 12/18/14.
//  Copyright (c) 2014 Chinh Nguyen. All rights reserved.
//

#import <CouchbaseLite/CouchbaseLite.h>
#import "MOStore.h"

#define kMOCartUpdatedNotification @"kMOCartUpdatedNotification"

#define kMOTaxRate (double)0.09

@interface MOCart : NSObject

@property (nonatomic, readonly) NSMutableDictionary* orderItems;
@property (nonatomic, readonly) MOStore* store;

- (id)initWithStore:(MOStore*)store;

- (NSDictionary*)addItem:(NSDictionary*)item withCategory:(NSDictionary*)category;
- (NSDictionary*)removeItem:(NSDictionary*)item;
- (NSDictionary*)getOrderItem:(NSDictionary*)item;

- (BOOL)isEmpty;
- (void)emptyCart;

- (NSDictionary*)addModifier:(NSDictionary*)modifier toItem:(NSDictionary*)orderItem;
- (NSDictionary*)removeModifier:(NSDictionary*)modifier fromItem:(NSDictionary*)orderItem;

- (NSDictionary*)saveOrderNote:(NSString*)note forItem:(NSDictionary*)orderItem;
- (NSString*)getOrderNote:(NSDictionary*)orderItem;

- (NSString*)orderSummary;

- (int)totalItems;
- (double)totalAmount;

- (void)calculateTotalDetail:(double*)subtotal andTax:(double*)tax;

@end
