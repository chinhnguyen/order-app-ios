//
//  ZTMandrill.m
//  iskipline
//
//  Created by Chinh Nguyen on 12/21/14.
//  Copyright (c) 2014 Chinh Nguyen. All rights reserved.
//

#import "ZTMandrill.h"

@implementation ZTMandrill

+ (void)sendMail:(NSDictionary*)message
         success:(void (^)())success
         failure:(void (^)(NSError *error))failure{
    NSMutableDictionary *parameters =
    [NSMutableDictionary dictionaryWithObjectsAndKeys:
     kZTMandrillKey, @"key",
     @"false", @"async",
     message, @"message",
     nil];
    
    // Create the request
    NSError *error;
    NSMutableURLRequest *request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"PUT" URLString:kZTMandrillSendMessageUrl parameters:parameters error:&error];
    if (request == nil) {
        failure(error);
        return;
    }
    // Create operation
    AFHTTPRequestOperation *op = [[AFHTTPRequestOperationManager manager] HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success();
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
    // Schedule for execution
    [[NSOperationQueue mainQueue] addOperation:op];
}

@end

