//
//  MOAdminViewController.m
//  iskipline
//
//  Created by Chinh Nguyen on 1/9/15.
//  Copyright (c) 2015 Chinh Nguyen. All rights reserved.
//

#import "MOAdminViewController.h"
#import "MOItemDetailViewController.h"
#import "UIViewController+Utils.h"
#import <NSOrderedDictionary.h>

#define kMOAdminAddItemButton 1000
#define kMOAdminAddItemWithCategoryButton 1001
#define kMOAdminAddModifierWithCategoryButton 1002
#define kMOAdminEditItemButton 1003

@interface MOAdminViewController () {
    NSMutableOrderedDictionary *_groupedItems;
    MOItemType _currentType;
    
    CBLReplication *_pullReplication;
    CBLReplication *_pushReplication;
    CBLDatabase *_database;
}

@end

@implementation MOAdminViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self formatNavigationBar:@selector(backDidTouch:)];
    // Menu button
    UIImage *menuButtonImage = [UIImage imageNamed:@"menu.button.png"];
    UIButton *menuButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, menuButtonImage.size.width, menuButtonImage.size.height)];
    [menuButton setImage:menuButtonImage forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(menuDidTouch:) forControlEvents:UIControlEventTouchUpInside];
    // Back bar button
    UIBarButtonItem *menuBarButton = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    [menuBarButton setBackgroundImage:[[UIImage alloc] init] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [menuBarButton setTitle:@""];
    [self.navigationItem setRightBarButtonItem:menuBarButton];
    
    [self.tableView registerClass:[UITableViewHeaderFooterView class] forHeaderFooterViewReuseIdentifier:@"CategorySectionHeaderView"];
    
    // Initial items
    //    _items = [NSMutableArray new];
    _groupedItems = [NSMutableOrderedDictionary new];
    // Initial type
    _currentType = MOCategory;
    self.navigationItem.title = NSLocalizedString(@"Categories", @"");
    
    self.progressView.hidden = YES;
    
    // Start the syncing
    [self startSync];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Hide the picker view
    self.menuRightBorderContraint.constant = self.menuView.frame.size.width * -1 + (-21.0f);
    
    // Reload tables
    [self reloadItemList:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    if (_pullReplication) {
        [_pullReplication stop];
    }
    
    if (_pushReplication) {
        [_pushReplication stop];
    }
    
    if (_database) {
        [_database close:nil];
    }
}

#pragma mark - Properties

- (BOOL)userHasFullPermission {
    if (!self.user)
        return NO;
    NSString *role = self.user[@"roles"];
    return ([@"admin" caseInsensitiveCompare:role] == NSOrderedSame) || ([@"owner" caseInsensitiveCompare:role] == NSOrderedSame) || ([@"manager" caseInsensitiveCompare:role] == NSOrderedSame);
}

- (id)currentItem {
    return [self itemAtIndexPath:[self.tableView indexPathForSelectedRow]];
}

- (id)itemAtIndexPath:(NSIndexPath*)indexPath {
    if (indexPath) {
        NSArray *keys = [_groupedItems.allKeys objectAtIndex:indexPath.section];
        NSAssert(keys.count == 2, @"Keys must contain 2 values");
        return [[_groupedItems objectForKey:keys] objectAtIndex:indexPath.row];
    }
    
    return nil;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"embedMenu"]) {
        MOAdminMenuTableViewController *destinationVC = segue.destinationViewController;
        destinationVC.adminMenuDelegate = self;
    } else if ([segue.identifier isEqualToString:@"showDetail"]) {
        MOItemDetailViewController *destinationVC = segue.destinationViewController;
        destinationVC.database = _database;
        if (sender && [sender isKindOfClass:[UIView class]]) {
            NSInteger tag = ((UIView*)sender).tag;
            if (tag == kMOAdminAddItemButton) {
                destinationVC.itemType = _currentType;
                destinationVC.item = nil;
                destinationVC.category = nil;
            } else if (tag == kMOAdminEditItemButton) {
                destinationVC.itemType = _currentType;
                destinationVC.category = nil;
                destinationVC.item = [self currentItem];
            } else if (tag == kMOAdminAddItemWithCategoryButton) {
                destinationVC.itemType = MOItem;
                destinationVC.category = [self currentItem];
                destinationVC.item = nil;
            } else if (tag == kMOAdminAddModifierWithCategoryButton) {
                destinationVC.itemType = MOModifier;
                destinationVC.category = [self currentItem];
                destinationVC.item = nil;
            }
        }
    }
}

#pragma mark - MOAdminMenuDelegate

- (void)didSelectMenuItem:(long)index {
    // Hide the menu
    [self hideMenuView];
    
    MOItemType type;
    switch (index) {
        case 0:
            type = MOCategory;
            self.navigationItem.title = NSLocalizedString(@"Categories", @"");
            [self.addButton setTitle:NSLocalizedString(@"Add category", @"") forState:UIControlStateNormal];
            break;
            
        case 1:
            type = MOItem;
            self.navigationItem.title = NSLocalizedString(@"Items", @"");
            [self.addButton setTitle:NSLocalizedString(@"Add item", @"") forState:UIControlStateNormal];
            break;
            
        case 2:
            type = MOModifier;
            self.navigationItem.title = NSLocalizedString(@"Modifiers", @"");
            [self.addButton setTitle:NSLocalizedString(@"Add modifier", @"") forState:UIControlStateNormal];
            break;
            
        case 3:
            if ([self userHasFullPermission]) {
                type = MOUser;
                self.navigationItem.title = NSLocalizedString(@"Users", @"");
                [self.addButton setTitle:NSLocalizedString(@"Add user", @"") forState:UIControlStateNormal];
            }
            break;
            
        default:
            break;
    }
    
    if (_currentType != type) {
        // Update current type
        _currentType = type;
        // reset query text - TODO
        self.searchBar.text = @"";
        // hide the search view
        //        searchView.onActionViewCollapsed();
        // Update list view
        [self reloadItemList:NO];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _groupedItems.count;
}

//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
//    if (_currentType == MOUser || _currentType == MOCategory)
//        return @"";
//
//    NSArray *keys = [_groupedItems.allKeys objectAtIndex:section];
//    NSAssert(keys.count == 2, @"Keys must contain 2 values");
//    return [keys objectAtIndex: 0];
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *keys = [_groupedItems.allKeys objectAtIndex:section];
    NSAssert(keys.count == 2, @"Keys must contain 2 values");
    NSArray *items = [_groupedItems objectForKey:keys];
    return items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MOAdminItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AdminItemTableViewCell"];
    
    id item = [self itemAtIndexPath:indexPath];
    if (_currentType == MOCategory) {
        cell.titleLabel.text = trim(item[@"name1"]);
        NSString *desc = trim(item[@"description"]);
        if ([@"no_description" isEqualToString:desc]) {
            cell.subtitleLabel.text = @"";
        } else {
            cell.subtitleLabel.text = desc;
        }
    } else if (_currentType == MOUser) {
        NSString *name = [NSString stringWithFormat:@"%@ %@", item[@"firstname"], item[@"lastname"]];
        cell.titleLabel.text = trim(name);
        cell.subtitleLabel.text = item[@"roles"];
    } else if (_currentType == MOItem || _currentType == MOModifier) {
        cell.titleLabel.text = trim(item[@"name1"]);
        cell.subtitleLabel.text = trim(item[@"description"]);
    } else {
        cell.titleLabel.text = @"";
        cell.subtitleLabel.text = @"";
    }
    
    if (cell.selectedBackgroundView.backgroundColor == nil) {
        UIView *bgColorView = [UIView new];
        bgColorView.layer.masksToBounds = YES;
        [bgColorView setBackgroundColor:[UIColor colorWithWhite:0.5 alpha:0.5]];
        [cell setSelectedBackgroundView:bgColorView];
    }
    
    if ([cell respondsToSelector:@selector(preservesSuperviewLayoutMargins)]){
        cell.layoutMargins = UIEdgeInsetsZero;
        cell.preservesSuperviewLayoutMargins = false;
    }
    
    return cell;
}


#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (_currentType == MOItem || _currentType == MOModifier) {
        NSArray *keys = [_groupedItems.allKeys objectAtIndex:section];
        NSAssert(keys.count == 2, @"Keys must contain 2 values");
        if ([[_groupedItems objectForKey:keys] count] > 0)
            return 40;
    }
    
    return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"CategorySectionHeaderView"];
//    return [[UITableViewHeaderFooterView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 48)];
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    
    NSMutableParagraphStyle *paragraphStyle =  [NSMutableParagraphStyle new];
    [paragraphStyle setFirstLineHeadIndent:10.0f];
    NSDictionary *attributes = @{
                                 NSFontAttributeName:[UIFont fontWithName:@"MyriadHebrew-Bold" size:18],
                                 NSForegroundColorAttributeName: [UIColor lightGrayColor],
                                 NSParagraphStyleAttributeName: paragraphStyle,
                                 };

    NSArray *keys = [_groupedItems.allKeys objectAtIndex:section];
    NSAssert(keys.count == 2, @"Keys must contain 2 values");
    
    if ([[_groupedItems objectForKey:keys] count] > 0)
        header.textLabel.attributedText = [[NSAttributedString alloc] initWithString:[keys objectAtIndex:0] attributes:attributes];
    else
        header.textLabel.attributedText = [[NSAttributedString alloc] initWithString:@"" attributes:attributes];

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    id item = [self itemAtIndexPath:indexPath];
    if (_currentType == MOUser) {
        NSString *title = [NSString stringWithFormat:@"%@ %@", item[@"firstname"], item[@"lastname"]];
        UIActionSheet *actions =
        [[UIActionSheet alloc]
         initWithTitle:trim(title)
         delegate:self
         cancelButtonTitle:NSLocalizedString(@"Cancel", @"")
         destructiveButtonTitle:NSLocalizedString(@"Delete user", @"")
         otherButtonTitles:NSLocalizedString(@"Edit user", @""), nil];
        //        actions.tag = indexPath.row;
        [actions showInView:self.view];
    } else if (_currentType == MOCategory){
        UIActionSheet *actions =
        [[UIActionSheet alloc]
         initWithTitle:trim(item[@"name1"])
         delegate:self
         cancelButtonTitle:NSLocalizedString(@"Cancel", @"")
         destructiveButtonTitle:NSLocalizedString(@"Delete category", @"")
         otherButtonTitles:NSLocalizedString(@"Edit category", @""), NSLocalizedString(@"Add item", @""), NSLocalizedString(@"Add modifier", @""), nil];
        //        actions.tag = indexPath.row;
        [actions showInView:self.view];
    } else if (_currentType == MOItem){
        UIActionSheet *actions =
        [[UIActionSheet alloc]
         initWithTitle:trim(item[@"name1"])
         delegate:self
         cancelButtonTitle:NSLocalizedString(@"Cancel", @"")
         destructiveButtonTitle:NSLocalizedString(@"Delete item", @"")
         otherButtonTitles:NSLocalizedString(@"Edit item", @""), nil];
        //        actions.tag = indexPath.row;
        [actions showInView:self.view];
    } else if (_currentType == MOModifier){
        UIActionSheet *actions =
        [[UIActionSheet alloc]
         initWithTitle:trim(item[@"name1"])
         delegate:self
         cancelButtonTitle:NSLocalizedString(@"Cancel", @"")
         destructiveButtonTitle:NSLocalizedString(@"Delete modifier", @"")
         otherButtonTitles:NSLocalizedString(@"Edit modifier", @""), nil];
        //        actions.tag = indexPath.row;
        [actions showInView:self.view];
    }
}

#pragma mark - UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [self reloadItemList:YES];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    [self reloadItemList:YES];
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == actionSheet.cancelButtonIndex)
        return;
    
    id item = [self currentItem];
    if (!item)
        return;
    
    if (buttonIndex == 0) {
        NSString *title;
        if (_currentType == MOUser) {
            title = [NSString stringWithFormat:@"%@ %@", item[@"firstname"], item[@"lastname"]];
        } else {
            title = item[@"name1"];
        }
        
        // Delete item
        [UIAlertView showWithTitle:trim(title)
                           message:NSLocalizedString(@"Deleting this item is irreversable. Continue?", @"")
                 cancelButtonTitle:NSLocalizedString(@"No", @"")
                 otherButtonTitles:[NSArray arrayWithObject:NSLocalizedString(@"Yes", @"")]
                          tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                              if (buttonIndex == 1) {
                                  [self deleteItem:item];
                              }
                          }];
    } else {
        UIView *sender = [UIView new];
        if (_currentType == MOCategory) {
            if (buttonIndex == 1) {
                sender.tag = kMOAdminEditItemButton;
            } else if (buttonIndex == 2) {
                sender.tag = kMOAdminAddItemWithCategoryButton;
            } else if (buttonIndex == 3) {
                sender.tag = kMOAdminAddModifierWithCategoryButton;
            }
        } else {
            sender.tag = kMOAdminEditItemButton;
        }
        [self performSegueWithIdentifier:@"showDetail" sender:sender];
    }
}

#pragma mark - Data

- (void)startSync {
    if (!self.dbhost || !self.store || !self.store[@"account"]) {
        return;
    }
    
    CBLManager *manager = [CBLManager sharedInstance];
    
    NSError *error;
    _database = [manager databaseNamed:self.store[@"account"] error:&error];
    if (!_database) {
        [UIAlertView showWithTitle:NSLocalizedString(@"Critical Error", @"")
                           message:NSLocalizedString(@"Could not open local database, the admin could not continue.", @"")
                 cancelButtonTitle:NSLocalizedString(@"Log out", @"")
                 otherButtonTitles:nil
                          tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                              // Back one screen
                              [self dismissViewControllerAnimated:YES completion:nil];
                          }];
        return;
    }
    
    _pullReplication = [_database createPullReplication:storeSyncURL(self.dbhost, self.store[@"account"])];
    _pullReplication.continuous = YES;
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(storePullReplicationChanged:)                                                 name: kCBLReplicationChangeNotification
                                               object: _pullReplication];
    [_pullReplication start];
    
    _pushReplication = [_database createPushReplication:storeSyncURL(self.dbhost, self.store[@"account"])];
    _pushReplication.continuous = YES;
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(storePushReplicationChanged:)                                                 name: kCBLReplicationChangeNotification                                               object: _pushReplication];
    [_pushReplication start];
}

- (void)storePullReplicationChanged: (NSNotification*)n {
    CBLReplicationStatus status = _pullReplication.status;
    if (status == kCBLReplicationOffline || status == kCBLReplicationIdle) {
        // Hide the progress view first
        self.progressView.hidden = YES;
        // Update table layouts
        [self reloadItemList:YES];
    } else if (status == kCBLReplicationActive) {
        if (_pullReplication.changesCount >  0) {
            float progress = (float)_pullReplication.completedChangesCount / _pullReplication.changesCount;
            // Show the progress view first
            self.progressView.hidden = NO;
            // Update its progress
            self.progressView.progress = progress;
        }
    }
}

- (void)storePushReplicationChanged: (NSNotification*)n {
    // No handling yet
}

- (NSString*)currentDocumentId {
    if (_currentType == MOCategory) {
        return @"categories";
    } else if (_currentType == MOItem) {
        return @"items";
    } else if (_currentType == MOModifier) {
        return @"modifiers";
    } else if (_currentType == MOUser) {
        return @"users";
    } else {
        return nil;
    }
}

- (NSString*)viewName {
    if (_currentType == MOItem) {
        return @"items_by_category";
    } else if (_currentType == MOModifier) {
        return @"modifiers_by_category";
    } else {
        return nil;
    }
}


- (void)reloadItemList:(BOOL)animated {
    if (!_database)
        return;
    NSString *documentid = [self currentDocumentId];
    CBLDocument *document = [_database documentWithID:documentid];
    if (!document)
        return;
    
    NSString *filter = self.searchBar.text;
    
    if (_currentType == MOUser) {
        CBLView *view = [_database viewNamed:@"all_users"];
        if (!view.mapBlock) {
            [view setMapBlock:^(NSDictionary* doc, void (^emit)(id key, id value)) {
                if ([doc[@"_id"] isEqualToString:documentid]) {
                    NSArray *values = doc[documentid];
                    for (NSDictionary *value in values) {
                        if (value[@"username"])
                            emit(trim(value[@"username"]), value);
                    }
                }
            } version:@"2"];
        }
        
        NSError *error;
        CBLQuery *query = [view createQuery];
        // Create query that sort by category name
        query.sortDescriptors = @[[[NSSortDescriptor alloc] initWithKey:@"value.firstname" ascending:YES],                                  [[NSSortDescriptor alloc] initWithKey:@"value.lastname" ascending:YES]];
        //Create filtering
        if (filter && filter.length > 0) {
            query.postFilter = [NSPredicate predicateWithFormat:@"value.firstname CONTAINS[cd] %@ || value.lastname CONTAINS[cd] %@", filter, filter];
        }
        // run the query and add to final result
        CBLQueryEnumerator* result = [query run: &error];
        NSMutableArray *users = [NSMutableArray new];
        for (CBLQueryRow* row in result) {
            [users addObject:row.value];
        }
        [_groupedItems removeAllObjects];
        [_groupedItems setObject:users forKey:@[@"", @""]];
        
        if (animated) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.tableView reloadSections:[NSIndexSet indexSetWithIndex: 0] withRowAnimation:UITableViewRowAnimationFade];
            });
        } else {
            [self.tableView reloadData];
        }
    } else if (_currentType == MOCategory) {
        CBLView *view = [_database viewNamed:@"all_categories"];
        if (!view.mapBlock) {
            [view setMapBlock:^(NSDictionary* doc, void (^emit)(id key, id value)) {
                if ([doc[@"_id"] isEqualToString:documentid]) {
                    NSArray *values = doc[documentid];
                    for (NSDictionary *value in values) {
                        if (value[@"id"])
                            emit(trim(value[@"id"]), value);
                    }
                }
            } version:@"1"];
        }
        
        NSError *error;
        CBLQuery *query = [view createQuery];
        // Create query that sort by category name
        query.sortDescriptors = @[ [[NSSortDescriptor alloc] initWithKey:@"value.name1" ascending:YES]];
        //Create filtering
        if (filter && filter.length > 0) {
            query.postFilter = [NSPredicate predicateWithFormat:@"%K CONTAINS[cd] %@", @"value.name1", filter];
        }
        // run the query and add to final result
        CBLQueryEnumerator* result = [query run: &error];
        NSMutableArray *categories = [NSMutableArray new];
        for (CBLQueryRow* row in result) {
            [categories addObject:row.value];
        }
        [_groupedItems removeAllObjects];
        [_groupedItems setObject:categories forKey:@[@"", @""]];
        
        if (animated) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.tableView reloadSections:[NSIndexSet indexSetWithIndex: 0] withRowAnimation:UITableViewRowAnimationFade];
            });
        } else {
            [self.tableView reloadData];
        }
    } else {
        CBLView *view = [_database viewNamed:[self viewName]];
        if (!view.mapBlock) {
            [view setMapBlock:^(NSDictionary* doc, void (^emit)(id key, id value)) {
                if ([doc[@"_id"] isEqualToString:documentid]) {
                    NSArray *values = doc[documentid];
                    for (NSDictionary *value in values) {
                        if (value[@"id_category"] && value[@"category"])
                            emit(@[trim(value[@"category"]), value[@"id_category"]], value);
                    }
                }
            } reduceBlock:^id(NSArray *keys, NSArray *values, BOOL rereduce){
                return values;
            } version:@"6"];
        }
        
        NSError *error;
        CBLQuery *query = [view createQuery];
        query.groupLevel = 2;
        // Create query that sort by category name
        query.sortDescriptors = @[[[NSSortDescriptor alloc] initWithKey:@"key" ascending:YES comparator:^NSComparisonResult(id obj1, id obj2) {
            NSString* cat1 = trim([obj1 objectAtIndex:0]);
            NSString* cat2 = trim([obj2 objectAtIndex:0]);
            return [cat1 caseInsensitiveCompare:cat2];
        }]];
        
        CBLQueryEnumerator* result = [query run: &error];
        
        [_groupedItems removeAllObjects];
        
        NSComparator comparator = ^NSComparisonResult(id obj1, id obj2) {
            NSString *name1 = trim(obj1[@"name1"]);
            NSString *name2 = trim(obj2[@"name1"]);
            return [name1 caseInsensitiveCompare:name2];
        };
        if (filter && filter.length > 0) {
            for (CBLQueryRow* row in result) {
                NSMutableArray *filteredItems = [NSMutableArray new];
                NSArray *items = row.value;
                for (NSDictionary *i in items) {
                    NSString *name = i[@"name1"];
                    if ([trim(name) localizedCaseInsensitiveContainsString:filter])
                        [filteredItems addObject:i];
                }
                if (filteredItems.count > 0) {
                    [_groupedItems setObject:[filteredItems sortedArrayUsingComparator:comparator] forKey:row.key];
                }
            }
        } else {
            for (CBLQueryRow* row in result) {
                [_groupedItems setObject:[row.value sortedArrayUsingComparator:comparator] forKey:row.key];
            }
        }
//        if (animated) {
//            dispatch_async(dispatch_get_main_queue(), ^{
//                NSIndexSet *indexSet = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, _groupedItems.count)];
//                [self.tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationFade];
//            });
//        } else {
            [self.tableView reloadData];
//        }
    }
    
    
    
}

//private void deleteItem(JSONObject item) {
- (void)deleteItem:(id)item {
    NSString *fieldname = (_currentType == MOUser) ? @"username" : @"id";
    if (!item)
        return;
    NSString *itemid = item[fieldname];
    if (!itemid) {
        [self.view makeToast:NSLocalizedString(@"Missing key property", @"")];
        return;
    }
    
    if (!_database)
        return;
    NSString *currentDocumentId = [self currentDocumentId];
    // retrieve the document from the database
    CBLDocument *document = [_database documentWithID:currentDocumentId];
    if (!document)
        return;
    
    NSMutableDictionary *properties = [document.properties mutableCopy];
    NSMutableArray *items = [NSMutableArray arrayWithArray:[properties objectForKey:currentDocumentId]];
    // Make sure we have the correspondence field (same name with
    // document)
    if (!items)
        return;
    
    for (id item in items) {
        if ([itemid isEqualToString:item[fieldname]]) {
            [items removeObject:item];
            break;
        }
    }
    
    // Save to document
    [properties setValue:items forKey:currentDocumentId];
    
    NSError *error;
    [document putProperties:properties error:&error];
    
    if (_currentType == MOCategory) {
        // Delete child items
        CBLDocument *itemsDocument = [_database documentWithID:@"items"];
        NSMutableDictionary *itemsProperties = [itemsDocument.properties mutableCopy];
        
        NSArray *existingItems = itemsProperties[@"items"];
        NSMutableArray *newItems = [NSMutableArray new];
        for (NSDictionary *item in existingItems) {
            if (![itemid isEqualToString:item[@"id_category"]]) {
                [newItems addObject:item];
            }
        }
        [itemsProperties setValue:newItems forKey:@"items"];
        [itemsDocument putProperties:itemsProperties error:nil];
        
        
        // Delete child modifiers
        CBLDocument *modifiersDocument = [_database documentWithID:@"modifiers"];
        NSMutableDictionary *modifiersProperties = [modifiersDocument.properties mutableCopy];
        
        NSArray *existingModifiers = modifiersProperties[@"modifiers"];
        NSMutableArray *newModifiers = [NSMutableArray new];
        for (NSDictionary *mod in existingModifiers) {
            if (![itemid isEqualToString:mod[@"id_category"]]) {
                [newModifiers addObject:mod];
            }
        }
        [modifiersProperties setValue:newModifiers forKey:@"modifiers"];
        [modifiersDocument putProperties:modifiersProperties error:nil];
    }
    // Let the list to load its data
    [self.tableView reloadData];
}



#pragma mark - IBAction

- (IBAction)backDidTouch:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)menuDidTouch:(id)sender {
    if (self.menuRightBorderContraint.constant == -21.0f) {
        // Visible => hide it
        [self hideMenuView];
    } else {
        [self showMenuView];
    }
}

- (IBAction)addDidTouch:(id)sender {
    if (_currentType == MOItem || _currentType == MOModifier) {
        CBLDocument *categoriesDocument = [_database documentWithID:@"categories"];
        if (!categoriesDocument) {
            [UIAlertView showWithTitle:NSLocalizedString(@"Bad data", @"")
                               message:NSLocalizedString(@"Missing categories in database, please have that fixed and try again!", @"")
                     cancelButtonTitle:NSLocalizedString(@"OK", @"")
                     otherButtonTitles:nil
                              tapBlock:nil];
            return;
        }
        
        NSArray *categories = [categoriesDocument propertyForKey:@"categories"];
        if (!categories || categories.count == 0) {
            [UIAlertView showWithTitle:NSLocalizedString(@"No category", @"")
                               message:NSLocalizedString(@"Please add at least one category before adding items or modifiers. Thanks!", @"")
                     cancelButtonTitle:NSLocalizedString(@"OK", @"")
                     otherButtonTitles:nil
                              tapBlock:nil];
            return;
        }
    }
    ((UIButton*)sender).tag = kMOAdminAddItemButton;
    [self performSegueWithIdentifier:@"showDetail" sender:sender];
}

- (IBAction)coverViewDidTouch:(id)sender {
    [self hideMenuView];
}

#pragma mark - Menu handling

- (void)showMenuView {
    [self.view layoutIfNeeded];
    
    // show the cover view
    self.coverView.hidden = NO;
    // Animate to show the menu
    [UIView animateWithDuration:0.3f delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.menuRightBorderContraint.constant= -21.0f;
        
        [self.view layoutIfNeeded];
    } completion:nil];
}

- (void)hideMenuView {
    [self.view layoutIfNeeded];
    
    // show the cover view
    self.coverView.hidden = YES;
    // Hide the menu view
    [UIView animateWithDuration:0.3f delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.menuRightBorderContraint.constant = self.menuView.frame.size.width * -1 + (-21.0f);
        
        [self.view layoutIfNeeded];
    } completion:nil];
}


@end

@implementation MOAdminMenuTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.tableFooterView = [UIView new];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:NO scrollPosition:UITableViewScrollPositionTop];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.adminMenuDelegate && [self.adminMenuDelegate respondsToSelector:@selector(didSelectMenuItem:)]) {
        [self.adminMenuDelegate didSelectMenuItem:indexPath.row];
    }
}


@end

@implementation MOAdminItemTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end