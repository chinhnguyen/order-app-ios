//
//  ZTTrumpia.h
//  iskipline
//
//  Created by Chinh Nguyen on 12/21/14.
//  Copyright (c) 2014 Chinh Nguyen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking.h>

#define kZTTrumpiaBaseUrl @"http://api.trumpia.com/rest/v1/chrislam"
#define kZTTrumpiaSubscriptionUrl kZTTrumpiaBaseUrl @"/subscription"
#define kZTTrumpiaReportUrl kZTTrumpiaBaseUrl @"/report"
#define kZTTrumpiaMessageUrl kZTTrumpiaBaseUrl @"/message"

#define kZTTrumpiaApiKey @"9c41c71e90b82b700b1a6db7a88bfed9"
#define kZTTrumpiaModibleOrderListName @"MobileOrder"

#define kZTTrumpiaErrorEmptyRequestId 600
#define kZTTrumpiaErrorSubscriptionAlreadyExists 601
#define kZTTrumpiaErrorEmptySubscriptionId 602
#define kZTTrumpiaErrorCreatingSubscription 603
#define kZTTrumpiaErrorUpdatingSubscription 604
#define kZTTrumpiaErrorSearchNoResult 605
#define kZTTrumpiaErrorSendingMessage 606

@interface ZTTrumpia : NSObject

+ (void)putSubscription:(NSDictionary*)user
                success:(void (^)(NSString *subscriptionId))success
                failure:(void (^)(NSError *error))failure;

+ (void)postSubscription:(NSDictionary*)user
                   forId:(NSString*)subscriptionId
                 success:(void (^)())success
                 failure:(void (^)(NSError *error))failure;

+ (void)searchSubscriptionWithType:(NSString*)type
                           andData:(NSString*)data
                           success:(void (^)(NSString *subscriptionId))success
                           failure:(void (^)(NSError *error))failure;

+ (void)sendSMSTo:(NSString*)subscriptionId
  withDescription:(NSString*)description
 organizationName:(NSString*)orgName
             text:(NSString*)text
          success:(void (^)())success
          failure:(void (^)(NSError *error))failure;

+ (void)getReport:(NSString*)requestId success:(void (^)(id responseObject))success
          failure:(void (^)(NSError *error))failure;
@end
