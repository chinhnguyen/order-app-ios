//
//  MOLoginViewController.h
//  iskipline
//
//  Created by Chinh Nguyen on 1/6/15.
//  Copyright (c) 2015 Chinh Nguyen. All rights reserved.
//

#import "MOBaseViewController.h"
#import <FormKit.h>
#import <UIView+Toast.h>

@interface MOLoginTableViewController : UITableViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *hostText;
@property (weak, nonatomic) IBOutlet UITextField *accountText;
@property (weak, nonatomic) IBOutlet UITextField *userText;
@property (weak, nonatomic) IBOutlet UITextField *passwordText;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *loginBarButtonItem;

@end

