//
//  MOLoginViewController.m
//  iskipline
//
//  Created by Chinh Nguyen on 1/6/15.
//  Copyright (c) 2015 Chinh Nguyen. All rights reserved.
//

#import "MOLoginViewController.h"
#import "UIViewController+Utils.h"
#import "MOAdminViewController.h"

@implementation MOLoginTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Create some padding
    self.tableView.contentInset = UIEdgeInsetsMake(10, 0, 0, 0);
    
    self.navigationController.navigationBarHidden = NO;
    [self formatNavigationBar:@selector(backDidTouch:)];

    [self.navigationItem.rightBarButtonItem setTitleTextAttributes:@{                                 NSForegroundColorAttributeName: [UIColor whiteColor],                                 NSFontAttributeName: [UIFont fontWithName:@"Myriad Pro" size:16.0]                                 } forState:UIControlStateNormal];
    
    self.navigationItem.title = NSLocalizedString(@"Admin", @"");
    self.navigationController.navigationBar.titleTextAttributes = @{ NSForegroundColorAttributeName: [UIColor whiteColor], NSFontAttributeName: [UIFont fontWithName:@"Helvetica-Bold" size:20.0]};
    
    [self applyStyle:self.hostText];
    [self applyStyle:self.accountText];
    [self applyStyle:self.userText];
    [self applyStyle:self.passwordText];
    
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    self.hostText.text = [defaults objectForKey:@"admin_dbhost"];
    self.accountText.text = [defaults objectForKey:@"admin_account"];
    
    if (self.hostText.text == nil || self.hostText.text.length == 0) {
        [self.hostText becomeFirstResponder];
    } else if (self.accountText.text == nil || self.accountText.text.length == 0) {
        [self.accountText becomeFirstResponder];
    } else {
        [self.userText becomeFirstResponder];
    }
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.hostText) {
        [self.accountText becomeFirstResponder];
    } else if (textField == self.accountText) {
        [self.userText becomeFirstResponder];
    } else if (textField == self.userText) {
        [self.passwordText becomeFirstResponder];
    } else if (textField == self.passwordText) {
        [self loginDidTouch:textField];
    }
    
    return YES;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showAdmin"]) {
        MOAdminViewController *destinationVC = segue.destinationViewController;
        destinationVC.dbhost = trim(self.hostText.text);
        if (sender && [sender respondsToSelector:@selector(valueForKey:)]) {
            destinationVC.user = [sender valueForKey:@"user"];
            destinationVC.store = [sender valueForKey:@"store"];
        }
    }
}

- (void)applyStyle:(UITextField*)textField {
    textField.layer.cornerRadius = 5;
    textField.leftViewMode = UITextFieldViewModeAlways;
    UIView *leftPaddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 0)];
    textField.leftView = leftPaddingView;
}

#pragma mark - IBAction

- (IBAction)backDidTouch:(id)sender {
    self.navigationController.navigationBarHidden = YES;
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)loginDidTouch:(id)sender {

    NSString *dbhost = trim(self.hostText.text);
    NSString *dbname = trim(self.accountText.text);
    NSString *username = trim(self.userText.text);
    NSString *password = trim(self.passwordText.text);
    
    if (dbhost.length == 0) {
        [self.view makeToast:NSLocalizedString(@"Missing host", @"") duration:3.0 position:CSToastPositionTop];
        [self.hostText becomeFirstResponder];
        return;
    }
    if (dbname.length == 0) {
        [self.view makeToast:NSLocalizedString(@"Missing account", @"") duration:3.0 position:CSToastPositionTop];
        [self.accountText becomeFirstResponder];
        return;
    }
    if (username.length == 0) {
        [self.view makeToast:NSLocalizedString(@"Missing user", @"") duration:3.0 position:CSToastPositionTop];
        [self.userText becomeFirstResponder];
        return;
    }
    if (password.length == 0) {
        [self.view makeToast:NSLocalizedString(@"Missing password", @"") duration:3.0 position:CSToastPositionTop];
        [self.passwordText becomeFirstResponder];
        return;
    }
    
    if (![self networkAvailable]) {
        [UIAlertView showWithTitle:NSLocalizedString(@"No network", @"")
                           message:NSLocalizedString(@"Please make sure your device's network is working and try again.", @"")
                 cancelButtonTitle:NSLocalizedString(@"OK", @"")
                 otherButtonTitles:nil
                          tapBlock:nil];
        return;
    }
    
    [self.view endEditing:YES];
    
    // Save for next time
    [self saveHost:dbhost andAccount:dbname];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[MOManager sharedInstance] loginHost:dbhost store:dbname user:username password:password success:^(id user, id store) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];

        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:user, @"user", store, @"store", nil];
        [self performSegueWithIdentifier:@"showAdmin" sender:params];
    } failure:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];

        [UIAlertView showWithTitle:NSLocalizedString(@"Login failed", @"") message:NSLocalizedString(@"Please make sure your store account, username and password are valid and try again.", @"") cancelButtonTitle:NSLocalizedString(@"Close", @"") otherButtonTitles:nil tapBlock:nil];
    }];
}


#pragma mark - History

- (void)saveHost:(NSString*)dbhost andAccount:(NSString*)account {
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:dbhost forKey:@"admin_dbhost"];
    [defaults setObject:account forKey:@"admin_account"];
    [defaults synchronize];
}

@end


