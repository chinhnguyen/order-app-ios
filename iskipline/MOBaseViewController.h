//
//  MOBaseViewController.h
//  iskipline
//
//  Created by Chinh Nguyen on 12/16/14.
//  Copyright (c) 2014 Chinh Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import <UIAlertView+Blocks/UIAlertView+Blocks.h>
#import <AFNetworking/UIKit+AFNetworking.h>
#import <Toast/UIView+Toast.h>
#import <QuartzCore/QuartzCore.h>
#import <CouchbaseLite.h>
#import "MOManager.h"
#import "MOStore.h"
#import "MOCart.h"
#import "UIViewController+Utils.h"

#define kOMIsOrderPadMode       @"kOMIsOrderPadMode"
#define kMOVisitedStoresList    @"kMOVisitedStoresList"

typedef NS_ENUM(NSUInteger, MOItemType) {
    MOCategory,
    MOItem,
    MOModifier,
    MOUser,
};

@interface MOBaseViewController : UIViewController

@end
