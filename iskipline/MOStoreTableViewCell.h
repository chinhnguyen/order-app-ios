//
//  MOStoreTableViewCell.h
//  iskipline
//
//  Created by Chinh Nguyen on 12/16/14.
//  Copyright (c) 2014 Chinh Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MOStoreTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UIButton *removeButton;

@end
