//
//  MOItemDetailViewController.h
//  iskipline
//
//  Created by Chinh Nguyen on 1/12/15.
//  Copyright (c) 2015 Chinh Nguyen. All rights reserved.
//

#import "MOBaseViewController.h"
#import <FormKit.h>

@interface MOItemDetailViewController : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic) MOItemType itemType;
@property (weak, nonatomic) NSDictionary *item;
@property (weak, nonatomic) NSDictionary *category;
@property (weak, nonatomic) CBLDatabase *database;

@end

@interface MOBaseFormModel : NSObject

@end

@interface MOUserFormModel : MOBaseFormModel

@property (nonatomic, retain) NSString* username;
@property (nonatomic, retain) NSString* password;
@property (nonatomic, retain) NSString* firstname;
@property (nonatomic, retain) NSString* lastname;
@property (nonatomic, retain) NSString* roles;
@property (nonatomic) BOOL active;
@property (nonatomic) BOOL hidden;

@end

@interface MOCategoryFormModel : MOBaseFormModel

@property (nonatomic, retain) NSString* ID;
@property (nonatomic, retain) NSString* parentID;
@property (nonatomic, retain) NSString* name1;
@property (nonatomic, retain) NSString* name2;
@property (nonatomic, retain) NSString* name3;
@property (nonatomic, retain) NSString* desc;
@property (nonatomic, retain) NSString* fontColor;
@property (nonatomic, retain) NSString* bgColor;
@property (nonatomic, retain) NSNumber* maxBlock;
@property (nonatomic) BOOL visible;
@property (nonatomic, retain) NSString* printerID;
@property (nonatomic) int order;

@end

@interface MOItemFormModel : MOBaseFormModel

@property (nonatomic, retain) NSString* ID;
@property (nonatomic, retain) NSString* category;
@property (nonatomic, retain) NSString* category_id;
@property (nonatomic, retain) NSString* item_number;
@property (nonatomic, retain) NSString* name1;
@property (nonatomic, retain) NSString* name2;
@property (nonatomic, retain) NSString* desc;
@property (nonatomic, retain) NSString* keyword;
@property (nonatomic) double regular_price;
@property (nonatomic) double promo_price;
@property (nonatomic) double combo_price;
@property (nonatomic) int quantity;
@property (nonatomic) BOOL to_kitchen;
@property (nonatomic, retain) NSString* barcode;
@property (nonatomic) BOOL togo_tax;
@property (nonatomic) BOOL forhere_tax;
@property (nonatomic, retain) NSString* mods;
@property (nonatomic) BOOL visible;

@end

@interface MOModifierFormModel : MOBaseFormModel

@property (nonatomic, retain) NSString* ID;
@property (nonatomic, retain) NSString* category;
@property (nonatomic, retain) NSString* category_id;
@property (nonatomic, retain) NSString* name1;
@property (nonatomic, retain) NSString* name2;
@property (nonatomic, retain) NSString* name3;
@property (nonatomic) double price;
@property (nonatomic) double no;
@property (nonatomic) double extra;
@property (nonatomic) double less;
@property (nonatomic) double onside;

@end
