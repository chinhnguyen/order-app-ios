//
//  UIViewController+Utils.h
//  iskipline
//
//  Created by Chinh Nguyen on 1/7/15.
//  Copyright (c) 2015 Chinh Nguyen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <UIAlertView+Blocks.h>

@interface UIViewController (Utils)

- (BOOL)checkNetwork;
- (BOOL)networkAvailable;
- (void)showNetworkNotAvailableMessage;
- (void)formatNavigationBar:(SEL)backAction;

- (IBAction)backDidTouch:(id)sender;

@end
