//
//  MOItemViewController.m
//  iskipline
//
//  Created by Chinh Nguyen on 12/18/14.
//  Copyright (c) 2014 Chinh Nguyen. All rights reserved.
//

#import "MOItemViewController.h"
#import "MOItemTableViewCell.h"
#import "MOModifierViewController.h"
#import "UIImageView+AFNetworking.h"

@interface MOItemViewController () {
    NSArray *_items;
}

@end

@implementation MOItemViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    BOOL isOrderPadMode = [userDefaults boolForKey:kOMIsOrderPadMode];
    self.labelCheckOut.text = isOrderPadMode ? [NSString stringWithFormat:@"#%@ >>", self.tableNumber] : @"CHECK OUT >>";
    _items = [NSArray new];
    
    // Setup UI relating to category
    if (self.category) {
        self.categoryNameLabel.text =  displayName(self.category);
        self.categoryDescriptionLabel.text = self.category[@"description"];
        
        NSString *image = self.category[@"image"];
        if (image && image.length > 0) {
            [self.categoryImageView setImageWithURL:[NSURL URLWithString:image]];
        }
    } else {
        self.categoryNameLabel.hidden = YES;
        self.categoryDescriptionLabel.hidden = YES;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    [super prepareForSegue:segue sender:sender];
    
    if ([segue.identifier isEqualToString:@"showModifiers"]) {
        MOModifierViewController *vc = segue.destinationViewController;
        vc.store = self.store;
        vc.order = self.order;
        vc.tableNumber = self.tableNumber;
        vc.category = self.category;
        NSIndexPath *p = [self.tableView indexPathForSelectedRow];
        vc.item = [_items objectAtIndex:p.row];        
    }
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MOItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ItemCell"];
    
    if (indexPath.row % 2 == 0) {
        cell.contentView.backgroundColor = [UIColor colorWithRed:0xd1/255.0f green:0x4b/255.0f blue:0x60/255.0f alpha:1.0f];
    } else {
        cell.contentView.backgroundColor = [UIColor colorWithRed:0xab/255.0f green:0x29/255.0f blue:0x29/255.0f alpha:1.0f];
    }
    
    NSDictionary *item = [_items objectAtIndex:indexPath.row];
    cell.nameLabel.text = displayName(item);
    cell.priceLabel.text = [NSString stringWithFormat:@"$%.2f", [item[@"regular_price"] doubleValue]];
    
    if (cell.selectedBackgroundView.backgroundColor == nil) {
        UIView *bgColorView = [UIView new];
        bgColorView.layer.masksToBounds = YES;
        [bgColorView setBackgroundColor:[UIColor colorWithWhite:0.5 alpha:0.5]];
        [cell setSelectedBackgroundView:bgColorView];
    }
    
    NSString *itemnumber = item[@"item_number"];
    if (itemnumber) {
        itemnumber = trim(itemnumber);
        if (itemnumber.length == 0 || [itemnumber isEqualToString:@"0"]) {
            cell.numberLabel.hidden = YES;
        } else {
            cell.numberLabel.hidden = NO;
            cell.numberLabel.text = [NSString stringWithFormat:@"#%@", itemnumber];
        }
    } else {
        cell.numberLabel.hidden = YES;
    }
    
    // Order is available
    if (self.order) {
        NSDictionary *orderItem = [self.order getOrderItem:item];
        if (item) {
            // Got an existing order for this item
            NSArray *modifiers = orderItem[@"item"];
            if (modifiers && modifiers.count > 0) {
                cell.customizedLabel.hidden = NO;
            } else {
                cell.customizedLabel.hidden = YES;
            }
            
            int itemCount = [orderItem[@"quatityItemOrder"] intValue];
            if (itemCount > 0) {
                cell.countLabel.hidden = NO;
                cell.countLabel.text = [NSString stringWithFormat:@"x %d", itemCount];
                cell.minusButton.hidden = NO;
            } else {
                cell.countLabel.hidden = YES;
                cell.countLabel.text = @"";
                cell.minusButton.hidden = YES;
            }
        } else {
            // no order yet
            cell.customizedLabel.hidden = YES;
            cell.countLabel.hidden = YES;
            cell.minusButton.hidden = YES;
        }
    }
    
    cell.addButton.tag = indexPath.row;
    cell.minusButton.tag = indexPath.row;
  
    return cell;
}


#pragma mark - Replication

- (void)loadDataAndUpdateLayout {
    if (!self.store || !self.store.storeDatabase || !self.category)
        return;
    
    // Get categories
    CBLDocument *doc = [self.store.storeDatabase existingDocumentWithID:kMODocItems];
    if (!doc)
        return;
    
    // List of all categories
    NSArray *allItems = doc[kMODocItems];
    
    // Filtered list of categories
    NSMutableArray *filteredItems = [NSMutableArray new];
    
    NSString *cat = self.category[@"id"];
    // Do not show item with self_order=false
    for (NSDictionary *item in allItems) {
        NSString *isShow = item[@"self_order"];
        NSString *itemCat = item[@"id_category"];
        if ((isShow == nil || [isShow boolValue] == true) && [cat isEqualToString:itemCat]) {
            [filteredItems addObject:item];
        }
    }
    
    // Sort by name
    _items = [filteredItems sortedArrayUsingComparator:kMOItemNameComparator];
    
    // Reload table view
    [self.tableView reloadData];    
}

#pragma mark - Add/Minus items

- (IBAction)addItemDidTouch:(id)sender {
    if (self.order) {
        UIButton *b = sender;
        NSDictionary *item = [_items objectAtIndex:b.tag];
        [self.order addItem:item withCategory:self.category];
    }
}

- (IBAction)minusItemDidTouch:(id)sender {
    if (self.order) {
        UIButton *b = sender;
        NSDictionary *item = [_items objectAtIndex:b.tag];
        [self.order removeItem:item];
    }
}

@end
