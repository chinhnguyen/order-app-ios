//
//  SOAppUser.m
//  SelfOrder
//
//  Created by PC02 on 1/15/14.
//  Copyright (c) 2014 nexmac. All rights reserved.
//

#import "MOAppUser.h"

@implementation MOAppUser

- (BOOL)paymentRegistered {
    return self.customerId && self.customerId.length > 0;
}

@end
