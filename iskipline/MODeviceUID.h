//
//  MODeviceUID.h
//  iskipline
//
//  Created by Chinh Nguyen on 9/11/15.
//  Copyright (c) 2015 Chinh Nguyen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MODeviceUID : NSObject

+ (NSString *)uid;

@end