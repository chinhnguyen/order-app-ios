//
//  MOPaymentViewController.m
//  iskipline
//
//  Created by Chinh Nguyen on 12/20/14.
//  Copyright (c) 2014 Chinh Nguyen. All rights reserved.
//

#import "MOPaymentViewController.h"
#import "MOConfirmCodeViewController.h"
#import "UIViewController+Utils.h"
#import "MOPickupTimeViewController.h"
#import "MOUserInfoViewController.h"
#import <RNDecryptor.h>

#pragma mark - MOPaymentViewController

@interface MOPaymentViewController () {
    MOPaymentTableViewController *_tableVC;
}


@end

@implementation MOPaymentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"tableContainer"]) {
        _tableVC = segue.destinationViewController;
        _tableVC.store = self.store;
        _tableVC.order = self.order;
    }
}

- (IBAction)backFromPickupTime:(UIStoryboardSegue *)unwindSegue {
}

@end

#pragma mark - MOPaymentTableViewController

@interface MOPaymentTableViewController ()

@end

@implementation MOPaymentTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self applyStyle:self.pickupNameText placeholder:NSLocalizedString(@"Your name", @"")];
    [self applyStyle:self.pickupPhoneText placeholder:NSLocalizedString(@"Your phone number", @"")];
    [self applyStyle:self.sendCodeButton];
    
    [self applyStyle:self.loginPhoneText placeholder:NSLocalizedString(@"Your phone number", @"")];
    [self applyStyle:self.loginPassText placeholder:NSLocalizedString(@"Password", @"")];
    [self applyStyle:self.loginButton];
    [self applyStyle:self.registerButton];
    
    if (self.store) {
        self.pickupTableViewCell.hidden = ![self.store allowPayWhenPickup];
        
        NSString *merchantId = [self.store decryptedMerchantId];
        NSString *merchantKey = [self.store decryptedMerchantKey];
        self.loginTableViewCell.hidden = !merchantId || !merchantKey;
    } else {
        self.pickupTableViewCell.hidden = YES;
        self.loginTableViewCell.hidden = YES;
    }
    
    if (self.pickupTableViewCell.hidden && self.loginTableViewCell.hidden) {
        [UIAlertView showWithTitle:NSLocalizedString(@"Not accepting orders", @"") message:NSLocalizedString(@"Sorry! This store is not currently acceptting orders. Please contact store for further information.", @"") cancelButtonTitle:NSLocalizedString(@"Close", @"") otherButtonTitles:nil tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
            [self.navigationController popViewControllerAnimated:YES];
        }];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Styles

- (void)applyStyle:(UITextField*)textField placeholder:(NSString*)placeholder {
    textField.layer.borderColor = [UIColor whiteColor].CGColor;
    textField.layer.borderWidth = 1;
    textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:placeholder attributes:@{ NSForegroundColorAttributeName:[UIColor colorWithWhite:0.5f alpha:0.8f]}];
}

- (void)applyStyle:(UIButton*)button {
    button.layer.cornerRadius = 5;
    button.layer.borderWidth = 1;
    button.layer.borderColor = [UIColor whiteColor].CGColor;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showConfirmCode"]) {
        MOConfirmCodeViewController *destinationVC = segue.destinationViewController;
        destinationVC.name = self.pickupNameText.text;
        destinationVC.phone = [self.pickupPhoneText.text stringByReplacingOccurrencesOfString:@"-" withString:@""];
        destinationVC.store = self.store;
        destinationVC.order = self.order;
    } else if ([segue.identifier isEqualToString:@"showPickupTime"]) {
        MOPickupTimeViewController *destinationVC = segue.destinationViewController;
        destinationVC.store = self.store;
        destinationVC.order = self.order;
        destinationVC.user = sender;
    } else if ([segue.identifier isEqualToString:@"showUserInfo"]) {
        MOUserInfoViewController *destinationVC = segue.destinationViewController;
        destinationVC.store = self.store;
        destinationVC.order = self.order;
        destinationVC.user = sender;
    }
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if ([identifier isEqualToString:@"showConfirmCode"]) {
        [self.view endEditing:YES];
        if (self.pickupNameText.text.length == 0) {
            [UIAlertView showWithTitle:NSLocalizedString(@"Missing name", @"") message:NSLocalizedString(@"Please enter your name to proceed with ordering.", @"") cancelButtonTitle:NSLocalizedString(@"Close", @"") otherButtonTitles:nil tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                [self.pickupNameText becomeFirstResponder];
            }];
            
            return NO;
        } else if (self.pickupPhoneText.text.length == 0) {
            [UIAlertView showWithTitle:NSLocalizedString(@"Missing phone number", @"") message:NSLocalizedString(@"Please enter the phone number you would like to receive the confirmation code.", @"") cancelButtonTitle:NSLocalizedString(@"Close", @"") otherButtonTitles:nil tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                [self.pickupPhoneText becomeFirstResponder];
            }];
            
            return NO;
        } else if (self.pickupPhoneText.text.length < 12) {
            [UIAlertView showWithTitle:NSLocalizedString(@"Invalid phone number", @"") message:NSLocalizedString(@"Please enter a valid phone number.", @"") cancelButtonTitle:NSLocalizedString(@"Close", @"") otherButtonTitles:nil tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                [self.pickupPhoneText becomeFirstResponder];
            }];
            
            return NO;
        } else if ([self exceedMaxAmount:@"max_paywhenpickup"]) {
            return NO;
        }
    } else if ([identifier isEqualToString:@"showPickupTime"]) {
        return sender;
    } else if ([identifier isEqualToString:@"showUserInfo"]) {
        return sender;
    }
    
    return [super shouldPerformSegueWithIdentifier:identifier sender:sender];
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    
    if (cell.hidden) {
        return 0;
    } else {
        return [super tableView:tableView heightForRowAtIndexPath:indexPath];
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField == self.pickupPhoneText || textField == self.loginPhoneText) {
        
        NSUInteger oldLength = [textField.text length];
        NSUInteger replacementLength = [string length];
        NSUInteger rangeLength = range.length;
        NSUInteger newLength = oldLength - rangeLength + replacementLength;
        if (newLength > 12) {
            return NO;
        }
        
        NSString *replacementString = [NSString stringWithFormat:@"%@%@%@", [textField.text substringToIndex:range.location], string, [textField.text substringFromIndex:(range.location + range.length)]];
        
        replacementString = [replacementString stringByReplacingOccurrencesOfString:@"-" withString:@""];
        
        NSMutableString *hippenedString = [NSMutableString stringWithString:replacementString];
        
        if (hippenedString.length > 3 && hippenedString.length < 7) {
            [hippenedString insertString:@"-" atIndex:3];
        } else if (hippenedString.length > 6) {
            [hippenedString insertString:@"-" atIndex:6];
            [hippenedString insertString:@"-" atIndex:3];
        }
        
        textField.text = hippenedString;
        // retuning NO as we are itself modifying text of textfiled as needed
        return NO;
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.pickupNameText) {
        [self.pickupPhoneText becomeFirstResponder];
    } else if (textField == self.pickupPhoneText) {
        [self performSegueWithIdentifier:@"showConfirmCode" sender:textField];
    } else if (textField == self.loginPhoneText) {
        [self.loginPassText becomeFirstResponder];
    } else if (textField == self.loginPassText) {
        [self loginDidTouch:textField];
    }
    
    return YES;
}

#pragma mark - IBAction

- (IBAction)loginDidTouch:(id)sender {
    if (![self checkNetwork]) {
        return;
    }
    
    if ([self exceedMaxAmount:@"max_paywithcard"])
        return;

    [self.view endEditing:YES];
    
    if (self.loginPhoneText.text.length == 0) {
        [UIAlertView showWithTitle:NSLocalizedString(@"Missing phone number", @"") message:NSLocalizedString(@"Please enter your phone number to log in.", @"") cancelButtonTitle:NSLocalizedString(@"Close", @"") otherButtonTitles:nil tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
            [self.loginPhoneText becomeFirstResponder];
        }];
        
        return;
    } else if (self.loginPassText.text.length == 0) {
        [UIAlertView showWithTitle:NSLocalizedString(@"Missing password", @"") message:NSLocalizedString(@"Please enter your password to log in.", @"") cancelButtonTitle:NSLocalizedString(@"Close", @"") otherButtonTitles:nil tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
            [self.loginPassText becomeFirstResponder];
        }];
        
        return;
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.001 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[MOManager sharedInstance] loginAppUser:trim(self.loginPhoneText.text) password:self.loginPassText.text success:^(id user) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [UIAlertView showWithTitle:NSLocalizedString(@"Edit profile?", @"")
                               message:NSLocalizedString(@"Do you want to update your account before submitting order?", @"")
                     cancelButtonTitle:NSLocalizedString(@"Submit", @"")
                     otherButtonTitles:@[NSLocalizedString(@"Edit", @"")]
                              tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                  if (buttonIndex == 0) {
                                      [self performSegueWithIdentifier:@"showPickupTime" sender:user];
                                  } else {
                                      // Edit
                                      [self performSegueWithIdentifier:@"showUserInfo" sender:user];
                                  }
                              }];
        } failure:^(NSError *error) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [UIAlertView showWithTitle:NSLocalizedString(@"Invalid credential", @"")
                               message:NSLocalizedString(@"Please check your phone/password and try again.", @"")
                     cancelButtonTitle:NSLocalizedString(@"OK", @"")
                     otherButtonTitles:nil
                              tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                  [self.loginPhoneText becomeFirstResponder];
                              }];
        }];
    });
}

- (IBAction)registerDidTouch:(id)sender {
    [self performSegueWithIdentifier:@"showUserInfo" sender:nil];
}

#pragma mark - Others

- (BOOL)exceedMaxAmount:(NSString*)key {
    if (!self.store || !self.order)
        return YES;
    
    double max = [@"max_paywhenpickup" isEqualToString:key] ? [self.store.max_paywhenpickup doubleValue] : [self.store.max_paywithcard doubleValue];
    if (max <= 0.0f)
        return NO;
    
    double total = self.order.totalAmount;
    if (total > max) {
        NSString *message = NSLocalizedString(@"Sorry, the maximum amount that you can order is $%1$.2f. But your total is $%2$.2f.", @"");
        message = [NSString stringWithFormat:message, max, total];
        [UIAlertView showWithTitle:NSLocalizedString(@"Exceed max amount", @"") message:message cancelButtonTitle:NSLocalizedString(@"Close", @"") otherButtonTitles:nil tapBlock:nil];
        return YES;
    }
    return NO;
}


@end

