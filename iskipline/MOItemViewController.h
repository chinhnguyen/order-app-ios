//
//  MOItemViewController.h
//  iskipline
//
//  Created by Chinh Nguyen on 12/18/14.
//  Copyright (c) 2014 Chinh Nguyen. All rights reserved.
//

#import "MOOrderViewController.h"

@interface MOItemViewController : MOOrderViewController<UITableViewDataSource, UITableViewDelegate>

#pragma mark - IBOutlet

@property (weak, nonatomic) IBOutlet UILabel *labelCheckOut;
@property (weak, nonatomic) IBOutlet UIImageView *categoryImageView;
@property (weak, nonatomic) IBOutlet UILabel *categoryDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *categoryNameLabel;

#pragma mark - Propertise

@property (weak, nonatomic) NSDictionary *category;

@end
