//
//  MOCategoryTableViewCell.h
//  iskipline
//
//  Created by Chinh Nguyen on 12/17/14.
//  Copyright (c) 2014 Chinh Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MOCategoryTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *bgImageView;

@end
