//
//  ZTCustomSegue.h
//  iskipline
//
//  Created by Chinh Nguyen on 2/15/15.
//  Copyright (c) 2015 Chinh Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZTCustomSegue : UIStoryboardSegue

@end

@interface ZTNoAnimationPushSegue : UIStoryboardSegue

@end
