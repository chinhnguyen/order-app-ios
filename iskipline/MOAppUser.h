//
//  SOAppUser.h
//  SelfOrder
//
//  Created by PC02 on 1/15/14.
//  Copyright (c) 2014 nexmac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MOAppUser : NSObject

//@property (nonatomic, retain) NSString* username;
@property (nonatomic, retain) NSString* password;
@property (nonatomic, retain) NSString* rawpass;
@property (nonatomic, retain) NSString* firstname;
@property (nonatomic, retain) NSString* lastname;
@property (nonatomic, retain) NSString* email;
@property (nonatomic, retain) NSString* phone;
@property (nonatomic, retain) NSString* address;
@property (nonatomic, retain) NSString* zip;
@property (nonatomic, retain) NSString* level;
@property (nonatomic, retain) NSNumber* deleted;
@property (nonatomic, retain) NSNumber* order_allowed;
@property (nonatomic, retain) NSDate* day_created;
@property (nonatomic, retain) NSNumber* reward_points;
@property (nonatomic, retain) NSDictionary* account_links;

@property (nonatomic, retain) NSString* cardNumber;
@property (nonatomic, retain) NSString* cardType;
@property (nonatomic, retain) NSString* monthExpired;
@property (nonatomic, retain) NSString* yearExpired;
@property (nonatomic, retain) NSString* customerId;

- (BOOL)paymentRegistered;

@end
