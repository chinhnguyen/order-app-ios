//
//  main.m
//  iskipline
//
//  Created by Chinh Nguyen on 12/15/14.
//  Copyright (c) 2014 Chinh Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
