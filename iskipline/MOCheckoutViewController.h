//
//  MOCheckoutViewController.h
//  iskipline
//
//  Created by Chinh Nguyen on 12/19/14.
//  Copyright (c) 2014 Chinh Nguyen. All rights reserved.
//

#import "SSIDFinder.h"
#import "MOReceiptPrinter.h"
#import "MOBaseViewController.h"
#import "UATitledModalPanel.h"

@interface MOCheckoutViewController : MOBaseViewController<UITableViewDataSource, UITableViewDelegate>

#pragma mark - Propertises

@property (nonatomic, strong) NSString *tableNumber;
@property (nonatomic, strong) MOStore *store;
@property (nonatomic, strong) MOCart *order;

#pragma mark - IBOutlet

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *mBtnContinue;
@property (weak, nonatomic) IBOutlet UILabel *subtotalLabel;
@property (weak, nonatomic) IBOutlet UILabel *discountLabel;
@property (weak, nonatomic) IBOutlet UILabel *taxLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalLabel;
@property (weak, nonatomic) IBOutlet UILabel *disountInfoLabel;

#pragma mark - IBAction

- (IBAction)mBtnContinue:(id)sender;

@end

typedef void (^PrinterSelected)(NSString*);
typedef void (^SkipPrinting)();

@interface MOSelectPrinterView: UATitledModalPanel<UITableViewDataSource, UITableViewDelegate>

- (instancetype)initWithFrame:(CGRect)frame title:(NSString *)title NS_DESIGNATED_INITIALIZER;

@property(readwrite, copy) PrinterSelected printerSelected;
@property(readwrite, copy) SkipPrinting skip;

@end
