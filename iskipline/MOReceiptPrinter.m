//
//  MOPrinters.m
//  iskipline
//
//  Created by TIENPHAM on 9/2/15.
//  Copyright (c) 2015 Chinh Nguyen. All rights reserved.
//

#import "MOReceiptPrinter.h"

@implementation MOReceiptPrinter


#pragma mark - Static

+ (NSString*)printerAddressFor:(NSString*)name {
    NSString *ipAddress = [SSIDFinder getIPAddress];
    if (nil == ipAddress || [ipAddress isEqualToString:@""])
        return nil;
    NSArray *ipComponents = [ipAddress componentsSeparatedByString:@"."];
    if (!ipComponents || ipComponents.count < 4)
        return nil;
    NSString *printerAddress = [NSString stringWithFormat:@"%@.%@.%@.%@", [ipComponents objectAtIndex:0], [ipComponents objectAtIndex:1], [ipComponents objectAtIndex:2], [kMOPrinters valueForKey:name]];
#ifdef TESTING
//    printerAddress = @"192.168.10.69";
#endif
    return printerAddress;
}


+ (void)printReceipt:(NSString*)printerName items:(NSArray *)orderItems table:(NSString *)table cart:(MOCart *)cart store:(MOStore *)store success:(void (^)(NSString* printer))success failure:(void (^)(NSString* printer, NSError *error))failure {
    // Check input
    if (!orderItems || orderItems.count == 0 || !cart || !store) {
        failure(printerName, [NSError errorWithDomain:@"ReceiptPrint" code:400 userInfo:@{NSLocalizedDescriptionKey: @"Bad inputs"}]);
        return;
    }
    
    // Make sure we always have success
    if (!success) success = ^(NSString* printer) {
        NSLog(@"Printer %@: print receipt OK", printerName);
    };
    // Make sure we always have failure
    if (!failure) failure = ^(NSString* printer, NSError* error) {
        NSLog(@"Printer %@: print receipt faile with error %@", printerName, error);
    };

    // Print in the background
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

        NSString *address = [MOReceiptPrinter printerAddressFor:printerName];
        if (!address || address.length == 0) {
            failure(printerName, [NSError errorWithDomain:@"ReceiptPrint" code:500 userInfo:@{NSLocalizedDescriptionKey: @"Could not find and printer's IP address"}]);
            return;
        }
        
//        NSLog(@"Printer %@ was found at %@", printerName, address);
        // Create a printer
        EposPrint *printer = [EposPrint new];
        if (!printer) {
            failure(printerName, [NSError errorWithDomain:@"ReceiptPrint" code:500 userInfo:@{NSLocalizedDescriptionKey: @"Could not create printer"}]);
            return;
        }
        int status;
        // Open the printer with error monitoring
        status = [printer openPrinter:EPOS_OC_DEVTYPE_TCP                          DeviceName:address Enabled:EPOS_OC_FALSE Interval:EPOS_OC_PARAM_DEFAULT                              Timeout:EPOS_OC_PARAM_DEFAULT];
        if (status != EPOS_OC_SUCCESS) {
            failure(printerName, [NSError errorWithDomain:@"ReceiptPrint" code:status userInfo:@{NSLocalizedDescriptionKey: @"Could not open printer"}]);
            return;
        }
        // Get printer status
        unsigned long printerStatus = 0;
        unsigned long batterStatus = 0;
        status = [printer getStatus:&printerStatus Battery:&batterStatus];
        // Inform if failed getting status
        if (status != EPOS_OC_SUCCESS) {
            failure(printerName, [NSError errorWithDomain:@"ReceiptPrint" code:status userInfo:@{NSLocalizedDescriptionKey: @"Could not get printer's status"}]);
            [printer closePrinter];
            return;
        }
        // Make sure printer is in good condition
        if ((printerStatus & EPOS_OC_ST_OFF_LINE) == EPOS_OC_ST_OFF_LINE) {
            failure(printerName, [NSError errorWithDomain:@"ReceiptPrint" code:EPOS_OC_ST_OFF_LINE userInfo:@{NSLocalizedDescriptionKey: @"Printer is offline"}]);
            [printer closePrinter];
            return;
        }
        // Make sure printer is in good condition
        if ((printerStatus & EPOS_OC_ST_NO_RESPONSE) == EPOS_OC_ST_NO_RESPONSE) {
            failure(printerName, [NSError errorWithDomain:@"ReceiptPrint" code:EPOS_OC_ST_NO_RESPONSE userInfo:@{NSLocalizedDescriptionKey: @"No response from printer"}]);
            [printer closePrinter];
            return;
        }
        // Start printing transaction with error controlling
        status = [printer beginTransaction];
        if(status != EPOS_OC_SUCCESS) {
            failure(printerName, [NSError errorWithDomain:@"ReceiptPrint" code:status userInfo:@{NSLocalizedDescriptionKey: @"Could not start print transaction"}]);
            [printer closePrinter];
            return;
        }
        // Build the receipt data
        NSError *error;
        EposBuilder* builder = [MOReceiptPrinter buildReceiptData:orderItems table:table cart:cart store:store error:&error];
        // If error, inform and return
        if(error) {
            failure(printerName, [NSError errorWithDomain:@"ReceiptPrint" code:501 userInfo:@{NSLocalizedDescriptionKey: @"Could not build receipt data"}]);
            [printer closePrinter];
            return;
        }
        // Start printing
        status = [printer sendData:builder Timeout:SEND_TIMEOUT Status:&printerStatus Battery:&batterStatus];
        if(status != EPOS_OC_SUCCESS) {
            failure(printerName, [NSError errorWithDomain:@"ReceiptPrint" code:status userInfo:@{NSLocalizedDescriptionKey: @"Could not print receipt"}]);
        } else {
            success(printerName);
        }
        // End the transaction - don't care about error
        [printer endTransaction];
        // Close the printer
        [printer closePrinter];
        // Clear command buffer
        if (builder) [builder clearCommandBuffer];
    });
}

+ (EposBuilder*)buildReceiptData:(NSArray *)orderItems table:(NSString *)table cart:(MOCart *)cart store:(MOStore *)store error:(NSError**)error {
    double subTotal = 0.0;
    double tax = 0.0;
    NSLocale *localeUS = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    
    NSString* (^uppercase)(NSString*) = ^(NSString*src) {
        return [src uppercaseStringWithLocale:localeUS];
    };
    
    // Initialize print builder
    EposBuilder *builder = [[EposBuilder alloc] initWithPrinterModel:@"TM-T20"                                                                Lang:EPOS_OC_MODEL_ANK];
    if (!builder) {
        *error = [NSError errorWithDomain:@"ReceiptPrint" code:503 userInfo:@{NSLocalizedDescriptionKey: @"Could not create print builder"}];
        return nil;
    }
    // General setup
    [builder addTextFont:EPOS_OC_FONT_A];
    [builder addTextAlign:EPOS_OC_ALIGN_CENTER];
    [builder addTextLineSpace:30];
    [builder addTextLang:EPOS_OC_MODEL_ANK];
    [builder addTextSize:1 Height:1];
    [builder addTextStyle:EPOS_OC_FALSE Ul:EPOS_OC_FALSE Em:EPOS_OC_FALSE Color:EPOS_OC_COLOR_1];
    [builder addTextSize:2 Height:2];
    // Line 1: Middle Title: TAKE-OUT
    // [builder addText:@"TAKE-OUT"];
    // Add free line
    [builder addFeedLine:2];
    // Line 2: Table Number + Barcode
    [builder addTextAlign:EPOS_OC_ALIGN_LEFT];
    [builder addText:@"TABLE "];
    [builder addTextStyle:EPOS_OC_TRUE Ul:EPOS_OC_FALSE Em:EPOS_OC_FALSE Color:EPOS_OC_COLOR_1];
    [builder addText:[NSString stringWithFormat:@"#%@", table]];
    [builder addTextStyle:EPOS_OC_FALSE Ul:EPOS_OC_FALSE Em:EPOS_OC_FALSE Color:EPOS_OC_COLOR_1];
//    [builder addTextSize:1 Height:1];
//    [builder addTextAlign:EPOS_OC_ALIGN_RIGHT];
    NSDate *currentDate = [NSDate date];
    NSString *receiptNumber = [self createReceiptNumber:currentDate];
//    NSLog(@"%d", [builder addBarcode:receiptNumber
//                   Type:EPOS_OC_BARCODE_CODE39
//                    Hri:EPOS_OC_HRI_NONE
//                   Font:EPOS_OC_FONT_A
//                  Width:2
//                 Height:60]);
    // Add free line
    [builder addFeedLine:2];
    // Line 3: Receipt Number + Station
    [builder addTextAlign:EPOS_OC_ALIGN_LEFT];
    [builder addTextSize:1 Height:1];
    [builder addText:[NSString stringWithFormat:@"Receipt: #%@", receiptNumber]];
    // Line 4: DateTime + Customer Name
    [builder addFeedLine:1];
    [builder addText:[NSString stringWithFormat:@"%@", [self getReceiptDate:currentDate]]];
    // Line 5: -...-
    [builder addFeedLine:1];
    [builder addText:@"------------------------------------------------"];
    // Line 6: Begin add order item categories
    [builder addFeedLine:1];
    for (int i = 0; i < orderItems.count ; i++) {
        NSDictionary *category = [orderItems objectAtIndex:i];
        NSString *categoryName = uppercase([category.allKeys objectAtIndex:0]);
        [builder addTextSize:1 Height:2];
        [builder addText:categoryName];
        [builder addFeedLine:1];
        NSArray *orderList = category[categoryName];
        for (NSInteger j = 0; j < orderList.count; j++) {
            double categoryPrice = 0.0;
            NSDictionary *orderInfo = [orderList objectAtIndex:j];
            double priceItem = [orderInfo[@"quatityItem"] integerValue] * [orderInfo[@"priceItem"] doubleValue];
            categoryPrice += priceItem;
            NSString *itemCateLeft = [NSString stringWithFormat:@"%@ x #%ld %@ %@", orderInfo[@"quatityItem"], (long)j + 1, uppercase(categoryName), uppercase(orderInfo[@"nameItem"])];
            [self addLineWithBuilder:builder stringLeft:itemCateLeft stringRight:[NSString stringWithFormat:@"$%.2f", priceItem]];
            NSArray *orderModifierList = orderInfo[@"item"];
            int modifierIndex = 0;
            for (int k = 0; k < orderModifierList.count; k++) {
                NSDictionary *modifierItem = [orderModifierList objectAtIndex:k];
                if ([@"Note" isEqualToString:modifierItem[@"typeModifierOrder"]]) {
                    NSString *noteItem = uppercase([NSString stringWithFormat:@"   NOTE: %@", modifierItem[@"noteModifierOrder"]]);
                    [self addLineWithBuilder:builder stringText:noteItem];
                } else if ([@"Add" isEqualToString:modifierItem[@"typeModifierOrder"]]) {
                    double priceModifier = [modifierItem[@"quantityModifier"] integerValue] * [modifierItem[@"priceModifier"] doubleValue];
                    categoryPrice += priceModifier;
                    NSString *modifierItemLeft = [NSString stringWithFormat:@"   %@ x #%ld %@", modifierItem[@"quantityModifier"], (long)modifierIndex + 1, uppercase(modifierItem[@"nameModifier"])];
                    [self addLineWithBuilder:builder stringLeft:modifierItemLeft stringRight:[NSString stringWithFormat:@"$%.2f", priceModifier]];
                    modifierIndex++;
                }
            }
            NSDictionary *item = [store findItemById:orderInfo[@"idItem"]];
            if ([@"yes" isEqualToString:item[@"togo_tax"]]) {
                tax += categoryPrice * kMOTaxRate;
            }
            subTotal += categoryPrice;
        }
    }
    [builder addTextSize:1 Height:1];
    // Line 7: -...-
    [builder addText:@"------------------------------------------------"];
    [builder addFeedLine:1];
    // Line 8:
//    [orderCart calculateTotalDetail:&subtotal andTax:&tax];
    int discountRate = [store discount];
    double discount = 0.0;
    if (discountRate > 0)
        discount = subTotal * discountRate / 100.0;
    else
        discount = 0.0;
    double total = subTotal + tax - discount;
    // Testing
//    double cashTender = 100.0;
    [self addLineWithBuilder:builder stringLeft:@"Subtotal" stringRight:[NSString stringWithFormat:@"$%.2f", subTotal]];
    [self addLineWithBuilder:builder stringLeft:@"Sales Tax" stringRight:[NSString stringWithFormat:@"$%.2f", tax]];
    [self addLineWithBuilder:builder stringLeft:@"TOTAL DUE" stringRight:[NSString stringWithFormat:@"$%.2f", total]];
//    [self addLineWithBuilder:builder stringLeft:@"" stringRight:[NSString stringWithFormat:@"Change       $%.2f", cashTender - total]];
    // Finish printing
    [builder addFeedLine:1];
    // Print the barcode
    [builder addTextAlign:EPOS_OC_ALIGN_CENTER];
    [builder addBarcode:receiptNumber
                   Type:EPOS_OC_BARCODE_CODE39
                    Hri:EPOS_OC_HRI_NONE
                   Font:EPOS_OC_FONT_A
                  Width:2
                 Height:60];
    // Cut paper
    [builder addFeedLine:1];
    [builder addCut:EPOS_OC_CUT_FEED];
    // Return the builder
    return builder;
}

#pragma mark - Utils

+ (NSString *)createReceiptNumber:(NSDate *)date {
    NSInteger randomNumber = arc4random() % 100;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYMMddHHmmss"];
    NSString *dateNow = [formatter stringFromDate:date];
    NSString *result = [NSString stringWithFormat:@"%@%ld", dateNow, (long)randomNumber];
    if (result && ![result isEqualToString:@""]) {
        return result;
    }
    return result;
}

+ (NSString *)getReceiptDate:(NSDate *)date {
    NSDateFormatter *printFormatter = [[NSDateFormatter alloc] init];
    [printFormatter setDateFormat:@"dd/MM/yyyy HH:mm a"];
    return [printFormatter stringFromDate:date];
}

+ (EposBuilder *)addLineWithBuilder:(EposBuilder *)builder stringText:(NSString *)text {
    [builder addText:[MOReceiptPrinter makeLine:text rightString:@""]];
    [builder addFeedLine:1];
    return builder;
}

+ (EposBuilder *)addLineWithBuilder:(EposBuilder *)builder stringLeft:(NSString *)textLeft stringRight:(NSString *)textRight {
    [builder addText:[MOReceiptPrinter makeLine:textLeft rightString:textRight]];
    [builder addFeedLine:1];
    return builder;
}

+ (NSString *)makeLine:(NSString *)leftString rightString:(NSString *)rightString {
    if (nil == leftString) {
        leftString = @"";
    }
    if (nil == rightString) {
        rightString = @"";
    }
    if (leftString.length + rightString.length < MAX_TEXT_IN_LINE) {
        return [NSString stringWithFormat:@"%@%@%@", leftString, [self createWhitespace:MAX_TEXT_IN_LINE - rightString.length - leftString.length], rightString];
    } else {
        return [NSString stringWithFormat:@"%@\n%@%@", leftString, [self createWhitespace:MAX_TEXT_IN_LINE - rightString.length], rightString];
    }
}

+ (NSString *)createWhitespace:(NSInteger)num {
    NSString *result = @"";
    for (NSInteger i = 0; i < num; i++) {
        result = [result stringByAppendingString:@" "];
    }
    return result;
}

@end
