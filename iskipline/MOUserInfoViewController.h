//
//  MOUserInfoViewController.h
//  iskipline
//
//  Created by Chinh Nguyen on 2/25/15.
//  Copyright (c) 2015 Chinh Nguyen. All rights reserved.
//

#import "MOBaseViewController.h"

@interface MOUserInfoViewController : MOBaseViewController

@property (nonatomic, strong) MOStore *store;
@property (nonatomic, strong) MOCart *order;
@property (nonatomic, strong) NSDictionary *user;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
