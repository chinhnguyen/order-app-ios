//
//  MOOrder.m
//  iskipline
//
//  Created by Chinh Nguyen on 12/24/14.
//  Copyright (c) 2014 Chinh Nguyen. All rights reserved.
//

#import "MOOrder.h"

@implementation MOOrder

@synthesize StatusID, TransactionAmount, TransactionID;
@synthesize customerName, address, phone, email, cashier;
@synthesize kindRemind, datePickup, timePickup, dateRemind, timeRemind, withAlert;
@synthesize ereceipt, promo_subscription, dayCreate, depositAmount, express_code, station;
@synthesize order;

@end
