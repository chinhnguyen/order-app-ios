//
//  MOConfirmCodeViewController.h
//  iskipline
//
//  Created by Chinh Nguyen on 12/21/14.
//  Copyright (c) 2014 Chinh Nguyen. All rights reserved.
//

#import "MOBaseViewController.h"
#import "ZTTrumpia.h"

@interface MOConfirmCodeViewController : MOBaseViewController

@property (nonatomic, strong) MOStore *store;
@property (nonatomic, strong) MOCart* order;

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *phone;

@end

@interface MOConfirmCodeTableViewController : UITableViewController<UITextFieldDelegate>

@property (nonatomic, strong) MOStore *store;
@property (nonatomic, strong) MOCart* order;

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *phone;

@property (weak, nonatomic) IBOutlet UITextField *codeText;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;

@end
