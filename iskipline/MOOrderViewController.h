//
//  MOOrderViewController.h
//  iskipline
//
//  Created by Chinh Nguyen on 12/17/14.
//  Copyright (c) 2014 Chinh Nguyen. All rights reserved.
//

#import "MOBaseViewController.h"

@interface MOOrderViewController : MOBaseViewController

@property (nonatomic, strong) MOStore *store;
@property (nonatomic, strong) MOCart *order;

@property (nonatomic, strong) NSString *tableNumber;

@property (weak, nonatomic) IBOutlet UIImageView *storeLogoImageView;
@property (weak, nonatomic) IBOutlet UILabel *cartDetailLabel;

@property (weak, nonatomic) IBOutlet UIView *cartView;

@property (weak, nonatomic) IBOutlet UILabel *storeNameLabel;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIProgressView *storeDataPullProgressView;

@end
