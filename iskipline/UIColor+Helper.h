//
//  UIColor+Helper.h
//  SelfOrder
//
//  Created by Chinh Nguyen on 12/25/13.
//  Copyright (c) 2013 nexmac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIColor(Helper)

+ (UIColor *)colorFromHexString:(NSString *)hexString;

@end
