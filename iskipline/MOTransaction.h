//
//  MOTransaction.h
//  iskipline
//
//  Created by Chinh Nguyen on 12/24/14.
//  Copyright (c) 2014 Chinh Nguyen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MOTransaction : NSObject
// Custom info
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *entryMethod;
// Clerk
@property (nonatomic, strong) NSString *clerkId;
// Card info
@property (nonatomic, strong) NSString *cardNumber;
@property (nonatomic, strong) NSString *cardName;
@property (nonatomic, strong) NSString *expiration;
@property (nonatomic, strong) NSString *cvv2;
// Customer vault info
@property (nonatomic, strong) NSString *customerId;
// Customer info
@property (nonatomic, strong) NSString *emailAddress;
// Billing info
@property (nonatomic, strong) NSString *billingFirstName;
@property (nonatomic, strong) NSString *billingLastName;
@property (nonatomic, strong) NSString *billingFullName;
@property (nonatomic, strong) NSString *billingAddress;
@property (nonatomic, strong) NSString *billingZipCode;
@property (nonatomic, strong) NSString *billingCity;
@property (nonatomic, strong) NSString *billingState;
// Transaction info
@property (nonatomic, assign) double subTotalAmount;
@property (nonatomic, assign) double discountAmount;
@property (nonatomic, assign) double totalAmount;
@property (nonatomic, assign) double taxAmount;
// Return values
@property (nonatomic, strong) NSString *transactionID;
@property (nonatomic, strong) NSString *statusCode;
@property (nonatomic, strong) NSString *postedDate;
@property (nonatomic, strong) NSString *authorizationCode;
@property (nonatomic, strong) NSString *responseCode;
@property (nonatomic, strong) NSString *responseMessage;
// Control info
@property (nonatomic, strong) NSString *errorDetail;

@end
