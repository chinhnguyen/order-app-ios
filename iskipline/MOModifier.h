//
//  SOModifier.h
//  SelfOrder
//
//  Created by PC02 on 12/31/13.
//  Copyright (c) 2013 nexmac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MOModifier : NSObject

@property (nonatomic, retain) NSString* ID;
@property (nonatomic, retain) NSString* category;
@property (nonatomic, retain) NSString* name1;
@property (nonatomic, retain) NSString* name2;
@property (nonatomic, retain) NSString* name3;
@property (nonatomic, retain) NSNumber* price;
@property (nonatomic, retain) NSNumber* no;
@property (nonatomic, retain) NSNumber* extra;
@property (nonatomic, retain) NSNumber* less;
@property (nonatomic, retain) NSNumber* onside;

@end
