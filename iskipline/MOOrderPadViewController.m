//
//  MOOrderPadViewController.m
//  iskipline
//
//  Created by TIENPHAM on 8/30/15.
//  Copyright (c) 2015 Chinh Nguyen. All rights reserved.
//

#import "MOManager.h"
#import "SSIDFinder.h"
#import "MOOrderPadViewController.h"
#import "MOCategoryViewController.h"
#import "MODeviceUID.h"
#import "AFNetworking.h"

@implementation MOOrderPadViewController {
    CBLDatabase* _ssidListDb;
    CBLReplication* _ssidPull;
    BOOL _shouldPullOnAppear;
}

@synthesize mTFTableNum;
@synthesize orderTableView, nextButton;

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    _shouldPullOnAppear = NO;
    [self pullData];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kCBLReplicationChangeNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    mTFTableNum.text = @"";
    if (_shouldPullOnAppear) [self pullData];
}

#pragma mark - Replication

- (void)pullData {
    // Listen to store data pulling changes
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ssidReplicationChanged:) name:kMOSsidReplicationChangedNofification object:nil];
    
    // Loading data asynchronously
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    _ssidPull = [[MOManager sharedInstance].ssidListDb createPullReplication:kMOSsidSyncURL];
    _ssidPull.continuous = NO;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ssidReplicationChanged:) name: kCBLReplicationChangeNotification object:_ssidPull];
    [_ssidPull start];
}

- (void)ssidReplicationChanged:(NSNotification*)n {
    CBLReplication *r = n.object;
    
    if (r.status == kCBLReplicationOffline) {
//        dispatch_async(dispatch_get_main_queue(), ^{
//            _shouldPullOnAppear = YES;
//            [UIAlertView showWithTitle:NSLocalizedString(@"No working connection", @"")message:NSLocalizedString(@"Please connect to a valid Wi-Fi connection and reopen the application.", @"") cancelButtonTitle:NSLocalizedString(@"Quit", @"") otherButtonTitles:nil tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
//                exit(0);
//            }];
//            // Found a valid store, hide the loading indicator ...
//            [MBProgressHUD hideHUDForView:self.view animated:YES];
//        });
    } else if (r.status == kCBLReplicationActive) {
    } else if (r.status == kCBLReplicationStopped) {
        // Finished - now query the store by ssid
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            NSString *ssid = [SSIDFinder getCurrWifiSSID];
            currentStore = [[MOManager sharedInstance] findStoreBySSID:ssid];
            [self verify:ssid store:currentStore.account];
            dispatch_async(dispatch_get_main_queue(), ^{
                if (currentStore == nil) {
                    _shouldPullOnAppear = YES;
                    // No valid data found
                    [UIAlertView showWithTitle:NSLocalizedString(@"Store not found", @"")message:NSLocalizedString(@"Could not find a store corresponde to connected Wi-Fi. Please check with service provider and try again.", @"") cancelButtonTitle:NSLocalizedString(@"Quit", @"") otherButtonTitles:nil tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                        exit(0);
                    }];
                }
                // ... and init the layout
                [self initNumberPad];
                // Found a valid store, hide the loading indicator ...
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                _shouldPullOnAppear = NO;
            });
        });
    }
}

- (void)verify:(NSString*)ssid store:(NSString*)store {
    if (!ssid || !store) return;
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *parameters = @{ @"device": [MODeviceUID uid], @"ssid": ssid, @"store": store };
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    NSString *authValue = [NSString stringWithFormat:@"Bearer %@", @"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhcHAiOiJpcGhvbmUiLCJuYW1lIjoiQ2hpbmggTmd1eWVuIn0.3leqGeYmXaR4jpryKZpJS9ytA0ExGpf--GC21NuXf7k"];
    [manager.requestSerializer setValue:authValue forHTTPHeaderField:@"Authorization"];
    [manager POST:@"http://mo-api.willbe.vn/license" parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if (!responseObject || [[responseObject valueForKey:@"result"] intValue] != 1) {
            [self disableApp];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self onValidationError];
    }];
}

- (void)disableApp {
    [UIAlertView showWithTitle:NSLocalizedString(@"Application locked", @"") message:NSLocalizedString(@"Application has failed validation and willbe blocked!", @"") cancelButtonTitle:NSLocalizedString(@"Quit", @"") otherButtonTitles:nil tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
        exit(0);
    }];
}

- (void)onValidationError {
    NSNumber *count = [[NSUserDefaults standardUserDefaults] valueForKey:@"validationErrorCount"];
    if (!count) count = [NSNumber numberWithInt:1];
    else count = [NSNumber numberWithInt:[count intValue] + 1];
    [[NSUserDefaults standardUserDefaults] setValue:count forKey:@"validationErrorCount"];
    if ([count intValue] > 5) [self disableApp];
}

#pragma mark - Initial View

- (void)initNumberPad {
    NSArray *inputArr = @[@"7", @"8", @"9", @"4", @"5", @"6", @"1", @"2", @"3" ,
                          @"CLR" , @"0"];
    CGRect orderTableFrame = orderTableView.frame;
    CGRect nextButtonFrame = nextButton.frame;
    CGFloat screenWidth = [[UIScreen mainScreen] bounds].size.width;
    CGFloat screenHeight = nextButtonFrame.origin.y - (orderTableFrame.size.height + orderTableFrame.origin.y);
    CGFloat currHeight = 20 + orderTableFrame.size.height + orderTableFrame.origin.y;
    CGFloat spaceSize = 5;
    CGFloat buttonSize = (screenHeight - (20 * 2 + spaceSize * 3)) / 4;
    CGFloat currWidth = (screenWidth - (buttonSize * 3 + spaceSize * 2)) / 2;
    for (int i = 0; i < 4; i++) {
        if (i < 3) {
            for (int j = 0; j < 3; j++) {
                UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
                [button addTarget:self action:@selector(tapNumPad:) forControlEvents:UIControlEventTouchUpInside];
                [button setTitle:[inputArr objectAtIndex:i * 3 + j] forState:UIControlStateNormal];
                button.frame = CGRectMake(currWidth + (j * (buttonSize + spaceSize)), currHeight + (i * (buttonSize + spaceSize)), buttonSize, buttonSize);
                [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                button.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-CondensedBold" size:42.0f];
                [button setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:225.0f/255 green:225.0f/255 blue:225.0f/255 alpha:1.0f]] forState:UIControlStateNormal];
                [button setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:225.0f/255 green:225.0f/255 blue:225.0f/255 alpha:0.6f]] forState:UIControlStateHighlighted];
                [self.view addSubview:button];
            }
        } else {
            for (int j = 0; j < 2; j++) {
                UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
                [button addTarget:self action:@selector(tapNumPad:) forControlEvents:UIControlEventTouchUpInside];
                // CLR button
                if (j == 0) {
                    [button setTitle:[inputArr objectAtIndex:9] forState:UIControlStateNormal];
                    button.frame = CGRectMake(currWidth + (j * (buttonSize + spaceSize)), currHeight + (i * (buttonSize + spaceSize)), buttonSize * 2 + spaceSize, buttonSize);
                    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                    button.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:38.0f];
                    [button setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:237.0f/255 green:142.0f/255 blue:2.0f/255 alpha:1.0f]] forState:UIControlStateNormal];
                    [button setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:237.0f/255 green:142.0f/255 blue:2.0f/255 alpha:0.6f]] forState:UIControlStateHighlighted];
                }
                // 0 button
                else {
                    [button setTitle:[inputArr objectAtIndex:10] forState:UIControlStateNormal];
                    button.frame = CGRectMake(currWidth + (2 * (buttonSize + spaceSize)), currHeight + (i * (buttonSize + spaceSize)), buttonSize, buttonSize);
                    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                    button.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-CondensedBold" size:42.0f];
                    [button setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:225.0f/255 green:225.0f/255 blue:225.0f/255 alpha:1.0f]] forState:UIControlStateNormal];
                    [button setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:225.0f/255 green:225.0f/255 blue:225.0f/255 alpha:0.6f]] forState:UIControlStateHighlighted];
                }
                button.tag = i * 10 + j;
                [self.view addSubview:button];
            }
        }
    }
}

#pragma mark - Tap Action

- (void)tapNumPad:(id)sender {
    if (mTFTableNum.text.length >= 2)
        return;
    NSInteger buttonTag = ((UIButton *)sender).tag;
    NSString *currTFStr = mTFTableNum.text;
    mTFTableNum.text = (buttonTag == 30 && currTFStr.length > 0) ? @"" : [currTFStr stringByAppendingString:((UIButton *)sender).currentTitle];
    // If textfield tabel num match 2 digits syntax => Push to Categories Screen
    if ([self checkIfTableNumberTextFieldValid]) {
        dispatch_time_t delay = dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC);
        dispatch_after(delay, dispatch_get_main_queue(), ^{
            [self performSegueWithIdentifier:@"showCategories" sender:self];
        });
    } else if (mTFTableNum.text.length >= 2) {
        [UIAlertView showWithTitle:@"Table Number Wrong Format" message:@"Order table number must be from 01 to 99. Please input number table again" cancelButtonTitle:@"" otherButtonTitles:nil tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
            mTFTableNum.text = @"";
        }];
    }
}

#pragma mark - Utils

- (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

- (BOOL)checkIfTableNumberTextFieldValid {
    BOOL isValid = NO;
    NSString *currTableNumString = mTFTableNum.text;
    if ((nil == currTableNumString) || [currTableNumString isEqualToString:@""])
        return NO;
    NSRegularExpression *regexTable = [NSRegularExpression regularExpressionWithPattern:@"\\d{2}" options:NSRegularExpressionCaseInsensitive error:nil];
    NSTextCheckingResult *match = [regexTable firstMatchInString:currTableNumString options:0 range:NSMakeRange(0, [currTableNumString length])];
    isValid = ((match != nil) && (![currTableNumString isEqualToString:@"00"]));
    return isValid;
}


#pragma mark - IBAction

- (IBAction)nextScreenButDidTouch:(id)sender {
    if (mTFTableNum.text.length >= 1) {
        [self performSegueWithIdentifier:@"showCategories" sender:self];
    }
}

#pragma mark - UINavigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showCategories"]) {
        MOCategoryViewController *destinationVC = segue.destinationViewController;
        destinationVC.store = currentStore;
        destinationVC.tableNumber = mTFTableNum.text;
    }
}

- (IBAction)orderSubmitted:(UIStoryboardSegue *)unwindSegue {
    
}

@end
