//
//  MOUserInfoViewController.m
//  iskipline
//
//  Created by Chinh Nguyen on 2/25/15.
//  Copyright (c) 2015 Chinh Nguyen. All rights reserved.
//

#import "MOUserInfoViewController.h"
#import <FormKit.h>
#import <BWSelectViewController.h>
#import "UIViewController+Utils.h"
#import "MOPickupTimeViewController.h"

@interface MOPhoneTextViewDelegate : NSObject<UITextFieldDelegate>

@end

@implementation MOPhoneTextViewDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    if (newLength > 12) {
        return NO;
    }
    
    NSString *replacementString = [NSString stringWithFormat:@"%@%@%@", [textField.text substringToIndex:range.location], string, [textField.text substringFromIndex:(range.location + range.length)]];
    
    replacementString = [replacementString stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    NSMutableString *hippenedString = [NSMutableString stringWithString:replacementString];
    
    if (hippenedString.length > 3 && hippenedString.length < 7) {
        [hippenedString insertString:@"-" atIndex:3];
    } else if (hippenedString.length > 6) {
        [hippenedString insertString:@"-" atIndex:6];
        [hippenedString insertString:@"-" atIndex:3];
    }
    
    textField.text = hippenedString;
    // retuning NO as we are itself modifying text of textfiled as needed
    return NO;
}

@end



@interface MOCardNumberTextViewDelegate : NSObject<UITextFieldDelegate>

@end

@implementation MOCardNumberTextViewDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    if (newLength > 19) {
        return NO;
    }
    
    NSString *replacementString = [NSString stringWithFormat:@"%@%@%@", [textField.text substringToIndex:range.location], string, [textField.text substringFromIndex:(range.location + range.length)]];
    
    replacementString = [replacementString stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    NSMutableString *hippenedString = [NSMutableString stringWithString:replacementString];
    
    if (hippenedString.length > 4 && hippenedString.length < 9) {
        [hippenedString insertString:@"-" atIndex:4];
    } else if (hippenedString.length > 9 && hippenedString.length < 14) {
        [hippenedString insertString:@"-" atIndex:8];
        [hippenedString insertString:@"-" atIndex:4];
    } else if (hippenedString.length > 14) {
        [hippenedString insertString:@"-" atIndex:12];
        [hippenedString insertString:@"-" atIndex:8];
        [hippenedString insertString:@"-" atIndex:4];
    }
    
    textField.text = hippenedString;
    // retuning NO as we are itself modifying text of textfiled as needed
    return NO;
}

@end



@interface MOUserInfoViewController () {
    FKFormModel *_formModel;
    MOAppUser *_userModel;
    
    
}

@end

@implementation MOUserInfoViewController

@synthesize user;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    [[UITableView appearanceWhenContainedIn:[MOUserInfoViewController class], nil] setSeparatorInset:UIEdgeInsetsMake(0, 16, 0, 0)];
    [[UITableViewCell appearanceWhenContainedIn:[MOUserInfoViewController class], nil] setSeparatorInset:UIEdgeInsetsMake(0, 16, 0, 0)];
    
    // Remove the top white space
    //    self.tableView.contentInset = UIEdgeInsetsMake(10, 0, 0, 0);
    self.tableView.rowHeight = 44;
    self.tableView.sectionHeaderHeight = 36;
    
    _formModel = [FKFormModel formTableModelForTableView:self.tableView navigationController:self.navigationController];
    _formModel.labelTextColor = [UIColor whiteColor];
    _formModel.valueTextColor = [UIColor colorWithWhite:0.9f alpha:0.9f];
    _formModel.valuePlaceholderTextColor = [UIColor colorWithWhite:0.9f alpha:0.5f];
    _formModel.validationNormalCellBackgroundColor = [UIColor colorWithWhite:0.9f alpha:0.1f];
    _formModel.validationErrorCellBackgroundColor = [UIColor colorWithWhite:0.9f alpha:0.1f];
    
    
    //    NSDictionary *placeholderAttributes =
    //    @{
    //      NSForegroundColorAttributeName: [UIColor colorWithWhite:0.9f alpha:0.6f]
    //      };
    
    // Customer cell configuration
    [_formModel configureCells:^(UITableViewCell *cell) {
        if ([cell isKindOfClass:[FKTextField class]]) {
            //            FKTextField *f = (FKTextField*)cell;
            //            NSString *placeholder = f.textField.placeholder ? f.textField.placeholder : @"";
            //            NSAttributedString *placeholderAttributedString = [[NSAttributedString alloc] initWithString:placeholder attributes:placeholderAttributes];
            //            f.textField.attributedPlaceholder =placeholderAttributedString;
            //            f.textField.extraDelegate = self;
        }
    }];
    
    
    // Define mapping
    [FKFormMapping mappingForClass:[MOAppUser class] block:^(FKFormMapping *mapping) {
        mapping.selectViewWillAppearBlock = ^(BWSelectViewController *vc) {
            
            UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, vc.view.frame.size.width, 44.0f)];
            headerView.backgroundColor = [UIColor blackColor];
            [vc.view addSubview:headerView];
            
            UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 44.0f, 44.0f)];
            [backButton setImage:[UIImage imageNamed:@"back_button.png"] forState:UIControlStateNormal];
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wundeclared-selector"
            [backButton addTarget:self action:@selector(backDidTouch:) forControlEvents:UIControlEventTouchUpInside];
#pragma clang diagnostic pop
            [headerView addSubview:backButton];
            
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(32, 0, vc.view.frame.size.width - 32*2, 44)];
            titleLabel.text = vc.title;
            titleLabel.font = [UIFont fontWithName:@"Myriad Pro" size:22.0f];
            titleLabel.textColor = [UIColor colorWithRed:250/255.0f green:77/255.0f blue:17/255.0f alpha:1.0f];
            titleLabel.textAlignment = NSTextAlignmentCenter;
            [headerView addSubview:titleLabel];
            
            UIImageView *bgView = [[UIImageView alloc] initWithFrame:vc.view.frame];
            bgView.image = [UIImage imageNamed:@"bg_home.png"];
            [vc.view addSubview:bgView];
            [vc.view sendSubviewToBack:bgView];
            
            vc.tableView.backgroundColor = [UIColor clearColor];
            vc.tableView.frame = CGRectOffset(vc.tableView.frame, 0, 44);
        };
        
        [mapping sectionWithTitle:NSLocalizedString(@"Personal information", @"") identifier:@"logindetail"];
        FKFormAttributeMapping *m = [mapping mapAttribute:@"phone"
                                                    title:NSLocalizedString(@"Phone", @"")
                                          placeholderText:NSLocalizedString(@"also your username", @"")
                                                     type:self.user ? FKFormAttributeMappingTypeLabel :  FKFormAttributeMappingTypeText];
        m.textFieldDelegate = [MOPhoneTextViewDelegate new];
        m.keyboardType = UIKeyboardTypeNumberPad;
        
        [mapping mapAttribute:@"rawpass"
                        title:NSLocalizedString(@"Password", @"")
              placeholderText:NSLocalizedString(@"secret password", @"")
                         type:FKFormAttributeMappingTypePassword];
        
        [mapping mapAttribute:@"firstname"
                        title:NSLocalizedString(@"First Name", @"")
              placeholderText:NSLocalizedString(@"ex: John", @"")
                         type:FKFormAttributeMappingTypeText];
        [mapping mapAttribute:@"lastname"
                        title:NSLocalizedString(@"Last Name", @"")
              placeholderText:NSLocalizedString(@"ex: Doe", @"")
                         type:FKFormAttributeMappingTypeText];
        
        [mapping mapAttribute:@"email"
                        title:NSLocalizedString(@"Email", @"")
              placeholderText:NSLocalizedString(@"for receiving e-receipt", @"")
                         type:FKFormAttributeMappingTypeText];
        
        [mapping mapAttribute:@"address"
                        title:NSLocalizedString(@"Address", @"")
              placeholderText:NSLocalizedString(@"ex: 5th avenue", @"")
                         type:FKFormAttributeMappingTypeText];
        
        [mapping mapAttribute:@"zip"
                        title:NSLocalizedString(@"Zip", @"")
              placeholderText:NSLocalizedString(@"ex: 50000", @"")
                         type:FKFormAttributeMappingTypeText];
        
        [mapping sectionWithTitle:NSLocalizedString(@"Credit card", @"") identifier:@"creditcard"];
        
        NSArray *cardTypes = @[@"Master", @"Visa"];
        [mapping mapAttribute:@"cardType"
                        title:NSLocalizedString(@"Card Type", @"")
                 showInPicker:NO
            selectValuesBlock:^NSArray *(id value, id object, NSInteger *selectedValueIndex) {
                *selectedValueIndex = [cardTypes indexOfObject:((MOAppUser*)object).cardType];
                return cardTypes;
            } valueFromSelectBlock:^id(id value, id object, NSInteger selectedValueIndex) {
                return value;
            } labelValueBlock:^id(id value, id object) {
                return value;
            }];
        
        m = [mapping mapAttribute:@"cardNumber"
                            title:NSLocalizedString(@"Card Number", @"")
                  placeholderText:@"xxxx-xxxx-xxxx-xxxx"
                             type:FKFormAttributeMappingTypeText];
        m.keyboardType = UIKeyboardTypeNumberPad;
        m.textFieldDelegate = [MOCardNumberTextViewDelegate new];
        
        NSMutableArray *months = [NSMutableArray new];
        for (int i=1; i<=12; i++) {
            [months addObject:[NSString stringWithFormat:@"%2d", i]];
        }
        [mapping mapAttribute:@"monthExpired"
                        title:NSLocalizedString(@"Month Expired", @"")
                 showInPicker:NO
            selectValuesBlock:^NSArray *(id value, id object, NSInteger *selectedValueIndex){
                *selectedValueIndex = [months indexOfObject:((MOAppUser*)object).monthExpired];
                return months;
            } valueFromSelectBlock:^id(id value, id object, NSInteger selectedValueIndex) {
                return value;
            } labelValueBlock:^id(id value, id object) {
                return value;
            }];
        
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSInteger year = [calendar component:NSCalendarUnitYear fromDate:[NSDate date]];
        NSMutableArray *years = [NSMutableArray new];
        for (NSInteger i = 0; i <= 20; i++) {
            [years addObject:[NSString stringWithFormat:@"%2ld", (year + i)]];
        }
        [mapping mapAttribute:@"yearExpired"
                        title:NSLocalizedString(@"Year Expired", @"")
                 showInPicker:NO
            selectValuesBlock:^NSArray *(id value, id object, NSInteger *selectedValueIndex){
                *selectedValueIndex = [years indexOfObject:((MOAppUser*)object).yearExpired];
                return years;
            } valueFromSelectBlock:^id(id value, id object, NSInteger selectedValueIndex) {
                return value;
            } labelValueBlock:^id(id value, id object) {
                return value;
            }];
        
        [mapping validationForAttribute:@"phone" validBlock:^BOOL(NSString *value, id object) {
            // Empty is not allow
            return value.length > 0;
        } errorMessageBlock:^NSString *(id value, id object) {
            return NSLocalizedString(@"Phone is required", @"");
        }];
        
        [mapping validationForAttribute:@"firstname" validBlock:^BOOL(NSString *value, id object) {
            // Empty is not allow
            return value.length > 0;
        } errorMessageBlock:^NSString *(id value, id object) {
            return NSLocalizedString(@"First name is required", @"");
        }];
        
        [mapping validationForAttribute:@"email" validBlock:^BOOL(NSString *value, id object) {
            NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
            NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
            return value.length > 0 && [emailTest evaluateWithObject:value];
        } errorMessageBlock:^NSString *(id value, id object) {
            return NSLocalizedString(@"Not valid email", @"");
        }];
        
        if (!self.user) {
            [mapping validationForAttribute:@"rawpass" validBlock:^BOOL(NSString *value, id object) {
                // Empty is not allow
                return value.length > 0;
            } errorMessageBlock:^NSString *(id value, id object) {
                return NSLocalizedString(@"Password is required", @"");
            }];
            
            [mapping validationForAttribute:@"cardType" validBlock:^BOOL(NSString *value, id object) {
                // Empty is not allow
                return value.length > 0;
            } errorMessageBlock:^NSString *(id value, id object) {
                return NSLocalizedString(@"Card Type is required", @"");
            }];
            
            [mapping validationForAttribute:@"cardNumber" validBlock:^BOOL(NSString *value, id object) {
                // Empty is not allow
                return value.length > 0;
            } errorMessageBlock:^NSString *(id value, id object) {
                return NSLocalizedString(@"Card Number is required", @"");
            }];
            
            [mapping validationForAttribute:@"monthExpired" validBlock:^BOOL(NSString *value, id object) {
                // Empty is not allow
                return value.length > 0;
            } errorMessageBlock:^NSString *(id value, id object) {
                return NSLocalizedString(@"Card expired month is required", @"");
            }];
            
            [mapping validationForAttribute:@"yearExpired" validBlock:^BOOL(NSString *value, id object) {
                // Empty is not allow
                return value.length > 0;
            } errorMessageBlock:^NSString *(id value, id object) {
                return NSLocalizedString(@"Card expired year is required", @"");
            }];
        }
        
        [_formModel registerMapping:mapping];
    }];
    
    _userModel = [MOAppUser new];
    if (self.user) {
        _userModel.phone = self.user[@"phone"];
        _userModel.rawpass = @"";
        _userModel.password = self.user[@"password"];
        _userModel.firstname = self.user[@"firstname"];
        _userModel.lastname = self.user[@"lastname"];
        _userModel.email = self.user[@"email"];
        _userModel.address = self.user[@"address"];
        _userModel.zip = self.user[@"zip"];
        _userModel.cardType = self.user[@"cardType"];
        _userModel.cardNumber = self.user[@"cardNumber"];
        _userModel.monthExpired = self.user[@"monthExpired"];
        _userModel.yearExpired = self.user[@"yearExpired"];
        _userModel.customerId = self.user[kXPCustomerID];
    }
    // Load data to form
    [_formModel loadFieldsWithObject:_userModel];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showPickupTime"]) {
        MOPickupTimeViewController *destinationVC = segue.destinationViewController;
        destinationVC.store = self.store;
        destinationVC.order = self.order;
        destinationVC.user = sender;
    }
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if ([identifier isEqualToString:@"showPickupTime"]) {
        return sender;
    }
    
    return [super shouldPerformSegueWithIdentifier:identifier sender:sender];
}


#pragma mark - IBAction

- (IBAction)submitDidTouch:(id)sender {
    // Add new
    if (!self.store || ![self checkNetwork])
        return;
    
    // Begin the validate
    [_formModel save];
    
    // No error found
    if (_formModel.invalidAttributes.count == 0) {
        UIView *view = self.view;
        if (self.user) {
            BOOL fullCCData = _userModel.cardType.length > 0 && _userModel.cardNumber.length > 0 && _userModel.monthExpired.length > 0 && _userModel.yearExpired.length > 0;
            BOOL noCCData = _userModel.cardType.length == 0 && _userModel.cardNumber.length == 0 && _userModel.monthExpired.length == 0 && _userModel.yearExpired.length == 0;
            
            if (fullCCData || noCCData) {
                BOOL updateVault = YES;
                if (noCCData) {
                    updateVault = ![_userModel.phone isEqualToString:self.user[@"phone"]] ||
                    ![_userModel.firstname isEqualToString:self.user[@"firstname"]] ||
                    ![_userModel.lastname isEqualToString:self.user[@"lastname"]] ||
                    ![_userModel.address isEqualToString:self.user[@"address"]] ||
                    ![_userModel.email isEqualToString:self.user[@"email"]] ||
                    ![_userModel.zip isEqualToString:self.user[@"zip"]];
                }
                
                void (^updateAppUser)(NSDictionary*) = ^(NSDictionary *ccinfo) {
                    if (updateVault || _userModel.rawpass.length > 0) {
                        if (_userModel.rawpass.length > 0)
                            _userModel.password = [_userModel.rawpass md5];
                        [[MOManager sharedInstance] putAppUser:_userModel withRev:self.user[@"_rev"] forStore:self.store ccinfo:(ccinfo ? ccinfo : self.user) success:^(NSDictionary *returnedUser){
                            [MBProgressHUD hideAllHUDsForView:view animated:YES];
                            
                            [self performSegueWithIdentifier:@"showPickupTime" sender:returnedUser];
                        } failure:^(NSError *error) {
                            [MBProgressHUD hideAllHUDsForView:view animated:YES];
                            
                            [UIAlertView showWithTitle:NSLocalizedString(@"Failed updating", @"") message:NSLocalizedString(@"Sorry, your information could not be updated. Please contact store for further support.", @"") cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
                        }];
                    } else {
                        [MBProgressHUD hideAllHUDsForView:view animated:YES];
                        
                        [self performSegueWithIdentifier:@"showPickupTime" sender:self.user];
                    }
                };
                
                [MBProgressHUD showHUDAddedTo:view animated:YES];
                
                if (updateVault) {
                    [self.store updateCustomer:_userModel success:^(NSDictionary *results) {
                        updateAppUser(results);
                    } failure:^(NSError *error) {
                        [MBProgressHUD hideAllHUDsForView:view animated:YES];
                        
                        [UIAlertView showWithTitle:NSLocalizedString(@"Failed updating", @"") message:NSLocalizedString(@"Sorry, your information could not be updated. Please contact store for further support.", @"") cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
                    }];
                } else {
                    updateAppUser(nil);
                }
            } else {
                [UIAlertView showWithTitle:NSLocalizedString(@"Invalid credit card", @"") message:NSLocalizedString(@"Please input all credit card information.", @"") cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
            }
            
        } else {
            [MBProgressHUD showHUDAddedTo:view animated:YES];
            
            MOAppUser *_tUser = _userModel;
            MOUserInfoViewController *_self = self;
            [self.store addCustomer:_userModel success:^(NSDictionary *results) {
                NSString *statusid = results[kXPStatusID];
                if ([@"1" isEqualToString:statusid]) {
                    // Update password field
                    _tUser.password = [_tUser.rawpass md5];
                    [[MOManager sharedInstance] putAppUser:_tUser withRev:nil forStore:_self.store ccinfo:results success:^(NSDictionary *returnedUser){
                        [MBProgressHUD hideAllHUDsForView:view animated:YES];
                        
                        [_self performSegueWithIdentifier:@"showPickupTime" sender:returnedUser];
                    } failure:^(NSError *error) {
                        [MBProgressHUD hideAllHUDsForView:view animated:YES];
                        
                        if (error.code == 409) {
                            [UIAlertView showWithTitle:NSLocalizedString(@"Failed registering", @"") message:NSLocalizedString(@"Sorry, the given phone number is already registered. Please choose a different phone number.", @"") cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
                        } else {
                            [UIAlertView showWithTitle:NSLocalizedString(@"Failed registering", @"") message:NSLocalizedString(@"Sorry, your registration could not go through. Please contact store for further support.", @"") cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
                        }
                    }];
                } else if ([@"3" isEqualToString:statusid]) {
                    [MBProgressHUD hideAllHUDsForView:view animated:YES];
                    
                    [UIAlertView showWithTitle:NSLocalizedString(@"Failed registering", @"") message:NSLocalizedString(@"The given phone number is already registered as a user, please choose a different phone number.", @"") cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
                } else {
                    [MBProgressHUD hideAllHUDsForView:view animated:YES];
                    
                    NSString *detail = [NSString stringWithFormat:@"Status: %@\nMessage: %@", results[kXPStatusID], results[kXPMessage]];
                    NSString *message = NSLocalizedString(@"Sorry, your registration could not go through. Please contact store for further support.", @"");
                    message = [NSString stringWithFormat:@"%@\n%@", message, detail];
                    [UIAlertView showWithTitle:NSLocalizedString(@"Failed registering", @"") message:message cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
                }
            } failure:^(NSError *error) {
                [MBProgressHUD hideAllHUDsForView:view animated:YES];
                
                [UIAlertView showWithTitle:NSLocalizedString(@"Failed registering", @"") message:NSLocalizedString(@"Sorry, your registration could not go through. Please contact store for further support.", @"") cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
            }];
        }
    }
}

#pragma mark - Keyboard Listener

- (void)keyboardWillShow:(NSNotification *)notification {
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets;
    if (UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation])) {
        contentInsets = UIEdgeInsetsMake(0.0, 0.0, (keyboardSize.height) + 20, 0.0);
    } else {
        contentInsets = UIEdgeInsetsMake(0.0, 0.0, (keyboardSize.width), 0.0);
    }
    
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
    //    [self.tableView scrollToRowAtIndexPath:self.editingIndexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    NSNumber *rate = notification.userInfo[UIKeyboardAnimationDurationUserInfoKey];
    [UIView animateWithDuration:rate.floatValue animations:^{
        self.tableView.contentInset = UIEdgeInsetsZero;
        self.tableView.scrollIndicatorInsets = UIEdgeInsetsZero;
    }];
}

@end
