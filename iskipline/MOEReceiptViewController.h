//
//  MOEReceiptViewController.h
//  iskipline
//
//  Created by Chinh Nguyen on 12/23/14.
//  Copyright (c) 2014 Chinh Nguyen. All rights reserved.
//

#import "MOBaseViewController.h"

@interface MOEReceiptViewController : MOBaseViewController

@property (nonatomic, strong) MOStore *store;
@property (nonatomic, strong) MOCart* order;

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *phone;

@end

@interface MOEReceiptTableViewController : UITableViewController

@property (nonatomic, strong) MOStore *store;
@property (nonatomic, strong) MOCart* order;

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *phone;

@property (weak, nonatomic) IBOutlet UIButton *textButton;
@property (weak, nonatomic) IBOutlet UIButton *emailButton;
@property (weak, nonatomic) IBOutlet UITextField *emailText;

@property (weak, nonatomic) IBOutlet UIButton *promotionButton;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;

@end
