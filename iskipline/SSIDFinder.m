//
//  SSIDFinder.m
//  iskipline
//
//  Created by TIENPHAM on 8/30/15.
//  Copyright (c) 2015 Chinh Nguyen. All rights reserved.
//

#import "SSIDFinder.h"
#include <ifaddrs.h>
#include <arpa/inet.h>
#import <SystemConfiguration/CaptiveNetwork.h>

@implementation SSIDFinder

#define kMOSSIDOrderPadSyntax @"OP_"

+ (BOOL)checkIfSSIDMatchOrderPad {
    NSString *currentSSID = nil;
    CFArrayRef myArray = CNCopySupportedInterfaces();
    if (myArray != nil){
        NSDictionary *networkDicts = (__bridge NSDictionary *)CNCopyCurrentNetworkInfo(CFArrayGetValueAtIndex(myArray, 0));
        if (networkDicts != nil){
            currentSSID = [networkDicts valueForKey:@"SSID"];
        }
    }
    // For testing function
#ifdef TESTING
    currentSSID = @"OP_001";
#endif
    if (currentSSID == nil)
        return NO;
    BOOL isMatch = [currentSSID hasPrefix:kMOSSIDOrderPadSyntax];
    return isMatch;
}

+ (NSString *)getCurrWifiSSID {
    NSString *currentSSID = nil;
    CFArrayRef myArray = CNCopySupportedInterfaces();
    if (myArray != nil){
        NSDictionary *networkDicts = (__bridge NSDictionary *)CNCopyCurrentNetworkInfo(CFArrayGetValueAtIndex(myArray, 0));
        if (networkDicts != nil){
            currentSSID = [networkDicts valueForKey:@"SSID"];
        }
    }
    return currentSSID;
}

+ (NSString *)getIPAddress {
    NSString *address = nil;
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            if(temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                }
            }
            temp_addr = temp_addr->ifa_next;
        }
    }
    // Free memory
    freeifaddrs(interfaces);
    return address;
}


@end
