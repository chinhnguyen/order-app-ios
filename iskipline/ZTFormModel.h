//
//  ZTFormModel.h
//  iskipline
//
//  Created by Chinh Nguyen on 1/13/15.
//  Copyright (c) 2015 Chinh Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FKFormModel.h"

@interface ZTFormModel : FKFormModel

/**
 Cell font
 */
@property (nonatomic, strong) UIFont *cellFont;

/**
 Cell text color
 */
@property (nonatomic, strong) UIColor *cellTextColor;

/**
 Cell background color
 */
@property (nonatomic, strong) UIColor *cellBackgroundColor;

/**
 Cell background color
 */
@property (nonatomic, strong) UIColor *cellSelectedBackgroundColor;

/**
 Cell accessory image
 */
@property (nonatomic, strong) UIImage *cellAccessoryDisclosureIndicatorImage;

/**
 Cell accessory image
 */
@property (nonatomic, strong) UIImage *cellAccessoryCheckedImage;

/**
 Section title color and font
 */
@property (nonatomic, strong) UIFont *sectionTitleFont;
@property (nonatomic, strong) UIColor *sectionTitleColor;


@end
