//
//  MOAdminViewController.h
//  iskipline
//
//  Created by Chinh Nguyen on 1/9/15.
//  Copyright (c) 2015 Chinh Nguyen. All rights reserved.
//

#import "MOBaseViewController.h"

@protocol MOAdminMenuDelegate <NSObject>

- (void)didSelectMenuItem:(long)index;

@end

@interface MOAdminViewController : UIViewController<UITableViewDataSource, UISearchBarDelegate, MOAdminMenuDelegate, UIActionSheetDelegate>

@property (strong, nonatomic) id user;
@property (strong, nonatomic) id store;
@property (strong, nonatomic) NSString *dbhost;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *menuRightBorderContraint;
@property (weak, nonatomic) IBOutlet UIView *menuView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UIProgressView *progressView;
@property (weak, nonatomic) IBOutlet UIButton *addButton;
@property (weak, nonatomic) IBOutlet UIView *coverView;

@end



@interface MOAdminMenuTableViewController : UITableViewController

@property (strong, nonatomic) id<MOAdminMenuDelegate> adminMenuDelegate;

@end





@interface MOAdminItemTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;

@end