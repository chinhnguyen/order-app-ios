//
//  UIViewController+Utils.m
//  iskipline
//
//  Created by Chinh Nguyen on 1/7/15.
//  Copyright (c) 2015 Chinh Nguyen. All rights reserved.
//

#import "UIViewController+Utils.h"
#import <Reachability.h>
#import <UIAlertView+Blocks.h>

@implementation UIViewController (Utils)

- (BOOL)checkNetwork {
    if (![self networkAvailable]) {
        [self showNetworkNotAvailableMessage];
        return NO;
    }
    
    return YES;
}

- (BOOL)networkAvailable {
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

- (void)showNetworkNotAvailableMessage {
    [UIAlertView showWithTitle:NSLocalizedString(@"No network", @"")
                       message:NSLocalizedString(@"Please make sure your device's network is working and try again.", @"")
             cancelButtonTitle:NSLocalizedString(@"OK", @"")
             otherButtonTitles:nil
                      tapBlock:nil];
}

- (void)formatNavigationBar:(SEL)backAction {
    // Back button
    UIImage *backButtonImage = [UIImage imageNamed:@"navbar.backbutton.png"];
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, backButtonImage.size.width, backButtonImage.size.height)];
    [backButton setImage:backButtonImage forState:UIControlStateNormal];
    [backButton addTarget:self action:backAction forControlEvents:UIControlEventTouchUpInside];
    // Back bar button
    UIBarButtonItem *backBarButton = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    [backBarButton setBackgroundImage:[[UIImage alloc] init] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [backBarButton setTitle:@""];
    [self.navigationItem setLeftBarButtonItem:backBarButton];
    
    // Navigation background color
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:51.0f/255.0f green:183.0f/255.0f blue:232.0f/255.0f alpha:1.0f];
    self.navigationController.navigationBar.translucent = NO;
    
    // Title
    self.navigationController.navigationBar.titleTextAttributes =
    [NSDictionary dictionaryWithObjectsAndKeys:
     [UIColor whiteColor], NSForegroundColorAttributeName,
     [UIFont fontWithName:@"Helvetica-Bold" size:20.0], NSFontAttributeName, nil];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
}

- (IBAction)backDidTouch:(id)sender {
    // Back one screen
    [self.navigationController popViewControllerAnimated:YES];
}

@end
