//
//  MOItemTableViewCell.h
//  iskipline
//
//  Created by Chinh Nguyen on 12/18/14.
//  Copyright (c) 2014 Chinh Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MOItemTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIButton *addButton;
@property (weak, nonatomic) IBOutlet UIButton *minusButton;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *numberLabel;
@property (weak, nonatomic) IBOutlet UILabel *customizedLabel;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;

@end
