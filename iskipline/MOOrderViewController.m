//
//  MOOrderViewController.m
//  iskipline
//
//  Created by Chinh Nguyen on 12/17/14.
//  Copyright (c) 2014 Chinh Nguyen. All rights reserved.
//

#import "MOOrderViewController.h"
#import "MOCheckoutViewController.h"
#import "UIImageView+AFNetworking.h"

@interface MOOrderViewController ()

@end

@implementation MOOrderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Listen to store data pulling changes
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(storeDataReplicationChanged:) name:kMOStoreDataReplicationChangedNofification object:nil];

    
    // Setup store logo and name
    if (self.store) {
        if (self.store.image && self.store.image.length > 0) {
            [self.storeLogoImageView setImageWithURL:[NSURL URLWithString:self.store.image]];
            self.storeNameLabel.hidden = YES;
        } else {
            self.storeLogoImageView.hidden = YES;
            self.storeNameLabel.text = self.store.store;
        }
    }
    
    // Setup cart view 
    self.cartView.layer.cornerRadius = 5;
    self.cartView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.cartView.layer.borderWidth = 1;

//    self.cartView add

    // Setup table view
    self.tableView.tableFooterView = [UIView new];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kMOStoreDataReplicationChangedNofification object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (self.order) {
        // Listen to cart change
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orderUpdated:) name:kMOCartUpdatedNotification object:nil];
        self.cartDetailLabel.text = [self.order orderSummary];
    }
    [self loadDataAndUpdateLayout];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showCart"]) {
        MOCheckoutViewController *destinationVC = segue.destinationViewController;
        destinationVC.store = self.store;
        destinationVC.order = self.order;
        destinationVC.tableNumber = self.tableNumber;
    }
}


- (void)loadDataAndUpdateLayout {
    // Child VC will provide its own data loading and layout updating
}

#pragma mark - Replication

- (void)storeDataReplicationChanged:(NSNotification*)n {
    CBLReplication *r = n.object;
    
    if (r.status == kCBLReplicationOffline || r.status == kCBLReplicationIdle) {
        // Hide the progress view first
        self.storeDataPullProgressView.hidden = YES;
        // Update other layouts
        [self loadDataAndUpdateLayout];
    } else if (r.status == kCBLReplicationActive) {
        if (r.changesCount >  0) {
            float progress = (float)r.completedChangesCount / r.changesCount;
            // Show the progress view first
            self.storeDataPullProgressView.hidden = NO;
            // Update its progress
            self.storeDataPullProgressView.progress = progress;
        }
    }
}

- (void)orderUpdated:(NSNotification*)n {
    if (self.order)
        self.cartDetailLabel.text = [self.order orderSummary];
    [self.tableView reloadData];
}

#pragma mark - IBAction

- (IBAction)cartDidTouch:(id)sender {
    self.cartView.backgroundColor = [UIColor colorWithWhite:0.5 alpha:0.5];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.cartView.backgroundColor = [UIColor clearColor];
    });
    if (self.order) {
        if ([self.order isEmpty]) {
            [self.view makeToast:NSLocalizedString(@"Cart is empty", @"")];
        } else {
            [self performSegueWithIdentifier:@"showCart" sender:sender];
        }
    }
}

@end
