//
//  ZTTrumpia.m
//  iskipline
//
//  Created by Chinh Nguyen on 12/21/14.
//  Copyright (c) 2014 Chinh Nguyen. All rights reserved.
//

#import "ZTTrumpia.h"

@implementation ZTTrumpia

+ (void)putSubscription:(NSDictionary*)user
                success:(void (^)(NSString *subscriptionId))success
                failure:(void (^)(NSError *error))failure {
    // Subscription's detail
    NSMutableDictionary *subscription =
    [NSMutableDictionary dictionaryWithObjectsAndKeys:
     user[@"firstname"], @"first_name",
     user[@"lastname"], @"last_name",
     user[@"email"], @"email",
     nil];
    // If phone is set, use it
    NSString *phone = user[@"phone"];
    if (phone && phone.length > 0) {
        NSDictionary *mobile =
        [NSDictionary dictionaryWithObjectsAndKeys:
         [phone stringByReplacingOccurrencesOfString:@"-" withString:@""], @"number", nil];
        [subscription setValue:mobile forKey:@"mobile"];
    }
    // List of subscriptions
    NSArray *subscriptions = [NSArray arrayWithObject:subscription];
    
    // The parameters
    NSDictionary *parameters =
    [NSDictionary dictionaryWithObjectsAndKeys:
     kZTTrumpiaModibleOrderListName, @"list_name",
     subscriptions, @"subscriptions",
     nil];
    // Create the request
    NSError *error;
    NSMutableURLRequest *request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"PUT" URLString:kZTTrumpiaSubscriptionUrl parameters:parameters error:&error];
    if (request == nil) {
        failure(error);
        return;
    }
    // Set the required HTTP headers
    [request setValue:kZTTrumpiaApiKey forHTTPHeaderField:@"X-Apikey"];
    // Create operation
    AFHTTPRequestOperation *op = [[AFHTTPRequestOperationManager manager] HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *requestid = responseObject[@"request_id"];
        if (requestid == nil || requestid.length == 0) {
            NSMutableDictionary *details = [NSMutableDictionary dictionary];
            [details setValue:@"Server returned empty request id" forKey:NSLocalizedDescriptionKey];
            failure([NSError errorWithDomain:@"Trumpia" code:kZTTrumpiaErrorEmptyRequestId userInfo:details]);
            return;
        }
        
        // Have the request id, now get the report for it
        [ZTTrumpia getReport:requestid success:^(id responseObject) {
            NSArray *responses = responseObject;
            if (responses.count == 0) {
                NSMutableDictionary *details = [NSMutableDictionary dictionary];
                [details setValue:@"Server returned empty response for getting request report" forKey:NSLocalizedDescriptionKey];
                failure([NSError errorWithDomain:@"Trumpia" code:kZTTrumpiaErrorEmptyRequestId userInfo:details]);
                return;
            }
            NSDictionary *response = [((NSArray*)responseObject) objectAtIndex:0];
            
            NSString *statusCode = response[@"status_code"];
            if (statusCode && statusCode.length > 0) {
                if ([@"MPSE2401" isEqualToString:statusCode]) {
                    NSMutableDictionary *details = [NSMutableDictionary dictionary];
                    [details setValue:@"Subscription already exits." forKey:NSLocalizedDescriptionKey];
                    failure([NSError errorWithDomain:@"Trumpia" code:kZTTrumpiaErrorSubscriptionAlreadyExists userInfo:details]);
                } else {
                    NSMutableDictionary *details = [NSMutableDictionary dictionary];
                    [details setValue:[NSString stringWithFormat:@"Error: %@", response[@"error_message"]] forKey:NSLocalizedDescriptionKey];
                    failure([NSError errorWithDomain:@"Trumpia" code:kZTTrumpiaErrorCreatingSubscription userInfo:details]);
                }
                return;
            }
            
            NSString *subscriptionId = [response[@"subscription_id"] stringValue];
            if (subscriptionId && subscriptionId.length > 0) {
                success(subscriptionId);
            } else {
                NSMutableDictionary *details = [NSMutableDictionary dictionary];
                [details setValue:@"Server returned empty subscription id." forKey:NSLocalizedDescriptionKey];
                failure([NSError errorWithDomain:@"Trumpia" code:kZTTrumpiaErrorEmptySubscriptionId userInfo:details]);
            }
        } failure:failure];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
    // Schedule for execution
    [[NSOperationQueue mainQueue] addOperation:op];
}

+ (void)postSubscription:(NSDictionary*)user
                   forId:(NSString*)subscriptionId
                 success:(void (^)())success
                 failure:(void (^)(NSError *error))failure {
    // Subscription's detail
    NSMutableDictionary *subscription =
    [NSMutableDictionary dictionaryWithObjectsAndKeys:
     user[@"firstname"], @"first_name",
     user[@"lastname"], @"last_name",
     user[@"email"], @"email",
     nil];
    // If phone is set, use it
    NSString *phone = user[@"phone"];
    if (phone && phone.length > 0) {
        NSDictionary *mobile =
        [NSDictionary dictionaryWithObjectsAndKeys:
         [phone stringByReplacingOccurrencesOfString:@"-" withString:@""], @"number", nil];
        [subscription setValue:mobile forKey:@"mobile"];
    }
    // List of subscriptions
    NSArray *subscriptions = [NSArray arrayWithObject:subscription];
    
    // The parameters
    NSDictionary *parameters =
    [NSDictionary dictionaryWithObjectsAndKeys:
     kZTTrumpiaModibleOrderListName, @"list_name",
     subscriptions, @"subscriptions",
     nil];
    // Create URL
    NSString *url = [kZTTrumpiaSubscriptionUrl stringByAppendingPathComponent:subscriptionId];
    // Create the request
    NSError *error;
    NSMutableURLRequest *request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:url parameters:parameters error:&error];
    if (request == nil) {
        failure(error);
        return;
    }
    // Set the required HTTP headers
    [request setValue:kZTTrumpiaApiKey forHTTPHeaderField:@"X-Apikey"];
    // Create operation
    AFHTTPRequestOperation *op = [[AFHTTPRequestOperationManager manager] HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *requestid = responseObject[@"request_id"];
        if (requestid == nil || requestid.length == 0) {
            NSMutableDictionary *details = [NSMutableDictionary dictionary];
            [details setValue:@"Server returned empty request id" forKey:NSLocalizedDescriptionKey];
            failure([NSError errorWithDomain:@"Trumpia" code:kZTTrumpiaErrorEmptyRequestId userInfo:details]);
            return;
        }
        // Get the report
        [ZTTrumpia getReport:requestid success:^(id responseObject) {
            NSString *statusCode = responseObject[@"status_code"];
            if (statusCode && statusCode.length > 0) {
                NSMutableDictionary *details = [NSMutableDictionary dictionary];
                [details setValue:[NSString stringWithFormat:@"Error: %@", responseObject[@"error_message"]] forKey:NSLocalizedDescriptionKey];
                failure([NSError errorWithDomain:@"Trumpia" code:kZTTrumpiaErrorUpdatingSubscription userInfo:details]);
                return;
            }
            success();
        } failure:failure];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
    // Schedule for execution
    [[NSOperationQueue mainQueue] addOperation:op];
}


+ (void)getReport:(NSString*)requestId success:(void (^)(id responseObject))success
          failure:(void (^)(NSError *error))failure {
    NSString *url = [kZTTrumpiaReportUrl stringByAppendingPathComponent:requestId];
    // Create the request
    NSError *error;
    NSMutableURLRequest *request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"GET" URLString:url parameters:nil error:&error];
    if (request == nil) {
        failure(error);
        return;
    }
    // Set the required HTTP headers
    [request setValue:kZTTrumpiaApiKey forHTTPHeaderField:@"X-Apikey"];
    // Create operation
    AFHTTPRequestOperation *op = [[AFHTTPRequestOperationManager manager] HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
    // Schedule for execution
    [[NSOperationQueue mainQueue] addOperation:op];
}

+ (void)searchSubscriptionWithType:(NSString*)type
                           andData:(NSString*)data
                           success:(void (^)(NSString *subscriptionId))success
                           failure:(void (^)(NSError *error))failure {
    NSString *url = [kZTTrumpiaSubscriptionUrl stringByAppendingPathComponent:@"search"];
    NSDictionary *parameters =
    [NSDictionary dictionaryWithObjectsAndKeys:
     type, @"search_type",
     data, @"search_data",
     nil];
    // Create the request
    NSError *error;
    NSMutableURLRequest *request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"GET" URLString:url parameters:parameters error:&error];
    if (request == nil) {
        failure(error);
        return;
    }
    // Set the required HTTP headers
    [request setValue:kZTTrumpiaApiKey forHTTPHeaderField:@"X-Apikey"];
    // Create operation
    AFHTTPRequestOperation *op = [[AFHTTPRequestOperationManager manager] HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *statusCode = responseObject[@"status_code"];
        if (statusCode && statusCode.length > 0) {
            //            NSMutableDictionary *details = [NSMutableDictionary dictionary];
            //            [details setValue:[NSString stringWithFormat:@"Error: %@", responseObject[@"error_message"]] forKey:NSLocalizedDescriptionKey];
            //            failure([NSError errorWithDomain:@"Trumpia" code:kZTTrumpiaErrorSearchNoResult userInfo:details]);
            success(nil);
            return;
        }
        
        NSArray *idlist = responseObject[@"subscription_id_list"];
        if (idlist && idlist.count > 0) {
            success([idlist objectAtIndex:0]);
        } else {
            //            NSMutableDictionary *details = [NSMutableDictionary dictionary];
            //            [details setValue:@"No matching result." forKey:NSLocalizedDescriptionKey];
            //            failure([NSError errorWithDomain:@"Trumpia" code:kZTTrumpiaErrorSearchNoResult userInfo:details]);
            success(nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
    // Schedule for execution
    [[NSOperationQueue mainQueue] addOperation:op];
}


+ (void)sendSMSTo:(NSString*)subscriptionId
  withDescription:(NSString*)description
 organizationName:(NSString*)orgName
             text:(NSString*)text
          success:(void (^)())success
          failure:(void (^)(NSError *error))failure {
    NSDictionary *sms =
    [NSDictionary dictionaryWithObjectsAndKeys:
     text, @"message",
     orgName, @"organization_name",
     nil];
    NSDictionary *recipient =
    [NSDictionary dictionaryWithObjectsAndKeys:
     @"subscription", @"type",
     subscriptionId, @"value",
     nil];
    
    NSDictionary *parameters =
    [NSDictionary dictionaryWithObjectsAndKeys:
     description, @"description",
     sms, @"sms",
     recipient, @"recipients",
     nil];
    
    // Create the request
    NSError *error;
    NSMutableURLRequest *request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"PUT" URLString:kZTTrumpiaMessageUrl parameters:parameters error:&error];
    if (request == nil) {
        failure(error);
        return;
    }
    // Set the required HTTP headers
    [request setValue:kZTTrumpiaApiKey forHTTPHeaderField:@"X-Apikey"];
    // Create operation
    AFHTTPRequestOperation *op = [[AFHTTPRequestOperationManager manager] HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *requestid = responseObject[@"request_id"];
        if (requestid == nil || requestid.length == 0) {
            NSMutableDictionary *details = [NSMutableDictionary dictionary];
            [details setValue:@"Server returned empty request id" forKey:NSLocalizedDescriptionKey];
            failure([NSError errorWithDomain:@"Trumpia" code:kZTTrumpiaErrorEmptyRequestId userInfo:details]);
            return;
        }
        // Get the report
        [ZTTrumpia getReport:requestid success:^(id responseObject) {
            NSString *statusCode = responseObject[@"status_code"];
            if (statusCode && statusCode.length > 0) {
                NSMutableDictionary *details = [NSMutableDictionary dictionary];
                [details setValue:@"Could not send message" forKey:NSLocalizedDescriptionKey];
                failure([NSError errorWithDomain:@"Trumpia" code:kZTTrumpiaErrorSendingMessage userInfo:details]);
                return;
            }
            success();
        } failure:failure];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
    // Schedule for execution
    [[NSOperationQueue mainQueue] addOperation:op];
}

@end
