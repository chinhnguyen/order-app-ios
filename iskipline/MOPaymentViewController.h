//
//  MOPaymentViewController.h
//  iskipline
//
//  Created by Chinh Nguyen on 12/20/14.
//  Copyright (c) 2014 Chinh Nguyen. All rights reserved.
//

#import "MOBaseViewController.h"

@interface MOPaymentViewController : MOBaseViewController

@property (nonatomic, strong) MOStore *store;
@property (nonatomic, strong) MOCart* order;

- (IBAction)backFromPickupTime:(UIStoryboardSegue *)unwindSegue;

@end

@interface MOPaymentTableViewController : UITableViewController<UITextFieldDelegate>

@property (nonatomic, strong) MOStore *store;
@property (nonatomic, strong) MOCart *order;
@property (weak, nonatomic) IBOutlet UITextField *pickupNameText;
@property (weak, nonatomic) IBOutlet UITextField *pickupPhoneText;
@property (weak, nonatomic) IBOutlet UIButton *sendCodeButton;
@property (weak, nonatomic) IBOutlet UITextField *loginPhoneText;
@property (weak, nonatomic) IBOutlet UITextField *loginPassText;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIButton *registerButton;
@property (weak, nonatomic) IBOutlet UITableViewCell *pickupTableViewCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *loginTableViewCell;


@end

