//
//  SOCategory.h
//  SelfOrder
//
//  Created by PC02 on 12/31/13.
//  Copyright (c) 2013 nexmac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MOCategory : NSObject

@property (nonatomic, retain) NSString* ID;
@property (nonatomic, retain) NSString* parentID;
@property (nonatomic, retain) NSString* name1;
@property (nonatomic, retain) NSString* name2;
@property (nonatomic, retain) NSString* name3;
@property (nonatomic, retain) NSString* desc;
@property (nonatomic, retain) NSString* fontColor;
@property (nonatomic, retain) NSString* bgColor;
@property (nonatomic, retain) NSNumber* maxBlock;
@property (nonatomic, retain) NSNumber* isShow;     // it's self_order field;
@property (nonatomic, retain) NSString* printerID;


@end
