//
//  MOCategoryViewController.m
//  iskipline
//
//  Created by Chinh Nguyen on 12/17/14.
//  Copyright (c) 2014 Chinh Nguyen. All rights reserved.
//

#import "MOCategoryViewController.h"
#import "MOCategoryTableViewCell.h"
#import "MOItemViewController.h"
#import "UIImageView+AFNetworking.h"

@interface MOCategoryViewController () {
    NSArray *_categories;
    CBLReplication *_storeDbPull;
}

@end

@implementation MOCategoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    BOOL isOrderPadMode = [userDefaults boolForKey:kOMIsOrderPadMode];
    self.labelCheckOut.text = isOrderPadMode ? [NSString stringWithFormat:@"#%@ >>", self.tableNumber] : @"CHECK OUT >>";
    
    _categories = [NSArray new];
    
    if (self.store) {
        [self saveToVisitedList];
        
        // Create new order
        self.order = [[MOCart alloc] initWithStore:self.store];

        NSError *err;
        [self.store openStoreDatabase:&err];
        if (err) {
            [UIAlertView showWithTitle:NSLocalizedString(@"Critical Error", @"") message:NSLocalizedString(@"Could not open store database.", @"") cancelButtonTitle:NSLocalizedString(@"Close", @"") otherButtonTitles:nil tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                [self dismissViewControllerAnimated:YES completion:nil];
            }];
            return;
        }
        
        // Start pull
        _storeDbPull = [self.store.storeDatabase createPullReplication:dbSyncURL(self.store.account)];
        _storeDbPull.continuous = YES;
        [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(replicationChanged:) name: kCBLReplicationChangeNotification object: _storeDbPull];
        [_storeDbPull start];
    }
}

- (void)dealloc {
    if (_storeDbPull) {
        [_storeDbPull stop];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    [super prepareForSegue:segue sender:sender];
    
    if ([segue.identifier isEqualToString:@"showItems"]) {
        MOItemViewController *vc = segue.destinationViewController;
        vc.store = self.store;
        vc.order = self.order;
        vc.tableNumber = self.tableNumber;
        NSIndexPath *p = [self.tableView indexPathForSelectedRow];
        vc.category = [_categories objectAtIndex:p.row];
    }
}

- (IBAction)orderCancelled:(UIStoryboardSegue *)unwindSegue {
    // Empty cart
    if (self.order) {
        [self.order emptyCart];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _categories.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MOCategoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CategoryCell"];
    
    NSDictionary *c = [_categories objectAtIndex:indexPath.row];
    cell.nameLabel.text = displayName(c);
    
    NSString *image = c[@"image"];
    if (image && image.length > 0) {
        [cell.bgImageView setImageWithURL:[NSURL URLWithString:image]];
    }
    
    if (cell.selectedBackgroundView.backgroundColor == nil) {
        UIView *bgColorView = [UIView new];
        bgColorView.layer.masksToBounds = YES;
        [bgColorView setBackgroundColor:[UIColor colorWithWhite:0.5 alpha:0.5]];
        [cell setSelectedBackgroundView:bgColorView];
    }
    
    return cell;
}

#pragma mark - Replication

- (void)replicationChanged:(NSNotification*)n {
    [[NSNotificationCenter defaultCenter] postNotificationName:kMOStoreDataReplicationChangedNofification object:n.object];
}

#pragma mark - Data

- (void)loadDataAndUpdateLayout {
    if (!self.store || !self.store.storeDatabase)
        return;
    
    // Get categories
    CBLDocument *doc = [self.store.storeDatabase existingDocumentWithID:kMODocCategories];
    if (!doc)
        return;
    
    // List of all categories
    NSArray *allCategories = doc[kMODocCategories];
    
    // Filtered list of categories
    NSMutableArray *filteredCategories = [NSMutableArray new];
    
    // Do not show item with self_order=false
    for (NSDictionary *category in allCategories) {
        NSString * isShow = category[@"self_order"];
        if (isShow == nil || [isShow boolValue] == true) {
            [filteredCategories addObject:category];
        }
    }
    
    // Sort by name
    _categories = [filteredCategories sortedArrayUsingComparator:kMOItemNameComparator];
    // Reload table view
    [self.tableView reloadData];
    
}

- (void)saveToVisitedList {
    if (self.store == nil)
        return;
    // Get mutable copy of the list
    NSMutableArray *visitedStores = [[[NSUserDefaults standardUserDefaults] arrayForKey:kMOVisitedStoresList] mutableCopy];
    // Remove any old one
    for (int i = 0; i < visitedStores.count; i++) {
        NSDictionary *s = [visitedStores objectAtIndex:i];
        if ([s[@"db"] isEqualToString:self.store.account]) {
            [visitedStores removeObject:s];
            break;
        }
    }    
    // Create the stored info
    NSDictionary *store =
    [NSDictionary dictionaryWithObjectsAndKeys:
     self.store.store                           , @"name",
     self.store.account                         , @"db",
     self.store.street                          , @"address",
     self.store.city                            , @"city",
     self.store.state                           , @"state",
     self.store.open                            , @"open",
     self.store.close                           , @"close",
     nil];
    // Put to list of visited store
    [visitedStores insertObject:store atIndex:0];
    // Save to user preferences
    [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithArray:visitedStores] forKey:kMOVisitedStoresList];
}

@end
