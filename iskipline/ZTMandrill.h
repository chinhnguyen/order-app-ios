//
//  ZTMandrill.h
//  iskipline
//
//  Created by Chinh Nguyen on 12/21/14.
//  Copyright (c) 2014 Chinh Nguyen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking.h>

#define kZTMandrillKey @"roOsUJ9TdJEoUQV4FHDuNg"
#define kZTMandrillBaseUrl @"https://mandrillapp.com/api/1.0"
#define kZTMandrillSendMessageUrl kZTMandrillBaseUrl @"/messages/send.json"

@interface ZTMandrill : NSObject

+ (void)sendMail:(NSDictionary*)message
         success:(void (^)())success
         failure:(void (^)(NSError *error))failure;

@end

