//
//  MOOrder.h
//  iskipline
//
//  Created by Chinh Nguyen on 12/24/14.
//  Copyright (c) 2014 Chinh Nguyen. All rights reserved.
//

#import <CouchbaseLite/CouchbaseLite.h>

@interface MOOrder : CBLModel

// PaymentXP
@property (nonatomic, strong) NSString *StatusID;
@property (nonatomic, assign) double TransactionAmount;
@property (nonatomic, strong) NSString *TransactionID;

// Customer info
@property (nonatomic, strong) NSString *customerName;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) NSString *email;

@property (nonatomic, strong) NSString *cashier;

// Pickup time
@property (nonatomic, strong) NSString *kindRemind;
@property (nonatomic, strong) NSString *datePickup;
@property (nonatomic, strong) NSString *timePickup;
@property (nonatomic, strong) NSString *dateRemind;
@property (nonatomic, strong) NSString *timeRemind;
@property (nonatomic, assign) BOOL withAlert;

// Ereceipt type
@property (nonatomic, strong) NSString *ereceipt;
@property (nonatomic, strong) NSString *promo_subscription;

@property (nonatomic, strong) NSString *dayCreate;
@property (nonatomic, assign) double depositAmount;
@property (nonatomic, strong) NSString *express_code;
//@property (nonatomic, strong) NSString *type;
@property (nonatomic, assign) NSNumber *station;

// Order detail
@property (nonatomic, strong) NSDictionary *order;

@end
