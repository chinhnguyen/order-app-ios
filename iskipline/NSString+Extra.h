//
//  NSString+Extra.h
//  iskipline
//
//  Created by Chinh Nguyen on 1/2/15.
//  Copyright (c) 2015 Chinh Nguyen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString(Extra)

- (NSString*)padLeft:(NSString*)stringToPad forLength:(NSUInteger)length;
- (NSString*)padRight:(NSString*)stringToPad forLength:(NSUInteger)length;

- (NSString*)md5;

@end
