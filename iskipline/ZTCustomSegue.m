//
//  ZTCustomSegue.m
//  iskipline
//
//  Created by Chinh Nguyen on 2/15/15.
//  Copyright (c) 2015 Chinh Nguyen. All rights reserved.
//

#import "ZTCustomSegue.h"

@implementation ZTCustomSegue

- (void)perform {
    UIViewController *sourceViewController = (UIViewController*)[self sourceViewController];
    UIViewController *destinationController = (UIViewController*)[self destinationViewController];
    
    CATransition* transition = [CATransition animation];
    transition.duration = .3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionMoveIn; //, kCATransitionPush, kCATransitionReveal, kCATransitionFade
    transition.subtype = kCATransitionFromRight; //kCATransitionFromLeft, kCATransitionFromRight, kCATransitionFromTop, kCATransitionFromBottom
    
    [sourceViewController.navigationController.view.layer addAnimation:transition                                                                forKey:kCATransition];
    
    [sourceViewController.navigationController pushViewController:destinationController animated:YES];
}

@end

@implementation ZTNoAnimationPushSegue

- (void)perform {
    UIViewController *sourceViewController = (UIViewController*)[self sourceViewController];
    UIViewController *destinationController = (UIViewController*)[self destinationViewController];
    [sourceViewController.navigationController pushViewController:destinationController animated:YES];
}

@end

