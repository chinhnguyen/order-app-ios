//
//  MOHomeViewController.h
//  iskipline
//
//  Created by Chinh Nguyen on 12/15/14.
//  Copyright (c) 2014 Chinh Nguyen. All rights reserved.
//

#import "MOBaseViewController.h"

@interface MOHomeViewController : MOBaseViewController<UIPickerViewDataSource, UIPickerViewDelegate, UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UIPickerView *stateCitiesPickerView;
@property (weak, nonatomic) IBOutlet UIView *pickerView;

// For displaying an overlay on top of other controls when picker view is shown
@property (weak, nonatomic) IBOutlet UIView *coverView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pickerViewBottomConstraint;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *showStoresButton;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *emptyLabel;

@end
