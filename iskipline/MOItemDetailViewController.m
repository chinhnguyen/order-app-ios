//
//  MOItemDetailViewController.m
//  iskipline
//
//  Created by Chinh Nguyen on 1/12/15.
//  Copyright (c) 2015 Chinh Nguyen. All rights reserved.
//

#import "MOItemDetailViewController.h"
#import "UIViewController+Utils.h"
#import <FormKit.m/FKFormMapper.h>

@interface MOItemDetailViewController () {
    FKFormModel *_formModel;
    MOBaseFormModel *_itemModel;
    
    NSArray *_roles;
    
    NSArray *_printerIds;
    NSArray *_printerNames;
    NSDictionary *_printers;
    
    UINavigationController *_navigationController;
    
    NSString *_documentid;
    
    BWSelectViewControllerWillAppearBlock _selectViewWillAppear;
}

@end

@implementation MOItemDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];    
    
    [[UITableView appearanceWhenContainedIn:[MOItemDetailViewController class], nil] setSeparatorInset:UIEdgeInsetsMake(0, 16, 0, 0)];
    [[UITableViewCell appearanceWhenContainedIn:[MOItemDetailViewController class], nil] setSeparatorInset:UIEdgeInsetsMake(0, 16, 0, 0)];
    
    // Remove the top white space
    self.tableView.contentInset = UIEdgeInsetsMake(10, 0, 0, 0);
    self.tableView.rowHeight = 44;
    
    [self formatNavigationBar:@selector(backDidTouch:)];
    
    if (self.itemType == MOUser) {
        self.navigationItem.title = NSLocalizedString(@"User", @"");
        _documentid = @"users";
    } else if (self.itemType == MOCategory) {
        self.navigationItem.title = NSLocalizedString(@"Category", @"");
        _documentid = @"categories";
    } else if (self.itemType == MOItem) {
        self.navigationItem.title = NSLocalizedString(@"Item", @"");
        _documentid = @"items";
    } else if (self.itemType == MOModifier) {
        self.navigationItem.title = NSLocalizedString(@"Modifier", @"");
        _documentid = @"modifiers";
    }
    
    UIBarButtonItem *saveBarButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Save", @"") style:UIBarButtonItemStylePlain target:self action:@selector(saveDidTouch:)];
    saveBarButton.tintColor = [UIColor whiteColor];
    [self.navigationItem setRightBarButtonItem:saveBarButton];
    
    _roles = [NSArray arrayWithObjects:@"ADMIN", @"OWNER", @"MANAGER", @"SUPERVISOR", @"CASHIER", @"SERVER", @"WAITER", nil];
    
    _printerIds = [NSArray arrayWithObjects:@"KP0", @"KP1",                   @"KP2", @"KP3", @"RP1", @"RP2", @"RP3", nil];
    _printerNames = [NSArray arrayWithObjects:@"No Printer",                     @"Kitchen 1", @"Kitchen 2", @"Kitchen 3", @"Receipt 1", @"Receipt 2", @"Receipt 3", nil];
    _printers = [NSDictionary dictionaryWithObjects:_printerNames forKeys:_printerIds];
    
    _formModel = [FKFormModel formTableModelForTableView:self.tableView navigationController:self.navigationController];
    _formModel.labelTextColor = [UIColor whiteColor];
    _formModel.valueTextColor = [UIColor colorWithWhite:0.9f alpha:0.9f];
    _formModel.validationNormalCellBackgroundColor = [UIColor colorWithWhite:0.9f alpha:0.1f];
    _formModel.validationErrorCellBackgroundColor = [UIColor colorWithWhite:0.9f alpha:0.1f];
    
    // Customer cell configuration
    [_formModel configureCells:^(UITableViewCell *cell) {
        if ([cell isKindOfClass:[FKIntegerField class]] || [cell isKindOfClass:[FKFloatField class]]) {
            FKTextField *f = (FKTextField*)cell;
            f.textField.extraDelegate = self;
        }
    }];
    
    
    _selectViewWillAppear = ^(BWSelectViewController *vc) {
        [vc formatNavigationBar:@selector(backDidTouch:)];
        
        vc.view.backgroundColor = [UIColor colorWithRed:12/255.0f green:93/255.0f blue:124/255.0f alpha:1.0f];
        vc.tableView.backgroundColor = [UIColor clearColor];
    };

    if (self.itemType == MOUser) {
        [self loadUserForm];
    } else if (self.itemType == MOCategory) {
        [self loadCategoryForm];
    } else if (self.itemType == MOItem) {
        [self loadItemForm];
    } else if (self.itemType == MOModifier) {
        [self loadModifierForm];
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


#pragma mark - Form

- (void)loadUserForm {
    // Define mapping
    [FKFormMapping mappingForClass:[MOUserFormModel class] block:^(FKFormMapping *mapping) {
        mapping.selectViewWillAppearBlock = _selectViewWillAppear;
        
        [mapping sectionWithTitle:NSLocalizedString(@" ", @"") identifier:@"logindetail"];
        [mapping mapAttribute:@"username"
                        title:NSLocalizedString(@"Username", @"")
              placeholderText:NSLocalizedString(@"ex: cashier", @"")
                         type:self.item ? FKFormAttributeMappingTypeLabel :FKFormAttributeMappingTypeText];
        [mapping mapAttribute:@"password"
                        title:NSLocalizedString(@"Password", @"")
              placeholderText:NSLocalizedString(@"secret password", @"")
                         type:FKFormAttributeMappingTypePassword];

        [mapping sectionWithTitle:NSLocalizedString(@" ", @"") identifier:@"userinfo"];
        [mapping mapAttribute:@"firstname"
                        title:NSLocalizedString(@"First Name", @"")
              placeholderText:NSLocalizedString(@"ex: John", @"")
                         type:FKFormAttributeMappingTypeText];
        [mapping mapAttribute:@"lastname"
                        title:NSLocalizedString(@"Last Name", @"")
              placeholderText:NSLocalizedString(@"ex: Doe", @"")
                         type:FKFormAttributeMappingTypeText];
        
        [mapping sectionWithTitle:NSLocalizedString(@" ", @"") identifier:@"roles"];
        [mapping mapAttribute:@"roles"
                        title:NSLocalizedString(@"Role", @"")
                 showInPicker:NO
            selectValuesBlock:^NSArray *(id value, id object, NSInteger *selectedValueIndex){
                // Get the role
                NSString *role = [(MOUserFormModel*)object roles];
                role = [role uppercaseString];
                // Get the index
                *selectedValueIndex = [_roles indexOfObject:role];
                // Return role list
                return _roles;
            } valueFromSelectBlock:^id(id value, id object, NSInteger selectedValueIndex) {
                return value;
            } labelValueBlock:^id(id value, id object) {
                return value;
            }];

        [mapping sectionWithTitle:NSLocalizedString(@" ", @"") identifier:@"active"];
        [mapping mapAttribute:@"active"
                        title:NSLocalizedString(@"Active", @"")
                         type:FKFormAttributeMappingTypeBoolean];
        
        [mapping sectionWithTitle:NSLocalizedString(@" ", @"") identifier:@"hidden"];
        [mapping mapAttribute:@"hidden"
                        title:NSLocalizedString(@"Hidden", @"")
                         type:FKFormAttributeMappingTypeBoolean];
        
        [mapping validationForAttribute:@"username" validBlock:^BOOL(NSString *value, id object) {
            // Empty is not allow
            if (value.length == 0)
                return NO;
            
            // Editing without changing username
            if (object && [value isEqualToString:((MOUserFormModel*)object).username])
                return YES;
            
            // Checking for dublication
            CBLDocument *usersDocument = [_database documentWithID:_documentid];
            if (!usersDocument)
                return NO;
            
            NSArray *users = [usersDocument propertyForKey:_documentid];
            if (!users)
                return NO;
            
            for (NSDictionary * user in users) {
                if ([user[@"username"] isEqualToString:value]) {
                    return FALSE;
                }
            }
            return YES;
        } errorMessageBlock:^NSString *(id value, id object) {
            if (value && ((NSString*)value).length == 0) {
                return NSLocalizedString(@"Username is required", @"");
            } else {
                return NSLocalizedString(@"Username already exists", @"");
            }
        }];
        
        [mapping validationForAttribute:@"password" validBlock:^BOOL(NSString *value, id object) {
            // Empty is not allow
            return value.length > 0;
        } errorMessageBlock:^NSString *(id value, id object) {
            return NSLocalizedString(@"Password is required", @"");
        }];
        
        [mapping validationForAttribute:@"firstname" validBlock:^BOOL(NSString *value, id object) {
            // Empty is not allow
            return value.length > 0;
        } errorMessageBlock:^NSString *(id value, id object) {
            return NSLocalizedString(@"First name is required", @"");
        }];
        
        [_formModel registerMapping:mapping];
    }];
    
    // Load the form model
    MOUserFormModel *model = [MOUserFormModel new];
    if (self.item) {
        model.username = self.item[@"username"];
        model.password = self.item[@"password"];
        model.firstname = self.item[@"firstname"];
        model.lastname = self.item[@"lastname"];
        model.roles = self.item[@"roles"];
        model.active = [@"active" isEqualToString:self.item[@"status"]];
        model.hidden = [@"yes" isEqualToString:self.item[@"hidden"]];
    } else {
        model.username = @"";
        model.password = @"";
        model.firstname = @"";
        model.lastname = @"";
        model.roles = @"CASHIER";
        model.active = YES;
        model.hidden = NO;
    }
    _itemModel = model;
    // Load data to form
    [_formModel loadFieldsWithObject:_itemModel];
}

- (void)loadCategoryForm {
    // Define mapping
    [FKFormMapping mappingForClass:[MOCategoryFormModel class] block:^(FKFormMapping *mapping) {
        mapping.selectViewWillAppearBlock = _selectViewWillAppear;
        
        [mapping sectionWithTitle:NSLocalizedString(@" ", @"") identifier:@"name"];
        [mapping mapAttribute:@"name1"
                        title:NSLocalizedString(@"Name 1", @"")
              placeholderText:NSLocalizedString(@"ex: Breakfast", @"")
                         type:FKFormAttributeMappingTypeText];
        [mapping mapAttribute:@"name2"
                        title:NSLocalizedString(@"Name 2", @"")
              placeholderText:NSLocalizedString(@"ex: BK", @"")
                         type:FKFormAttributeMappingTypeText];
        [mapping mapAttribute:@"name3"
                        title:NSLocalizedString(@"Name 3", @"")
              placeholderText:NSLocalizedString(@"ex: BKfast", @"")
                         type:FKFormAttributeMappingTypeText];
        
        [mapping sectionWithTitle:NSLocalizedString(@" ", @"") identifier:@"desc"];
        [mapping mapAttribute:@"desc"
                        title:NSLocalizedString(@"Description", @"")
                         type:FKFormAttributeMappingTypeBigText];
        
        [mapping sectionWithTitle:NSLocalizedString(@" ", @"") identifier:@"order"];
        [mapping mapAttribute:@"order"
                        title:NSLocalizedString(@"Order", @"")
              placeholderText:NSLocalizedString(@"", @"")
                         type:FKFormAttributeMappingTypeInteger];

        [mapping sectionWithTitle:NSLocalizedString(@" ", @"") identifier:@"printer"];
        [mapping mapAttribute:@"printerID"
                        title:NSLocalizedString(@"Ticket Printer", @"")
                 showInPicker:NO
            selectValuesBlock:^NSArray *(id value, id object, NSInteger *selectedValueIndex){
                // Get the role
                NSString *printerID = ((MOCategoryFormModel*)object).printerID;
                // Get the index
                *selectedValueIndex = [_printerIds indexOfObject:printerID];
                // Return printer id list
                return _printerNames;
            } valueFromSelectBlock:^id(id value, id object, NSInteger selectedValueIndex) {
                return [_printerIds objectAtIndex:selectedValueIndex];
            } labelValueBlock:^id(id value, id object) {
                return _printers[value];
            }];
        
        [mapping sectionWithTitle:NSLocalizedString(@" ", @"") identifier:@"maxblock"];
        [mapping mapAttribute:@"maxBlock"
                        title:NSLocalizedString(@"Max block", @"")
                         type:FKFormAttributeMappingTypeInteger];
        
        [mapping sectionWithTitle:NSLocalizedString(@" ", @"") identifier:@"color"];
        [mapping mapAttribute:@"bgColor"
                        title:NSLocalizedString(@"Background color", @"")
                         type:FKFormAttributeMappingTypeText];
        
        [mapping mapAttribute:@"fontColor"
                        title:NSLocalizedString(@"Font color", @"")
                         type:FKFormAttributeMappingTypeText];
        
        [mapping sectionWithTitle:NSLocalizedString(@" ", @"") identifier:@"visible"];
        [mapping mapAttribute:@"visible"
                        title:NSLocalizedString(@"Visibility", @"")
                         type:FKFormAttributeMappingTypeBoolean];
        
        [mapping validationForAttribute:@"name1" validBlock:^BOOL(NSString *value, id object) {
            // Empty is not allow
            return value.length > 0;
        } errorMessageBlock:^NSString *(id value, id object) {
            return NSLocalizedString(@"Name 1 is required", @"");
        }];
        
        [_formModel registerMapping:mapping];
    }];
    
    // Load the form model
    MOCategoryFormModel *model = [MOCategoryFormModel new];
    if (self.item) {
        model.ID = self.item[@"id"];
        model.parentID = self.item[@"parent_id"];
        model.name1 = self.item[@"name1"];
        model.name2 = self.item[@"name2"];
        model.name3 = self.item[@"name3"];
        model.desc = self.item[@"description"];
        model.fontColor = self.item[@"font_color"];
        model.bgColor = self.item[@"bg_color"];
        model.maxBlock = self.item[@"max_block"];
        model.visible = [@"true" isEqualToString:self.item[@"self_order"]];
        model.printerID = self.item[@"printer_id"];
        model.order = [self.item[@"arrangement"] intValue];
    } else {
        model.ID = @"";
        model.parentID = @"0";
        model.name1 = @"";
        model.name2 = @"";
        model.name3 = @"";
        model.desc = @"";
        model.fontColor = @"FFFFFF";
        model.bgColor = @"33CCFF";
        model.maxBlock = [NSNumber numberWithInteger:6];
        model.visible = YES;
        model.printerID = @"KP0";
        model.order = 0;
    }
    _itemModel = model;
    // Load data to form
    [_formModel loadFieldsWithObject:_itemModel];
}

- (void)loadItemForm {
    if (!_database)
        return;
    CBLDocument *document = [_database documentWithID:@"categories"];
    if (!document)
        return;
    NSArray *categories = [document propertyForKey:@"categories"];
    
    // Preparing for the formkit selection dialog
    NSMutableArray *categoryNames = [NSMutableArray new];
    NSMutableArray *categoryIds = [NSMutableArray new];
    for (int i = 0; i < categories.count; i++) {
        NSDictionary *cat = [categories objectAtIndex:i];
        [categoryNames addObject:cat[@"name1"]];
        [categoryIds addObject:cat[@"id"]];
    }
    NSDictionary *categoryIdNameMap = [NSDictionary dictionaryWithObjects:categoryNames forKeys:categoryIds];
    
    // Define mapping
    [FKFormMapping mappingForClass:[MOItemFormModel class] block:^(FKFormMapping *mapping) {
        mapping.selectViewWillAppearBlock = _selectViewWillAppear;
        
        [mapping sectionWithTitle:NSLocalizedString(@" ", @"") identifier:@"category"];
        [mapping mapAttribute:@"category_id"
                        title:NSLocalizedString(@"Category", @"")
                 showInPicker:NO
            selectValuesBlock:^NSArray *(id value, id object, NSInteger *selectedValueIndex){
                // Get the role
                NSString *categoryid = ((MOItemFormModel*)object).category_id;
                // Get the index
                *selectedValueIndex = [categoryIds indexOfObject:categoryid];
                // Return role list
                return categoryNames;
            } valueFromSelectBlock:^id(id value, id object, NSInteger selectedValueIndex) {
                return [categoryIds objectAtIndex:selectedValueIndex];
            } labelValueBlock:^id(id value, id object) {
                return [categoryIdNameMap objectForKey:value];
            }];

        [mapping sectionWithTitle:NSLocalizedString(@" ", @"") identifier:@"name"];
        [mapping mapAttribute:@"name1"
                        title:NSLocalizedString(@"Name", @"")
              placeholderText:NSLocalizedString(@"ex: Sandwich", @"")
                         type:FKFormAttributeMappingTypeText];
        [mapping mapAttribute:@"name2"
                        title:NSLocalizedString(@"Name 2", @"")
              placeholderText:NSLocalizedString(@"displayed on Mobile Order", @"")
                         type:FKFormAttributeMappingTypeText];
        
        [mapping sectionWithTitle:NSLocalizedString(@" ", @"") identifier:@"desc"];
        [mapping mapAttribute:@"desc"
                        title:NSLocalizedString(@"Description", @"")
              placeholderText:NSLocalizedString(@"ex: Biggest sandwich", @"")
                         type:FKFormAttributeMappingTypeBigText];

        [mapping sectionWithTitle:NSLocalizedString(@" ", @"") identifier:@"prices"];
        [mapping mapAttribute:@"regular_price"
                        title:NSLocalizedString(@"Regular price", @"")
              placeholderText:NSLocalizedString(@"", @"")
                         type:FKFormAttributeMappingTypeFloat];
        [mapping mapAttribute:@"promo_price"
                        title:NSLocalizedString(@"Promo price", @"")
              placeholderText:NSLocalizedString(@"", @"")
                         type:FKFormAttributeMappingTypeFloat];
        [mapping mapAttribute:@"combo_price"
                        title:NSLocalizedString(@"Combo price", @"")
              placeholderText:NSLocalizedString(@"", @"")
                         type:FKFormAttributeMappingTypeFloat];

        [mapping sectionWithTitle:NSLocalizedString(@" ", @"") identifier:@"extras"];
        [mapping mapAttribute:@"item_number"
                        title:NSLocalizedString(@"Item number", @"")
              placeholderText:NSLocalizedString(@"ex: #1", @"")
                         type:FKFormAttributeMappingTypeText];
        [mapping mapAttribute:@"barcode"
                        title:NSLocalizedString(@"Barcode", @"")
              placeholderText:NSLocalizedString(@"ex: 123456", @"")
                         type:FKFormAttributeMappingTypeText];
        [mapping mapAttribute:@"quantity"
                        title:NSLocalizedString(@"Quantity", @"")
              placeholderText:NSLocalizedString(@"ex: 1", @"")
                         type:FKFormAttributeMappingTypeInteger];
        [mapping mapAttribute:@"keyword"
                        title:NSLocalizedString(@"Keyword", @"")
              placeholderText:NSLocalizedString(@"ex: sandwich", @"")
                         type:FKFormAttributeMappingTypeText];
        [mapping mapAttribute:@"mods"
                        title:NSLocalizedString(@"Mods", @"")
              placeholderText:NSLocalizedString(@"ex: mod", @"")
                         type:FKFormAttributeMappingTypeText];
        [mapping mapAttribute:@"to_kitchen"
                        title:NSLocalizedString(@"To kitchen", @"")
                         type:FKFormAttributeMappingTypeBoolean];

        [mapping sectionWithTitle:NSLocalizedString(@" ", @"") identifier:@"taxes"];
        [mapping mapAttribute:@"togo_tax"
                        title:NSLocalizedString(@"Togo tax", @"")
                         type:FKFormAttributeMappingTypeBoolean];
        [mapping mapAttribute:@"forhere_tax"
                        title:NSLocalizedString(@"Forhere tax", @"")
                         type:FKFormAttributeMappingTypeBoolean];

        [mapping sectionWithTitle:NSLocalizedString(@" ", @"") identifier:@"visible"];
        [mapping mapAttribute:@"visible"
                        title:NSLocalizedString(@"Visibility", @"")
                         type:FKFormAttributeMappingTypeBoolean];
        
        [mapping validationForAttribute:@"name1" validBlock:^BOOL(NSString *value, id object) {
            // Empty is not allow
            return value.length > 0;
        } errorMessageBlock:^NSString *(id value, id object) {
            return NSLocalizedString(@"Name is required", @"");
        }];
        
        [_formModel registerMapping:mapping];
    }];
    
    // Load the form model
    MOItemFormModel *model = [MOItemFormModel new];
    if (self.item) {
        model.ID = self.item[@"id"];
        model.category = self.item[@"category"];
        model.category_id = self.item[@"id_category"];
        model.item_number = self.item[@"item_number"];
        model.name1 = self.item[@"name1"];
        model.name2 = self.item[@"name2"];
        model.desc = self.item[@"description"];
        model.keyword = self.item[@"keyword"];
        model.regular_price = [self.item[@"regular_price"] doubleValue];
        model.promo_price = [self.item[@"promo_price"] doubleValue];
        model.combo_price = [self.item[@"combo_price"] doubleValue];
        model.quantity = [self.item[@"quantity"] intValue];
        model.to_kitchen = [@"yes" isEqualToString:self.item[@"to_kitchen"]];
        model.barcode = self.item[@"barcode"];
        model.togo_tax = [@"yes" isEqualToString:self.item[@"togo_tax"]];
        model.forhere_tax = [@"yes" isEqualToString:self.item[@"forhere_tax"]];
        model.mods = self.item[@"mods"];
        if ([@"no_mod" isEqualToString:model.mods]) {
            model.mods = @"";
        }
        model.visible = [@"yes" isEqualToString:self.item[@"self_order"]];
    } else {
        model.ID = @"";
        if (self.category) {
            model.category = self.category[@"name1"];
            model.category_id = self.category[@"id"];
        } else {
            model.category = [categoryNames objectAtIndex:0];
            model.category_id = [categoryIds objectAtIndex:0];
        }
        model.item_number = @"";
        model.name1 = @"";
        model.name1 = @"";
        model.desc = @"";
        model.keyword = @"";
        model.regular_price = 0.0;
        model.promo_price = 0.0;
        model.combo_price = 0.0;
        model.quantity = 1;
        model.to_kitchen = YES;
        model.barcode = @"";
        model.togo_tax = YES;
        model.forhere_tax = YES;
        model.mods = @"";
        model.visible = YES;
    }
    _itemModel = model;
    // Load data to form
    [_formModel loadFieldsWithObject:_itemModel];
}

- (void)loadModifierForm {
    if (!_database)
        return;
    CBLDocument *document = [_database documentWithID:@"categories"];
    if (!document)
        return;
    NSArray *categories = [document propertyForKey:@"categories"];
    
    NSMutableArray *categoryNames = [NSMutableArray new];
    NSMutableArray *categoryIds = [NSMutableArray new];
    for (int i = 0; i < categories.count; i++) {
        NSDictionary *cat = [categories objectAtIndex:i];
        [categoryNames addObject:cat[@"name1"]];
        [categoryIds addObject:cat[@"id"]];
    }
    NSDictionary *categoryIdNameMap = [NSDictionary dictionaryWithObjects:categoryNames forKeys:categoryIds];
    
    // Define mapping
    [FKFormMapping mappingForClass:[MOModifierFormModel class] block:^(FKFormMapping *mapping) {
        mapping.selectViewWillAppearBlock = _selectViewWillAppear;
        
        [mapping sectionWithTitle:NSLocalizedString(@" ", @"") identifier:@"category"];
        [mapping mapAttribute:@"category_id"
                        title:NSLocalizedString(@"Category", @"")
                 showInPicker:NO
            selectValuesBlock:^NSArray *(id value, id object, NSInteger *selectedValueIndex){
                // Get the role
                NSString *categoryid = ((MOModifierFormModel*)object).category_id;
                // Get the index
                *selectedValueIndex = [categoryIds indexOfObject:categoryid];
                // Return role list
                return categoryNames;
            } valueFromSelectBlock:^id(id value, id object, NSInteger selectedValueIndex) {
                return [categoryIds objectAtIndex:selectedValueIndex];
            } labelValueBlock:^id(id value, id object) {
                return [categoryIdNameMap objectForKey:value];
            }];
        
        [mapping sectionWithTitle:NSLocalizedString(@" ", @"") identifier:@"names"];
        [mapping mapAttribute:@"name1"
                        title:NSLocalizedString(@"Name", @"")
              placeholderText:NSLocalizedString(@"ex: Sandwich", @"")
                         type:FKFormAttributeMappingTypeText];
        [mapping mapAttribute:@"name2"
                        title:NSLocalizedString(@"Name 2", @"")
              placeholderText:NSLocalizedString(@"displayed on Mobile Order", @"")
                         type:FKFormAttributeMappingTypeText];
        [mapping mapAttribute:@"name3"
                        title:NSLocalizedString(@"Name 3", @"")
              placeholderText:NSLocalizedString(@"ex: sw", @"")
                         type:FKFormAttributeMappingTypeText];

        
        [mapping sectionWithTitle:NSLocalizedString(@" ", @"") identifier:@"prices"];
        [mapping mapAttribute:@"price"
                        title:NSLocalizedString(@"Price", @"")
              placeholderText:NSLocalizedString(@"", @"")
                         type:FKFormAttributeMappingTypeFloat];
        [mapping mapAttribute:@"no"
                        title:NSLocalizedString(@"No", @"")
              placeholderText:NSLocalizedString(@"", @"")
                         type:FKFormAttributeMappingTypeFloat];
        [mapping mapAttribute:@"extra"
                        title:NSLocalizedString(@"Extra", @"")
              placeholderText:NSLocalizedString(@"", @"")
                         type:FKFormAttributeMappingTypeFloat];
        [mapping mapAttribute:@"less"
                        title:NSLocalizedString(@"Less", @"")
              placeholderText:NSLocalizedString(@"", @"")
                         type:FKFormAttributeMappingTypeFloat];
        [mapping mapAttribute:@"onside"
                        title:NSLocalizedString(@"Onside", @"")
              placeholderText:NSLocalizedString(@"", @"")
                         type:FKFormAttributeMappingTypeFloat];
        
        [mapping validationForAttribute:@"name1" validBlock:^BOOL(NSString *value, id object) {
            // Empty is not allow
            return value.length > 0;
        } errorMessageBlock:^NSString *(id value, id object) {
            return NSLocalizedString(@"Name 1 is required", @"");
        }];
        
        [_formModel registerMapping:mapping];
    }];
    
    // Load the form model
    MOModifierFormModel *model = [MOModifierFormModel new];
    if (self.item) {
        model.ID = self.item[@"id"];
        model.category = self.item[@"category"];
        model.category_id = self.item[@"id_category"];
        model.name1 = self.item[@"name1"];
        model.name2 = self.item[@"name2"];
        model.name3 = self.item[@"name3"];
        model.price = [self.item[@"price"] doubleValue];
        model.no = [self.item[@"no"] doubleValue];
        model.extra = [self.item[@"extra"] doubleValue];
        model.less = [self.item[@"less"] doubleValue];
        model.onside = [self.item[@"onside"] doubleValue];
    } else {
        model.ID = @"";
        if (self.category) {
            model.category = self.category[@"name1"];
            model.category_id = self.category[@"id"];
        } else {
            model.category = [categoryNames objectAtIndex:0];
            model.category_id = [categoryIds objectAtIndex:0];
        }
        model.name1 = @"";
        model.name2 = @"";
        model.name3 = @"";
        model.price = 0.0;
        model.no = 0.0;
        model.extra = 0.0;
        model.less = 0.0;
        model.onside = 0.0;
    }
    _itemModel = model;
    
    // Load data to form
    [_formModel loadFieldsWithObject:_itemModel];
}

- (void)copyModel:(MOBaseFormModel*)m toDict:(NSMutableDictionary*)dict {
    if (self.itemType == MOUser) {
        MOUserFormModel *model = (MOUserFormModel*)m;
        [dict setValue:model.username forKey:@"username"];
        [dict setValue:model.password forKey:@"password"];
        [dict setValue:model.firstname forKey:@"firstname"];
        [dict setValue:model.lastname forKey:@"lastname"];
        [dict setValue:model.roles forKey:@"roles"];
        [dict setValue:model.active ? @"active" : @"deactive" forKey:@"active"];
        [dict setValue:model.hidden ? @"true" : @"false" forKey:@"hidden"];
    } else if (self.itemType == MOCategory) {
        MOCategoryFormModel *model = (MOCategoryFormModel*)m;
        if (!model.ID || model.ID.length == 0) {
            model.ID = [self nextId];
        }
        [dict setValue:model.ID forKey:@"id"];
        [dict setValue:model.parentID forKey:@"parent_id"];
        [dict setValue:model.name1 forKey:@"name1"];
        [dict setValue:model.name2 forKey:@"name2"];
        [dict setValue:model.name3 forKey:@"name3"];
        [dict setValue:model.desc forKey:@"description"];
        [dict setValue:model.fontColor forKey:@"font_color"];
        [dict setValue:model.bgColor forKey:@"bg_color"];
        [dict setValue:model.maxBlock forKey:@"max_block"];
        [dict setValue:model.visible ? @"true" : @"false" forKey:@"self_order"];
        [dict setValue:model.printerID forKey:@"printer_id"];
        [dict setValue:[NSString stringWithFormat:@"%d", model.order] forKey:@"arrangement"];
    } else if (self.itemType == MOItem) {
        MOItemFormModel *model = (MOItemFormModel*)m;
        if (!model.ID || model.ID.length == 0) {
            model.ID = [self nextId];
        }
        NSDictionary *category = [self categoryById:model.category_id];
        if (category) {
            model.category = category[@"name1"];
        }
        [dict setValue:model.ID forKey:@"id"];
        [dict setValue:model.category forKey:@"category"];
        [dict setValue:model.category_id forKey:@"id_category"];
        [dict setValue:model.item_number forKey:@"item_number"];
        [dict setValue:model.name1 forKey:@"name1"];
        [dict setValue:model.name2 forKey:@"name2"];
        [dict setValue:model.desc forKey:@"description"];
        [dict setValue:model.keyword forKey:@"keyword"];
        [dict setValue:[NSString stringWithFormat:@"%.2f", model.regular_price] forKey:@"regular_price"];
        [dict setValue:[NSString stringWithFormat:@"%.2f", model.promo_price] forKey:@"promo_price"];
        [dict setValue:[NSString stringWithFormat:@"%.2f", model.combo_price] forKey:@"combo_price"];
        [dict setValue:[NSString stringWithFormat:@"%d", model.quantity] forKey:@"quantity"];
        [dict setValue:model.to_kitchen ? @"yes" : @"no" forKey:@"to_kitchen"];
        [dict setValue:model.barcode forKey:@"barcode"];
        [dict setValue:model.togo_tax ? @"yes" : @"no" forKey:@"togo_tax"];
        [dict setValue:model.forhere_tax ? @"yes" : @"no" forKey:@"forhere_tax"];
        [dict setValue:model.mods forKey:@"mods"];
        [dict setValue:model.visible ? @"true" : @"false" forKey:@"self_order"];
    } else if (self.itemType == MOModifier) {
        MOModifierFormModel *model = (MOModifierFormModel*)m;
        
        if (!model.ID || model.ID.length == 0) {
            model.ID = [self nextId];
        }
        NSDictionary *category = [self categoryById:model.category_id];
        if (category) {
            model.category = category[@"name1"];
        }
        
        [dict setValue:model.ID forKey:@"id"];
        [dict setValue:model.category forKey:@"category"];
        [dict setValue:model.category_id forKey:@"id_category"];
        [dict setValue:model.name1 forKey:@"name1"];
        [dict setValue:model.name2 forKey:@"name2"];
        [dict setValue:model.name3 forKey:@"name3"];
        [dict setValue:[NSString stringWithFormat:@"%.2f", model.price] forKey:@"price"];
        [dict setValue:[NSString stringWithFormat:@"%.2f", model.no] forKey:@"no"];
        [dict setValue:[NSString stringWithFormat:@"%.2f", model.extra] forKey:@"extra"];
        [dict setValue:[NSString stringWithFormat:@"%.2f", model.less] forKey:@"less"];
        [dict setValue:[NSString stringWithFormat:@"%.2f", model.onside] forKey:@"onside"];
    }
}

- (NSString *)nextId {
    if (self.itemType == MOUser)
        return nil;
    // Return default
    CBLDocument *document = [_database documentWithID:_documentid];
    if (!document)
        return @"1";
    // Return default
    NSArray *items = [document propertyForKey:_documentid];
    if (!items)
        return @"1";
    
    // Find the next id
    int maxID = -1;
    for (NSDictionary *i in items) {
        if ([i[@"id"] intValue] > maxID) {
            maxID = [i[@"id"] intValue];
        }
    }
    return [NSString stringWithFormat:@"%d", (maxID + 1)];
}

- (NSDictionary*)categoryById:(NSString*)catid {
    if (!catid || [catid isEqual:[NSNull null]])
        return nil;
    CBLDocument *document = [_database documentWithID:@"categories"];
    if (document) {
        NSArray *categories = [document propertyForKey:@"categories"];
        for (NSDictionary *c in categories) {
            if ([catid isEqualToString:c[@"id"]]) {
                return c;
            }
        }
    }
    return nil;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (textField.text.length > 0 && [textField.text floatValue] == 0.0f) {
        textField.text = @"";
    }
    return YES;
}

#pragma mark - IBAction

- (IBAction)backDidTouch:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)deleteDidTouch:(id)sender {
    if (!_database)
        return;
    
    if (!self.item)
        return;
    
    NSString *title;
    if (self.itemType == MOUser) {
        title = [NSString stringWithFormat:@"%@ %@", self.item[@"firstname"], self.item[@"lastname"]];
    } else {
        title = self.item[@"name1"];
    }
    
    // Delete item
    [UIAlertView showWithTitle:trim(title)
                       message:NSLocalizedString(@"Deleting this item is irreversable. Continue?", @"")
             cancelButtonTitle:NSLocalizedString(@"No", @"")
             otherButtonTitles:[NSArray arrayWithObject:NSLocalizedString(@"Yes", @"")]
                      tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                          if (buttonIndex == 1) {
                              [self deleteCurrentItem];
                          }
                      }];
}


- (void)deleteCurrentItem {
    NSString *fieldname = (self.itemType == MOUser) ? @"username" : @"id";
    NSString *itemid = self.item[fieldname];
    if (!itemid) {
        [self.view makeToast:NSLocalizedString(@"Missing key property", @"")];
        return;
    }
    
    // retrieve the document from the database
    CBLDocument *document = [_database documentWithID:_documentid];
    if (!document)
        return;
    
    NSMutableDictionary *properties = [document.properties mutableCopy];
    NSMutableArray *items = [[properties objectForKey:_documentid] mutableCopy];
    // Make sure we have the correspondence field (same name with
    // document)
    if (!items)
        return;
    
    for (id item in items) {
        if (![itemid isEqualToString:item[fieldname]]) {
            [items removeObject:item];
            break;
        }
    }
    
    // Save to document
    [properties setValue:items forKey:_documentid];
    NSError *error;
    [document putProperties:properties error:&error];
    
    if (self.itemType == MOCategory) {
        // Delete child items
        CBLDocument *itemsDocument = [_database documentWithID:@"items"];
        NSMutableDictionary *itemsProperties = [itemsDocument.properties mutableCopy];
        
        NSArray *existingItems = itemsProperties[@"items"];
        NSMutableArray *newItems = [NSMutableArray new];
        for (NSDictionary *item in existingItems) {
            if (![itemid isEqualToString:item[@"id_category"]]) {
                [newItems addObject:item];
            }
        }
        [itemsProperties setValue:newItems forKey:@"items"];
        [itemsDocument putProperties:itemsProperties error:nil];
        
        
        // Delete child modifiers
        CBLDocument *modifiersDocument = [_database documentWithID:@"modifiers"];
        NSMutableDictionary *modifiersProperties = [modifiersDocument.properties mutableCopy];
        
        NSArray *existingModifiers = modifiersProperties[@"modifiers"];
        NSMutableArray *newModifiers = [NSMutableArray new];
        for (NSDictionary *mod in existingModifiers) {
            if (![itemid isEqualToString:mod[@"id_category"]]) {
                [newModifiers addObject:mod];
            }
        }
        [modifiersProperties setValue:newModifiers forKey:@"modifiers"];
        [modifiersDocument putProperties:modifiersProperties error:nil];
    }
    
    
    // inform user
    [self.view makeToast:NSLocalizedString(@"Deleted", @"")];
    
    // Back to list
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)saveDidTouch:(id)sender {
    if (!_database)
        return;
    // Begin the validate
    [_formModel save];
    // No error found
    if (_formModel.invalidAttributes.count == 0) {
        // Get the doc
        CBLDocument *document = [_database documentWithID:_documentid];
        if (!document)
            return;
        // Get the array
        NSMutableArray *items = [[document propertyForKey:_documentid] mutableCopy];
        if (!items) {
            // Create when needed
            items = [NSMutableArray new];
        }
        // Convert form model to dictionary for saving
        NSString *idfield = self.itemType == MOUser ? @"username" : @"id";
        NSMutableDictionary *item = nil;
        
        // In case of editing, find the corresponding record
        if (self.item) {
            NSString *itemid = self.item[idfield];
            for (int i = 0; i < items.count; i++) {
                NSDictionary *oi = [items objectAtIndex:i];
                if ([itemid isEqualToString:oi[idfield]]) {
                    // Get a mutable copy
                    item = [oi mutableCopy];
                    // Replace existing one with this one
                    [items replaceObjectAtIndex:i withObject:item];
                    break;
                }
            }
        }
        // If not exitsting
        if (!item) {
            // Create a new one
            item = [NSMutableDictionary new];
            // Add to array
            [items addObject:item];
        }
        // Populate item with data from model
        [self copyModel:_itemModel toDict:item];
        
        NSString *date = [[MOItemDetailViewController dateFormatter] stringFromDate:[NSDate date]];
        if (!self.item) {
            item[@"created_time"] = date;
        }
        item[@"modified_time"] = date;
        
        // Save to document
        NSMutableDictionary *properties = [document.properties mutableCopy];
        [properties setValue:items forKey:_documentid];
        NSError *error;
        [document putProperties:properties error:&error];
        
        
        // Category is updated with new name
        if (self.itemType == MOCategory && self.item && ![self.item[@"name1"] isEqualToString:item[@"name"]]) {
            NSString *catid = item[@"id"];
            NSString *catname = item[@"name1"];
            
            // Update child items
            CBLDocument *itemsDocument = [_database documentWithID:@"items"];
            NSMutableDictionary *itemsProperties = [itemsDocument.properties mutableCopy];
            NSArray *items = itemsProperties[@"items"];
            NSMutableArray *newItems = [NSMutableArray new];
            for (NSDictionary *item in items) {
                NSDictionary *i = item;
                if ([catid isEqualToString:i[@"id_category"]]) {
                    i = [NSMutableDictionary dictionaryWithDictionary:i];
                    [i setValue:catname forKey:@"category"];
                }
                [newItems addObject:i];
            }
            [itemsProperties setValue:newItems forKey:@"items"];
            [itemsDocument putProperties:itemsProperties error:nil];
            
            
            // Update child modifiers
            CBLDocument *modifiersDocument = [_database documentWithID:@"modifiers"];
            NSMutableDictionary *modifiersProperties = [modifiersDocument.properties mutableCopy];
            NSArray *modifiers = modifiersDocument[@"modifiers"];
            NSMutableArray *newModifiers = [NSMutableArray new];
            for (NSDictionary *mod in modifiers) {
                NSDictionary *m = mod;
                if ([catid isEqualToString:m[@"id_category"]]) {
                    m = [NSMutableDictionary dictionaryWithDictionary:m];
                    [m setValue:catname forKey:@"category"];
                }
                [newModifiers addObject:m];
            }
            [modifiersProperties setValue:newModifiers forKey:@"modifiers"];
            [modifiersDocument putProperties:modifiersProperties error:nil];
        }
        
        // inform user
        [self.view makeToast:NSLocalizedString(@"Created", @"")];
        
        // Back to list
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - Keyboard Listener

- (void)keyboardWillShow:(NSNotification *)notification {
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets;
    if (UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation])) {
        contentInsets = UIEdgeInsetsMake(0.0, 0.0, (keyboardSize.height) + 20, 0.0);
    } else {
        contentInsets = UIEdgeInsetsMake(0.0, 0.0, (keyboardSize.width), 0.0);
    }
    
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
    //    [self.tableView scrollToRowAtIndexPath:self.editingIndexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    NSNumber *rate = notification.userInfo[UIKeyboardAnimationDurationUserInfoKey];
    [UIView animateWithDuration:rate.floatValue animations:^{
        self.tableView.contentInset = UIEdgeInsetsZero;
        self.tableView.scrollIndicatorInsets = UIEdgeInsetsZero;
    }];
}


#pragma mark - Static

static NSDateFormatter *_dateFormatter;
+ (NSDateFormatter*)dateFormatter {
    if (_dateFormatter == nil) {
        _dateFormatter = [NSDateFormatter new];
        _dateFormatter.dateFormat = @"MM/dd/yyyy hh:mm a";
    }
    return _dateFormatter;
}



@end


@implementation MOBaseFormModel
@end

@implementation MOUserFormModel
@end

@implementation MOCategoryFormModel
@end

@implementation MOItemFormModel
@end

@implementation MOModifierFormModel
@end
