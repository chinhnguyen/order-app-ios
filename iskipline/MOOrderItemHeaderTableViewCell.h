//
//  MOOrderItemHeaderTableViewCell.h
//  iskipline
//
//  Created by Chinh Nguyen on 12/19/14.
//  Copyright (c) 2014 Chinh Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MOOrderItemHeaderTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@end
