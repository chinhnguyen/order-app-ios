//
//  MOPickupTimeViewController.m
//  iskipline
//
//  Created by Chinh Nguyen on 2/25/15.
//  Copyright (c) 2015 Chinh Nguyen. All rights reserved.
//

#import "MOPickupTimeViewController.h"

@interface MOPickupTimeViewController () {
    NSInteger _openHour;
    NSInteger _openMinute;
    NSInteger _closeHour;
    NSInteger _closeMinute;
}

@end

@implementation MOPickupTimeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (!self.store || !self.order)
        return;
    
    
//#if __IPHONE_OS_VERSION_MAX_ALLOWED > __IPHONE_8_1
//#error "Check if this hack works on this OS"
//#endif
    
    [self.dateTimePicker setValue:[UIColor whiteColor] forKeyPath:@"textColor"];
    
    SEL selector = NSSelectorFromString(@"setHighlightsToday:");
    NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:[UIDatePicker instanceMethodSignatureForSelector:selector]];
    BOOL no = NO;
    [invocation setSelector:selector];
    [invocation setArgument:&no atIndex:2];
    [invocation invokeWithTarget:self.dateTimePicker];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"HH:mm"];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *now = [NSDate date];
    if (self.store.open && self.store.open.length > 0) {
        NSDate *openTime = [dateFormat dateFromString:self.store.open];
        NSDateComponents *components = [calendar components:(NSHourCalendarUnit | NSMinuteCalendarUnit) fromDate:openTime];
        _openHour = [components hour];
        _openMinute = [components minute];
        
        NSDate *minOrderTime = [calendar dateBySettingHour:_openHour minute:_openMinute second:0 ofDate:now options:NSCalendarWrapComponents];
        
        self.dateTimePicker.minimumDate = [minOrderTime laterDate:now];
    } else {
        self.dateTimePicker.minimumDate = now;
        _openHour = -1;
        _openMinute = -1;
    }
    
    if (self.store.close && self.store.close.length > 0) {
        NSDate *closeTime = [dateFormat dateFromString:self.store.close];
        NSDateComponents *components = [calendar components:(NSHourCalendarUnit | NSMinuteCalendarUnit) fromDate:closeTime];
        _closeHour = [components hour];
        _closeMinute = [components minute] - 30;
        
        NSDate *maxOrderTime = [calendar dateBySettingHour:(_closeHour < 12 ? _closeHour + 24 : _closeHour) minute:_closeMinute second:0 ofDate:now options:NSCalendarWrapComponents];
        self.dateTimePicker.maximumDate = maxOrderTime;
    } else {
        _closeHour = -1;
        _closeMinute = -1;
    }
    
    self.dateTimePicker.date = [calendar dateByAddingUnit:NSCalendarUnitMinute value:5 toDate:self.dateTimePicker.minimumDate options:NSCalendarWrapComponents];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - Data Validation

- (BOOL)checkPickupTime {
    if ([self.dateTimePicker.date compare:[NSDate date]] == NSOrderedAscending) {
        [UIAlertView showWithTitle:NSLocalizedString(@"Invalid time", @"") message:NSLocalizedString(@"Please select a valid pickup time (you have selected a time in the past).", @"") cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
        return NO;
    }
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSHourCalendarUnit | NSMinuteCalendarUnit) fromDate:self.dateTimePicker.date];
    NSInteger time = components.hour * 60 + components.minute;
    if ((_openHour >= 0 && _openMinute >= 0 && time < (_openHour * 60 + _openMinute)) ||
        (_closeHour >= 0 && _closeMinute >= 0 && time > (_closeHour * 60 + _closeMinute))) {
        [UIAlertView showWithTitle:NSLocalizedString(@"Invalid time", @"") message:NSLocalizedString(@"Sorry, your pickup date or time must be between the store business hours.", @"") cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
        return NO;
    }
    
    return YES;
}

- (BOOL)checkUser {
    if (!self.user || !self.user[@"CustomerID"]) {
        [UIAlertView showWithTitle:NSLocalizedString(@"Invalid user", @"") message:NSLocalizedString(@"Your customer record is missing some critical data that prevents your from submitting orders.\n\n Please contact the application\'s administrator for further support.", @"") cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
        return NO;
    }
    return YES;
}

#pragma mark - IBAction

- (IBAction)submitDidTouch:(id)sender {
    if (!self.store || !self.order || ![self checkPickupTime] || ![self checkUser] || ![self checkNetwork])
        return;
    
    // Create the transaction information
    MOTransaction *transaction = [MOTransaction new];
    double subtotal = 0, tax = 0, discount = 0;
    [self.order calculateTotalDetail:&subtotal andTax:&tax];
    
    // Calculate discount
    int discountRate = [self.store discount];
    if (discountRate > 0) {
        discount = subtotal * discount / 100.0;
    }
    
    // Set value to transaction
    transaction.taxAmount = tax;
    transaction.subTotalAmount = subtotal;
    transaction.discountAmount = discount;
    transaction.totalAmount = subtotal - discount + tax;
    
    transaction.entryMethod = @"M";
    transaction.clerkId = @"iPhone";
    transaction.customerId = self.user[@"CustomerID"];
    
    void (^_showChargingFailedMessage)() = ^{
        [UIAlertView showWithTitle:NSLocalizedString(@"Failed ordering", @"") message:NSLocalizedString(@"Sorry, your credit card did not go through. Please check your credit card detail and try again!", @"") cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
    };
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self.store chargeCustomer:transaction success:^(MOTransaction *transaction) {
        if (!transaction.statusCode || ![transaction.statusCode isEqualToString:@"0"]) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            _showChargingFailedMessage();
            return;
        }
        
        MOAppUser *appuser = [MOAppUser new];
        appuser.phone = self.user[@"phone"];
        appuser.firstname = self.user[@"firstname"];
        appuser.lastname = self.user[@"lastname"];
        appuser.email = self.user[@"email"];
        appuser.address = self.user[@"address"];
        appuser.zip = self.user[@"zip"];
        appuser.customerId = self.user[kXPCustomerID];
        
        [self.store makeOrder:self.order.orderItems.allValues forUser:appuser withTransaction:transaction andPickupTime:nil subsribe:NO ereceipt:@"email" success:^(MOOrder *order) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            // Inform user
            [UIAlertView showWithTitle:NSLocalizedString(@"Order submitted", @"")
                               message:NSLocalizedString(@"Thanks for ordering, please wait while your order is being served.", @"")
                     cancelButtonTitle:NSLocalizedString(@"OK", @"")
                     otherButtonTitles:nil
                              tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                  // Now restart ordering
                                  [self performSegueWithIdentifier:@"restartOrdering" sender:self];
                              }];
        } failure:^(NSError *error) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            [UIAlertView showWithTitle:NSLocalizedString(@"Order failed", @"")
                               message:NSLocalizedString(@"Sorry! The order was not submitted successfully.", @"")
                     cancelButtonTitle:NSLocalizedString(@"Close", @"")
                     otherButtonTitles:nil
                              tapBlock:nil];
        } receiptSuccess:^{
            // Do nothing
        } receiptFailure:^(NSError *error) {
            [self.view makeToast:NSLocalizedString(@"Failed sending receipt", @"")];
        }];
        
    } failure:^(NSError *error) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        _showChargingFailedMessage();
    }];
}

@end
