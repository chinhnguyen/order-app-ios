//
//  MOStore.h
//  iskipline
//
//  Created by Chinh Nguyen on 12/16/14.
//  Copyright (c) 2014 Chinh Nguyen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CouchbaseLite/CouchbaseLite.h>
#import "MOAppUser.h"
#import "MOTransaction.h"
#import "MOOrder.h"

#define kMOErrorOrderDbNotExists 404

@interface MOStore : CBLModel

@property (nonatomic, retain) NSString* account;
@property (nonatomic, retain) NSString* phone;
@property (nonatomic, retain) NSString* email;
@property (nonatomic, retain) NSString* store;
@property (nonatomic, retain) NSString* street;
@property (nonatomic, retain) NSString* zipcode;
@property (nonatomic, retain) NSString* city;
@property (nonatomic, retain) NSString* state;
@property (nonatomic, retain) NSString* close;
@property (nonatomic, retain) NSString* open;
@property (nonatomic, retain) NSString* merchant_id;
@property (nonatomic, retain) NSString* merchant_key;
@property (nonatomic, retain) NSString* image;
@property (nonatomic, retain) NSNumber* lat;
@property (nonatomic, retain) NSNumber* lng;
@property (nonatomic, retain) NSNumber* max_paywhenpickup;
@property (nonatomic, retain) NSNumber* max_paywithcard;
@property (nonatomic, retain) NSString* mo_paywhenpickup;

@property (retain, readonly) CBLDatabase* storeDatabase;

- (CBLDatabase *)openStoreDatabase:(NSError **)outError;

- (NSDictionary *)findItemById:(NSString*)itemid;
- (int)discount;

- (void)makeOrder:(NSArray*)orderItems
          forUser:(MOAppUser*)user
  withTransaction:(MOTransaction*)transaction
    andPickupTime:(NSDate*)pickupTime
         subsribe:(BOOL)promotionSubscribe
         ereceipt:(NSString*)ereceiptType
          success:(void (^)())success
          failure:(void (^)(NSError *error))failure
   receiptSuccess:(void (^)())receiptSuccess
   receiptFailure:(void (^)(NSError *error))receiptFailure ;

- (void)sendOrderReceipt:(NSDictionary*)order
                 success:(void (^)())success
                 failure:(void (^)(NSError *error))failure;

- (BOOL)allowPayWhenPickup;

- (NSString*)decryptedMerchantId;
- (NSString*)decryptedMerchantKey;

@end

@interface MOStore (PaymentXP)

#define kXPUrl                      @"https://webservice.paymentxp.com/wh/WebHost.aspx"

#define kXPTransactionType          @"TransactionType"
#define kXPMerchantID               @"MerchantID"
#define kXPMerchantKey              @"MerchantKey"
#define kXPCardNumber               @"CardNumber"
#define kXPCVV2                     @"CVV2"
#define kXPBillingNameFirst         @"BillingNameFirst"
#define kXPBillingNameLast          @"BillingNameLast"
#define kXPBillingFullName          @"BillingFullName"
#define kXPBillingAddress           @"BillingAddress"
#define kXPBillingCity              @"BillingCity"
#define kXPBillingZipCode           @"BillingZipCode"
#define kXPExpirationDateMMYY       @"ExpirationDateMMYY"
#define kXPCardExpirationDate       @"CardExpirationDate"
#define kXPBillingState             @"BillingState"
#define kXPSubtotalAmount           @"CustomInfo1"
#define kXPTaxAmount                @"CustomInfo2"
#define kXPDiscountAmount           @"CustomInfo3"
#define kXPResultCode               @"ResultCode"
#define kXPExpirationMonth          @"ExpirationMonth"
#define kXPExpirationYear           @"ExpirationYear"
#define kXPClerkID                  @"ClerkID"
#define kXPStatusID                 @"StatusID"
#define kXPTransactionID            @"TransactionID"
#define kXPReferenceNumber          @"ReferenceNumber"
#define kXPTransactionAmount        @"TransactionAmount"
#define kXPAuthorizationCode        @"AuthorizationCode"
#define kXPResponseCode             @"ResponseCode"
#define kXPResponseMessage          @"ResponseMessage"
#define kXPMessage                  @"Message"
#define kXPCardNumber               @"CardNumber"
#define kXPCustomerName             @"CustomerName"
#define kXPCustomerID               @"CustomerID"
#define kXPProductDesc              @"ProductDesc"
#define kXPAction                   @"Action"
#define kXPPostedDate               @"PostedDate"
#define kXPEmailAddress             @"EmailAddress"

#define kXPAccountNumber            @"AccountNumber"
#define kXPAddress                  @"Address"
#define kXPFirstName                @"FirstName"
#define kXPLastName                 @"LastName"
#define kXPEmail                    @"Email"
#define kXPPhone                    @"Phone"
#define kXPZip                      @"Zip"

#define kXPStatus                   @"Status"

- (void)addCustomer:(MOAppUser*)customer
            success:(void (^)(NSDictionary *results))success
            failure:(void (^)(NSError *error))failure;
- (void)updateCustomer:(MOAppUser*)customer
               success:(void (^)(NSDictionary *results))success
               failure:(void (^)(NSError *error))failure;
- (void)chargeCustomer:(MOTransaction*)transaction
               success:(void (^)(MOTransaction* transaction))success
               failure:(void (^)(NSError *error))failure;
@end
