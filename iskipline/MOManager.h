//
//  MOStoreDb.h
//  iskipline
//
//  Created by Chinh Nguyen on 12/15/14.
//  Copyright (c) 2014 Chinh Nguyen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CouchbaseLite/CouchbaseLite.h>
#import "MOStore.h"

//#define STAGING

#ifdef STAGING

#define kMODatabaseHost @"central.sweetswipe.com:5984"
#define kMODatabaseUser @"saigon"
#define kMODatabasePass @"Devteam14"

#else

#define kMODatabaseHost @"wisetrak.com:7890"
#define kMODatabaseUser @"devteam"
#define kMODatabasePass @"Team500"

#endif

#define kMOBaseURL @"http://" kMODatabaseUser @":" kMODatabasePass @"@" kMODatabaseHost

#define kMOSyncURL kMOBaseURL @"/%@"
#define dbSyncURL(db) [NSURL URLWithString:[NSString stringWithFormat:kMOSyncURL, db]]

#define kMOBaseApiURL @"http://" kMODatabaseHost @"/%@"

#define apiURL(path) [NSURL URLWithString:[NSString stringWithFormat:kMOBaseApiURL, path]]
#define apiURLString(path) [NSString stringWithFormat:kMOBaseApiURL, path]

#define storeLoginViewURLString(host, db, user) [NSString stringWithFormat:@"http://%@/%@/_design/views/_view/user_login?key=%%22%@%%22", host, db, user]

#define storeByAccountViewURLString(host, account) [NSString stringWithFormat:@"http://%@/store_location/_design/view01/_view/by_account?key=%%22%@%%22", host, account]

#define storeOrderApiURLString(store, id) [NSString stringWithFormat:@"http://%@/%@_ao/%@", kMODatabaseHost, store, id]

#define storeSyncURL(host, account) [NSURL URLWithString:[NSString stringWithFormat:@"http://%@:%@@%@/%@", kMODatabaseUser, kMODatabasePass, host, account]]

#define appuserURLString(phone) [NSString stringWithFormat:@"http://%@/app_users/%@", kMODatabaseHost,phone]


#define kMODbSsid @"ssid_list"
#define kMODbStores @"store_location"

#define kMODocItems @"items"
#define kMODocCategories @"categories"
#define kMODocModifiers @"modifiers"
#define kMODocUsers @"users"


#define kMOSsidSyncURL dbSyncURL(kMODbSsid)
#define kMOStoresSyncURL dbSyncURL(kMODbStores)

#define kMOSsidReplicationChangedNofification @"kMOSsidReplicationChangedNofification"
#define kMOStoresReplicationChangedNofification @"kMOStoresReplicationChangedNofification"
#define kMOStoreDataReplicationChangedNofification @"kMOStoreDataReplicationChangedNofification"

@interface MOManager : NSObject

+ (MOManager*)sharedInstance;

- (void)startPullMetaDatabases:(NSError**)outError;
- (NSArray*)loadAllStores;
- (NSDictionary*)loadAvailableStatesCities;
- (NSArray*)findStoreByState:(NSString*)state andCity:(NSString*)city;
- (MOStore*)findStoreByAccount:(NSString*)account;
- (MOStore *)findStoreBySSID:(NSString *)nameSSID;

- (void)loginHost:(NSString*)host
            store:(NSString*)account
             user:(NSString*)user
         password:(NSString*)password
          success:(void (^)(id user, id store))success
          failure:(void (^)(NSError *error))failure;

- (void)loginAppUser:(NSString*)phone
            password:(NSString*)password
             success:(void (^)(id user))success
             failure:(void (^)(NSError *error))failure;
- (void)putAppUser:(MOAppUser*)user
           withRev:(NSString*)rev
          forStore:(MOStore*)store
            ccinfo:(NSDictionary*)ccinfo
           success:(void (^)(NSDictionary *user))success
           failure:(void (^)(NSError *error))failure;


@property CBLDatabase* ssidListDb;
@property CBLDatabase* storesDb;

- (void)put:(NSString*)url
     object:(NSDictionary*)data
    success:(void (^)(NSInteger statusCode, id responseObject))success
    failure:(void (^)(NSInteger statusCode, NSError *error))failure;

- (void)get:(NSString*)url
     params:(NSDictionary*)params
    success:(void (^)(NSInteger statusCode, id responseObject))success
    failure:(void (^)(NSInteger statusCode, NSError *error))failure;

@end
