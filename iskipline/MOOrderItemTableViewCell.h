//
//  MOOrderItemTableViewCell.h
//  iskipline
//
//  Created by Chinh Nguyen on 12/19/14.
//  Copyright (c) 2014 Chinh Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MOOrderItemTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *itemCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *itemNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *itemPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *modifierNoteLabel;
@property (weak, nonatomic) IBOutlet UILabel *xLabel;

@end
