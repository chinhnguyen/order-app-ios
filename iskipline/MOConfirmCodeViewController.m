//
//  MOConfirmCodeViewController.m
//  iskipline
//
//  Created by Chinh Nguyen on 12/21/14.
//  Copyright (c) 2014 Chinh Nguyen. All rights reserved.
//

#import "MOConfirmCodeViewController.h"
#import "MOEReceiptViewController.h"

#pragma mark - MOConfirmCodeViewController

@interface MOConfirmCodeViewController ()

@end

@implementation MOConfirmCodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"tableContainer"]) {
        MOConfirmCodeTableViewController *destinationVC = segue.destinationViewController;
        destinationVC.name = self.name;
        destinationVC.phone = self.phone;
        destinationVC.store = self.store;
        destinationVC.order = self.order;
    }
}

@end


#pragma mark - MOConfirmCodeTableViewController

@interface MOConfirmCodeTableViewController () {
    int _code;
}

@end

@implementation MOConfirmCodeTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.codeText.layer.borderColor = [UIColor whiteColor].CGColor;
    self.codeText.layer.borderWidth = 1;
    self.codeText.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Confirmation code", @"") attributes:@{ NSForegroundColorAttributeName:[UIColor colorWithWhite:0.5f alpha:0.8f]}];
    
    self.submitButton.layer.cornerRadius = 5;
    self.submitButton.layer.borderWidth = 1;
    self.submitButton.layer.borderColor = [UIColor whiteColor].CGColor;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.codeText becomeFirstResponder];
    });
    
    if (self.phone && self.store && self.order) {
        _code = arc4random_uniform(10000);
#ifdef DEBUG
        [self.view makeToast:[NSString stringWithFormat:@"%d", _code]duration:3.0 position:CSToastPositionTop];
#endif
        void(^failure)(NSError*) = ^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [UIAlertView showWithTitle:NSLocalizedString(@"Failed sending code", @"") message:[NSString stringWithFormat:NSLocalizedString(@"Could not send verification code to the given phone number. Please contact store for further support. \n\n%@", @""), error.localizedDescription] cancelButtonTitle:NSLocalizedString(@"Close", @"") otherButtonTitles:nil tapBlock:nil];
            });
        };
        
        NSString *firstname = @"";
        NSString *lastname = @"";
        
        if (self.name && self.name.length > 0) {
            NSArray *words = [self.name componentsSeparatedByString:@" "];
            NSArray *lastnameWords = [words subarrayWithRange:NSMakeRange(1, words.count-1)];
            firstname = [words objectAtIndex:0];
            lastname = [lastnameWords componentsJoinedByString:@" "];
        }
        
        NSDictionary *user =
        [NSDictionary dictionaryWithObjectsAndKeys:
         self.phone, @"phone",
         firstname, @"firstname",
         lastname, @"lastname",
         nil];
        
        // Send the code via SMS
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            void(^sendSMS)(NSString*) = ^(NSString* subscriptionId) {
                NSString *description = [NSString stringWithFormat:NSLocalizedString(@"Confirmation code %d", @""), _code];
                
                NSString *message;
                if (self.name && self.name.length > 0) {
                    message = [NSString stringWithFormat:NSLocalizedString(@"Please enter %d into the iSkipLine app now. Thank you, %@!", @""), _code, self.name];
                } else {
                    message = [NSString stringWithFormat:NSLocalizedString(@"Please enter %d into the iSkipLine app now. Thank you!", @""), _code];
                }
                [ZTTrumpia sendSMSTo:subscriptionId withDescription:description organizationName:self.store.store text:message success:^{
                    //Nothing to do here
                } failure:failure];
            };
            
            [ZTTrumpia searchSubscriptionWithType:@"2" andData:self.phone success:^(NSString *subscriptionId) {
                if (subscriptionId && subscriptionId.length > 0) {
                    //                    [ZTTrumpia postSubscription:user forId:subscriptionId success:^{
                    //                        sendSMS(subscriptionId);
                    //                    } failure:failure];
                    sendSMS(subscriptionId);
                } else {
                    [ZTTrumpia putSubscription:user success:^(NSString *subscriptionId) {
                        sendSMS(subscriptionId);
                    } failure:failure];
                }
            } failure:failure];
        });
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if ([identifier isEqualToString:@"showEReceipt"]) {
        if ([self.codeText.text intValue] != _code) {
            [self showInvalidCodeMessage];
            return NO;
        }
        return YES;
    }
    
    return [super shouldPerformSegueWithIdentifier:identifier sender:sender];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showEReceipt"]) {
        MOEReceiptViewController *destinationVC = segue.destinationViewController;
        destinationVC.store = self.store;
        destinationVC.order = self.order;
        destinationVC.name = self.name;
        destinationVC.phone = self.phone;
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ([self.codeText.text intValue] != _code) {
        [self showInvalidCodeMessage];
        return NO;
    }
    
    [self performSegueWithIdentifier:@"showEReceipt" sender:textField];
    return YES;
}

- (void)showInvalidCodeMessage {
    [UIAlertView showWithTitle:NSLocalizedString(@"Invalid code", @"") message:NSLocalizedString(@"Please enter the code that you've received via SMS. Or going back to previous screen and request for a new code.", @"") cancelButtonTitle:NSLocalizedString(@"Close", @"") otherButtonTitles:nil tapBlock:nil];
}

@end

