//
//  MOHomeViewController.m
//  iskipline
//
//  Created by Chinh Nguyen on 12/15/14.
//  Copyright (c) 2014 Chinh Nguyen. All rights reserved.
//

#import "MOHomeViewController.h"
#import "MOStoreListViewController.h"
#import "MOStoreTableViewCell.h"
#import "MOCategoryViewController.h"

@interface MOHomeViewController () {
    NSArray* _sortedStates;
    NSDictionary* _stateCitiesMap;
    
    NSArray* _visitedStores;
}

@end

@implementation MOHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _visitedStores = [NSArray new];
    
    // Init the cover view, add touch to hide picker view
    self.coverView.userInteractionEnabled = YES;
    self.coverView.hidden = YES;
    UITapGestureRecognizer* singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self                                            action:@selector(coverViewDidTouch:)];
    [self.coverView addGestureRecognizer:singleFingerTap];
    
    // Toolbar
    [self.showStoresButton setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, [UIFont fontWithName:@"Myriad Pro" size:22.0f], NSFontAttributeName, nil] forState:UIControlStateNormal];
    [self.view layoutIfNeeded];
    
    // Listen to the changes of visited stores
    [[NSUserDefaults standardUserDefaults] addObserver:self forKeyPath:kMOVisitedStoresList options:NSKeyValueObservingOptionNew                                            context:NULL];
}

- (void)dealloc {
    [[NSUserDefaults standardUserDefaults] removeObserver:self forKeyPath:kMOVisitedStoresList];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Hide the picker view
    self.pickerViewBottomConstraint.constant = self.pickerView.frame.size.height * -1;
    
    // Load recent visited list
    [self loadDataAndUpdateLayout];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Always hide this view
    self.coverView.hidden = YES;
    
    if ([segue.identifier isEqualToString:@"showStores"]) {
        MOStoreListViewController *destinationVC = segue.destinationViewController;
        //            if (_isByLocation) {
        //                destViewController.isByLocation = YES;
        //            }else{
        //                destViewController.isByLocation = NO;
        NSInteger iState = [self.stateCitiesPickerView selectedRowInComponent:0];
        NSInteger iCity = [self.stateCitiesPickerView selectedRowInComponent:1];
        NSString *state = [_sortedStates objectAtIndex:iState];
        destinationVC.city = [[_stateCitiesMap objectForKey:state] objectAtIndex:iCity];
        destinationVC.state = state;
        destinationVC.isNearby = NO;
    } else if ([segue.identifier isEqualToString:@"enterStore"]) {
        NSIndexPath *p = [self.tableView indexPathForSelectedRow];
        MOCategoryViewController *destinationVC = segue.destinationViewController;
        NSDictionary *s = [_visitedStores objectAtIndex:p.row];
        destinationVC.store = [[MOManager sharedInstance] findStoreByAccount:s[@"db"]];
    }
}


#pragma mark - Buttons

- (IBAction)byCityStateDidTouch:(id)sender {
    //    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        _stateCitiesMap = [[MOManager sharedInstance] loadAvailableStatesCities];
        
        // Sorted states being displayed on UIPickerView
        _sortedStates = [[_stateCitiesMap allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
        
        // Sorted cities
        for(id key in _stateCitiesMap) {
            id cities = [_stateCitiesMap objectForKey:key];
            [cities sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.stateCitiesPickerView reloadAllComponents];
            [self setPickerDefaultValue];
            
            if (_sortedStates && _sortedStates.count > 0) {
                [self showPickerView];
            }
            
            //            [MBProgressHUD hideHUDForView:self.view animated:YES];
        });
    });
}

- (IBAction)logoDicTouch:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Admin" bundle:nil];
    UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"Login"];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - UIPickerViewDataSource

// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 2;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (_sortedStates && _sortedStates.count > 0) {
        if (component == 0) {
            return _sortedStates.count;
        } else if (component == 1) {
            NSInteger stateIndex = [pickerView selectedRowInComponent:0];
            NSString *key = [_sortedStates objectAtIndex:stateIndex];
            return [[_stateCitiesMap objectForKey:key] count];
        }
    }
    return 0;
}

#pragma mark - UIPickerViewDelegate

// returns width of column and height of row for each component.
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 30.0;
}

// these methods return either a plain NSString, a NSAttributedString, or a view (e.g UILabel) to display the row for the component.
// for the view versions, we cache any hidden and thus unused views and pass them back for reuse.
// If you return back a different object, the old one will be released. the view will be centered in the row rect
- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSString* title = @"";
    if (_sortedStates && _sortedStates.count > 0) {
        if (component == 0) {
            title = [_sortedStates objectAtIndex:row];
        } else if (component == 1) {
            NSInteger stateIndex = [pickerView selectedRowInComponent:0];
            NSString *key = [_sortedStates objectAtIndex:stateIndex];
            title = [[_stateCitiesMap objectForKey:key] objectAtIndex:row];
        }
    }
    
    return [[NSAttributedString alloc] initWithString:title attributes:@{ NSForegroundColorAttributeName:[UIColor whiteColor] }];
}

//If the user chooses from the pickerview, it calls this function;
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if (component == 0) {
        [pickerView reloadComponent:1];
        
        NSString* state = [_sortedStates objectAtIndex:row];
        if ([state isEqualToString:@"CA"]) {
            NSArray * array = [_stateCitiesMap objectForKey:@"CA"];
            NSUInteger cityIndex = [array indexOfObject:@"San Jose"];
            if (cityIndex != NSNotFound) {
                [self.stateCitiesPickerView reloadComponent:1];
                [self.stateCitiesPickerView selectRow:cityIndex inComponent:1 animated:NO];
            }
        }
    }
}

#pragma mark - UIPickerView related

- (void)setPickerDefaultValue {
    NSUInteger stateIndex = [_sortedStates indexOfObject:@"CA"];
    if (stateIndex != NSNotFound) {
        NSArray *array = [_stateCitiesMap objectForKey:@"CA"];
        NSUInteger cityIndex = [array indexOfObject:@"San Jose"];
        if (cityIndex != NSNotFound) {
            [self.stateCitiesPickerView selectRow:stateIndex inComponent:0 animated:NO];
            [self.stateCitiesPickerView reloadComponent:1];
            [self.stateCitiesPickerView selectRow:cityIndex inComponent:1 animated:NO];
        }
    }
}

//http://stackoverflow.com/questions/12926566/are-nslayoutconstraints-animatable
- (void)showPickerView {
    [self.view layoutIfNeeded];
    
    // show the cover view
    self.coverView.hidden = NO;
    
    [UIView animateWithDuration:0.3f animations:^{
        self.pickerViewBottomConstraint.constant= 0.0f;
        
        [self.view layoutIfNeeded];
    }];
}

- (void)hidePickerView {
    [self.view layoutIfNeeded];
    
    // Hide the cover view
    self.coverView.hidden = YES;
    
    [UIView animateWithDuration:0.3f animations:^{
        self.pickerViewBottomConstraint.constant = self.pickerView.frame.size.height * -1;
        
        [self.view layoutIfNeeded];
    }];
}

- (void)coverViewDidTouch:(id)sender {
    [self hidePickerView];
}

#pragma mark - Visited Stores Related

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    [self loadDataAndUpdateLayout];
}

- (void)loadDataAndUpdateLayout {
    _visitedStores = [[NSUserDefaults standardUserDefaults] arrayForKey:kMOVisitedStoresList];
    
    if (_visitedStores && _visitedStores.count > 0) {
        self.emptyLabel.hidden = YES;
        self.tableView.hidden = NO;
    } else {
        self.emptyLabel.hidden = NO;
        self.tableView.hidden = YES;
    }
    
    [self.tableView reloadData];
}

- (IBAction)removeStoreDidTouch:(id)sender {
    // Get mutable copy of the list
    NSMutableArray *visitedStores = [[[NSUserDefaults standardUserDefaults] arrayForKey:kMOVisitedStoresList] mutableCopy];
    
    // Remove the store
    UIButton *b = sender;
    [visitedStores removeObjectAtIndex:b.tag];
    
    // Save to user preferences
    [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithArray:visitedStores] forKey:kMOVisitedStoresList];
    
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _visitedStores.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MOStoreTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"StoreCell"];
    
    NSDictionary *s = [_visitedStores objectAtIndex:indexPath.row];
    cell.nameLabel.text = s[@"name"];
    cell.addressLabel.text = [NSString stringWithFormat:@"%@, %@, %@", s[@"address"], s[@"city"], s[@"state"]];
    cell.removeButton.tag = indexPath.row;
    
    if (cell.selectedBackgroundView.backgroundColor == nil) {
        UIView *bgColorView = [UIView new];
        bgColorView.layer.masksToBounds = YES;
        [bgColorView setBackgroundColor:[UIColor colorWithWhite:0.5 alpha:0.5]];
        [cell setSelectedBackgroundView:bgColorView];
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60.0f;
}


@end
