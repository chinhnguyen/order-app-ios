//
//  SSIDFinder.h
//  iskipline
//
//  Created by TIENPHAM on 8/30/15.
//  Copyright (c) 2015 Chinh Nguyen. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface SSIDFinder : NSObject

+ (BOOL)checkIfSSIDMatchOrderPad;
+ (NSString *)getCurrWifiSSID;
+ (NSString *)getIPAddress;

@end
