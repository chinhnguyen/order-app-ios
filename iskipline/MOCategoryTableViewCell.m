//
//  MOCategoryTableViewCell.m
//  iskipline
//
//  Created by Chinh Nguyen on 12/17/14.
//  Copyright (c) 2014 Chinh Nguyen. All rights reserved.
//

#import "MOCategoryTableViewCell.h"

@implementation MOCategoryTableViewCell

- (void)awakeFromNib {
//    self.bgImageView.clipsToBounds = YES;    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    if (selected) {
        // Configure the view for the selected state
        UIView *bgColorView = [UIView new];
        bgColorView.layer.masksToBounds = YES;
        [bgColorView setBackgroundColor:[UIColor colorWithWhite:0.5 alpha:0.5]];
        [self setSelectedBackgroundView:bgColorView];
    }
}

@end
